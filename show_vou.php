<?php
require_once('connect.php');

$vou_no=mysqli_real_escape_string($conn,strtoupper($_POST['vou_no']));
$voutype=mysqli_real_escape_string($conn,$_POST['voutype']);
?>		
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css" media="print">
@media print {
body {
   zoom:70%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.printpage * { visibility: visible}
.head * { visibility: visible}
.fileupload * { display: none}
.buttons * { display: none}
.printpage { position: absolute; top: 0; left: 0;}
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>

<body style="overflow-x:hidden;">
<div class="printpage">
<?php 
if($voutype=='Expense_Voucher')
{
$sql="select * from mk_venf where vno='$vou_no'";

if($result = mysqli_query($conn,$sql))
{
if(mysqli_num_rows($result) > 0)
    {
$row = mysqli_fetch_array($result);
?>
<br />
<center><h4 style="font-family:Verdana">&nbsp; <?php echo $vou_no; ?> - <?php echo $voutype; ?> </h3></center>
<br />
<div class="container">
<center>
<table style="width:1100px;font-family:Verdana;font-size:12px;" class="table table-bordered">
      <tr>
        <th>Vou. Date </th>
        <td id="linew"><?php echo $row ['newdate']; ?></td>
        <th>Company </th>
        <td id="linew"><?php echo $row ['comp']; ?></td>
      </tr>
      <tr>
        <th>Expense Desc. </th>
        <td id="linew"><?php echo $row ['des']; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row ['amt']; ?></td>
      </tr>
      <tr>
        <th>Amount (words)</th>
        <td id="linew"><?php echo $row ['amt_w']; ?></td>
        <th>Branch</th>
        <td id="linew"><?php echo $row ['user']; ?></td>
	  </tr>
      <tr>
        <th>Payment By </th>
        <td id="linew"><?php echo $row ['chq']; ?></td>
		<th>PAN No. </th>
        <td id="linew"><?php echo $row ['pan']; ?></td>
      </tr>
	  <?php
	  if($row['chq']=='CHEQUE')
	  {
	 ?>
	  <tr>
        <th>Cheque No.  </th>
        <td id="linew"><?php echo $row ['chq_no']; ?></td>
        <th>Bank Name  </th>
        <td id="linew"><?php echo $row ['chq_bnk_n']; ?></td>
      </tr>
	 <?php
	  }
	  else if($row['chq']=='NEFT')
	  {
	 ?>
	  <tr>
        <th>A/c Holder </th>
        <td id="linew"><?php echo $row ['neft_acname']; ?></td>
        <th>A/c No </th>
        <td id="linew"><?php echo $row ['neft_acno']; ?></td>
      </tr>
	   <tr>
        <th>Bank Name </th>
        <td id="linew"><?php echo $row ['neft_bank']; ?></td>
        <th>IFSC Code </th>
        <td id="linew"><?php echo $row ['neft_ifsc']; ?></td>
      </tr>
	  <?php 
	  }
	  ?>
     
      <tr>
		<th>Narration </th>
        <td id="linew"><?php echo $row ['narrat']; ?></td>
        <th class="fileupload">Uploads</th>
        <td id="linew" class="fileupload">
		<form action="exp_vou_download.php" method="POST" target="_blank">
		<input type="hidden" name="vno" value="<?php echo $vou_no; ?>" />
		<input type="submit" value="View Attachment File" class="btn btn-sm btn-primary" />
		</form>
		</td>
      </tr>
     <tr>
<tr>
        <th>System Date</th>
        <td id="linew"><?php echo $row ['date']; ?></td>
        <td></td>
        <td id="linew"></td>
      </tr>
       <th>Cashier Sign</th>
 <td><img src="../b5aY6EZzK52NA8F/<?php echo $row['cash_sign']; ?>" width="160px" height="80px;" style="border:0px solid #ccc;"></td>
       
        <th>Receiver Sign</th>
        <td><img src="../b5aY6EZzK52NA8F/<?php echo $row['rcvr_sign']; ?>" width="160px" height="80px;" style="border:0px solid #ccc;"></td>
        
      </tr>
   
</table>
</center>
</div>
</div>

<center>
<br />
<button onclick="print();" style="font-family:Verdana;letter-spacing:1px;color:#000" class="btn-md btn btn-warning"><b>Print Voucher</button></b>
&nbsp;
<a href="./"><button style="font-family:Verdana;letter-spacing:1px;color:#000" class="btn btn-warning btn-md"><b>Dashboard</button></b></a>
</center>

<?php

}
else
{
	echo "<script>
		alert('No result found...');
		window.location.href='./';
		window.close();
		</script>";
		mysqli_close($conn);
		exit();
}
}
}
?>
<div class="printpage">
<?php
if($voutype=='Truck_Voucher')
{
$sql="select * from mk_tdv where tdvid='$vou_no' order by id desc limit 1";

if($result = mysqli_query($conn,$sql))
{
if(mysqli_num_rows($result) > 0)
    {
$row = mysqli_fetch_array($result);
?>
<br />
<center><h4 style="font-family:Verdana">&nbsp; <?php echo $vou_no; ?> - <?php echo $voutype; ?> </h4></center>
<br />
<div class="container">
<center>
<table class="table table-bordered" style="width:1100px;font-family:Verdana;font-size:12px;">
      <tr>
        <th>Vou. Date  </th>
        <td id="linew"><?php echo $row ['newdate']; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row ['amt']; ?></td>
      </tr>
      <tr>
        <th>Amount (words)</th>
        <td id="linew"><?php echo $row ['amtw']; ?></td>
        <th>Truck No.</th>
        <td id="linew"><?php echo $row ['truckno']; ?></td>
      </tr>
      <tr>
        <th>Company </th>
        <td id="linew"><?php echo $row ['company']; ?></td>
        <th>Narr./Destination</th>
        <td id="linew"><?php echo $row ['dest']; ?></td>
      </tr>
      <tr>
        <th>Driver Name</th>
        <td id="linew"><?php echo $row ['dname']; ?></td>
        <th>Narration</th>
        <td id="linew" style="width:330px;"><?php echo $row ['nar']; ?></td>
      </tr>
	  	  <?php
	  if($row['mode']=='CHEQUE')
	  {
	 ?>
	  <tr>
        <th>Cheque No.  </th>
        <td id="linew"><?php echo $row ['chq_no']; ?></td>
        <th>Bank Name  </th>
        <td id="linew"><?php echo $row ['chq_bank']; ?></td>
      </tr>
	 <?php
	  }
	  else if($row['mode']=='NEFT')
	  {
	 ?>
	  <tr>
        <th>A/c Holder </th>
        <td id="linew"><?php echo $row ['ac_name']; ?></td>
        <th>A/c No </th>
        <td id="linew"><?php echo $row ['ac_no']; ?></td>
      </tr>
	   <tr>
        <th>Bank Name </th>
        <td id="linew"><?php echo $row ['bank']; ?></td>
        <th>IFSC Code </th>
        <td id="linew"><?php echo $row ['ifsc']; ?></td>
      </tr>
	  <?php 
	  }
	  ?>
     
     <tr>
        <th>System Date</th>
        <td id="linew"><?php echo $row ['date']; ?></td>
        <th>PAN No. </th>
        <td id="linew"><?php echo $row ['pan']; ?></td>
      </tr>
     <tr>
        <th>Cashier Sign</th>
        <td><img src="../b5aY6EZzK52NA8F/<?php echo $row['cash_sign']; ?>" width="160px" height="80px;" style="border:0px solid #ccc;"></td>
        <th>Driver Sign</th>
        <td><img src="../b5aY6EZzK52NA8F/<?php echo $row['rcvr_sign']; ?>" width="160px" height="80px;" style="border:0px solid #ccc;"></td>  
      </tr>
    </tbody>
</table>
</center>
</div>
</div>
<center>
<br />
<div class="buttons">
<button style="font-family:Verdana;letter-spacing:1px;color:#000;" onclick="print();" class="btn btn-warning">Print Voucher</button>
&nbsp;
<a href="./"><button style="font-family:Verdana;letter-spacing:1px;color:#000;" class="btn btn-warning">Dashboard</button></a>
</center>
</div>



<?php 
}
else
{
	echo "<script>
		alert('No result found...');
		window.location.href='./';
		window.close();
		</script>";
		mysqli_close($conn);
		exit();
}
}
}
?>
<script>
function myFunction() {
    window.print();
}
</script>
</body>
</html>					
<?php
mysqli_close($conn);
?>	