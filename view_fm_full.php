<?php				
require_once './connect.php';

$idmemo=mysqli_real_escape_string($conn,strtoupper($_POST['idmemo']));

$fetch_data=mysqli_query($conn,"SELECT f.newdate,f.truck_no,f.branch,f.company,f.actualf,
f.newtds,f.dsl_inc,f.newother,f.tds,f.totalf,f.totaladv,f.ptob,
f.adv_date,f.cashadv,f.chqadv,f.chqno,f.disadv,f.rtgsneftamt,f.narre,
f.baladv,f.unloadd,f.otherfr,f.claim,f.late_pod,f.totalbal,f.paidto,f.bal_date,
f.paycash,f.paycheq,f.paycheqno,f.paydsl,f.newrtgsamt,f.narra,f.sign_cash,f.sign_adv,f.sign_bal,f.pod_date,f.branch_bal,
b.name as b_name,b.mo1 as b_mobile,b.pan as b_pan,
d.name as d_name,d.mo1 as d_mobile,d.pan as d_lic,
o.name as o_name,o.mo1 as o_mobile,o.pan as o_pan,
pod.branch as pod_branch,
dsl.dsl_by as adv_dsl_by,dsl.dcard as adv_card,dsl.dsl_nrr as adv_dsl_nrr,
dsl1.dsl_by as bal_dsl_by,dsl1.dcard as bal_card,dsl1.dsl_nrr as bal_dsl_nrr 
FROM freight_form as f
		LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
		LEFT OUTER JOIN mk_driver AS d ON d.id = f.did
		LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
		LEFT OUTER JOIN rcv_pod AS pod ON pod.frno = f.frno
		LEFT OUTER JOIN diesel_fm AS dsl ON dsl.fno = f.frno AND dsl.type='ADVANCE'
		LEFT OUTER JOIN diesel_fm AS dsl1 ON dsl1.fno = f.frno AND dsl1.type='BALANCE'
		WHERE f.frno='$idmemo'");

if(!$fetch_data)
{
	echo mysqli_error($conn);
	exit();
}
	if(mysqli_num_rows($fetch_data)==0)
	{
		echo "No result found..";
		exit();
	}
	
		$row1=mysqli_fetch_array($fetch_data);
		
		$fmdt=$row1['newdate'];
		$tno=$row1['truck_no'];
		$fm_branch=$row1['branch'];
		$company=$row1['company'];
		$af=$row1['actualf'];
		$load=$row1['newtds'];
		$dsl_inc=$row1['dsl_inc'];
		$other=$row1['newother'];
		$tds=$row1['tds'];
		$tf=$row1['totalf'];
		$total_adv=$row1['totaladv'];
		$adv_to=$row1['ptob'];
		$adv_date=$row1['adv_date'];
		$cash=$row1['cashadv'];
		$cheque=$row1['chqadv'];
		$cheque_no=$row1['chqno'];
		$diesel=$row1['disadv'];
		$rtgs=$row1['rtgsneftamt'];
		$adv_narr=$row1['narre'];
		
		$balance=$row1['baladv'];
		$unloading=$row1['unloadd'];
		$other_charge=$row1['otherfr'];
		$claim=$row1['claim'];
		$late_pod=$row1['late_pod'];
		$total_balance=$row1['totalbal'];
		$bal_to=$row1['paidto'];
		$bal_date=$row1['bal_date'];
		$cash2=$row1['paycash'];
		$cheque2=$row1['paycheq'];
		$cheque_no2=$row1['paycheqno'];
		$diesel2=$row1['paydsl'];
		$rtgs2=$row1['newrtgsamt'];
		$bal_narr=$row1['narra'];
		
		$sign_cash=$row1['sign_cash'];
		$sign_adv=$row1['sign_adv'];
		$sign_bal=$row1['sign_bal'];
		$pod_date=$row1['pod_date'];
		$branch_bal=$row1['branch_bal'];
		
		$broker_name=$row1['b_name'];
		$broker_mobile=$row1['b_mobile'];
		$broker_pan=$row1['b_pan'];
		
		$drover_name=$row1['d_name'];
		$driver_mobile=$row1['d_mobile'];
		$driver_lic=$row1['d_lic'];
		
		$owner_name=$row1['o_name'];
		$owner_mobile=$row1['o_mobile'];
		$owner_pan=$row1['o_pan'];
		$pod_by=$row1['pod_branch'];
		
		$fmdate=date('d/m/y', strtotime($fmdt));
		$adv_date1=date('d/m/y', strtotime($adv_date));
		$bal_date1=date('d/m/y', strtotime($bal_date));
?>
<!DOCTYPE html>
<html lang="en">
<head>

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center>
	<img style="margin-top:150px" src="./load.gif" />
</center>
</div>

<style>
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}

.form-control{
	border:1px solid #000;
	text-transform:uppercase;
}
</style>	

<style type="text/css">
@media print
{
body {
   zoom:70%;
 }	
body * { visibility: hidden; }
#printpage * { visibility: visible; }
#printpage { position: absolute; top: 0; left: 0; }
}
</style>

</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<body style="overflow-x: scroll !important;" onload="lrFetch();">

<a href="./">
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;color:#FFF;font-family:Verdana" class="btn btn-primary">Dashboard</button></a>
	<button style="font-family:Verdana;margin-top:10px;margin-left:10px;font-weight:bold;" class="btn btn-danger" onclick="print()">Print Freight Memo</button>
<div id="printpage">
 <div class="container-fluid">
	<div class="row" style="font-family:Verdana">
	  <div class="col-md-3">
		
	</div>		
	 <div class="col-md-6">
		<center><span class="" style="font-size:15px;letter-spacing:1px">Freight Memo : <?php echo strtoupper($idmemo); ?> </span> </center>
	</div>		
	 <div class="col-md-3">
		<span style="font-weight:bold;font-size:12px;margin-right:10px" class="pull-right">FM Date : <?php echo $fmdate; ?>, Adv Date : <?php echo $adv_date; ?></span>
	</div>
 </div>

<input type="text" id="fids" name="fids" value="<?php echo $idmemo; ?>" style="visibility:hidden;" />

<div class="row">
<div class="col-md-12" style="font-family: verdana;">
<table border="0" width="100%" style="font-size:11px;">
	<tr>
		<td>
           <label>Truck No: &nbsp;</label>
           <?php echo $tno; ?>
        </td>
		
		<td>
           <label>Company: &nbsp;</label>
           <?php echo $company; ?>
        </td>
        
		<td>  
			<label>Branch: &nbsp;</label>
           <?php echo $fm_branch; ?>
        </td>
	</tr>		

	<tr>
        <td>   
			<label>Broker: &nbsp;</label>
           <?php echo $broker_name; ?>
        </td>
		
		<td>  
           <label>Broker Mo.: &nbsp;</label>
           <?php echo $broker_mobile; ?>
        </td>
		
		<td>  
           <label>Broker PAN: &nbsp;</label>
           <?php echo $broker_pan; ?>
        </td>
</tr>		

<tr>
		<td>
           <label>Driver: &nbsp;</label>
           <?php echo $drover_name; ?>
        </td>
        
		<td>  
           <label>Driver Mo.: &nbsp;</label>
           <?php echo $driver_mobile; ?>
		</td>
        
		<td>  
			<label>Driver LIC: &nbsp;</label>
           <?php echo $driver_lic; ?>
        </td>
</tr>		

<tr>
		<td>
			<label>Owner: &nbsp;</label>
           <?php echo $owner_name; ?>
        </td>
		
		<td>  
           <label>Owner Mobile: &nbsp;</label>
           <?php echo $owner_mobile; ?>
        </td>
		
		<td>  
           <label>Owner PAN: &nbsp;</label>
           <?php echo $owner_pan; ?>
        </td>
</tr>		
</table>
<br />
<span style="color:blue;font-size:13px;font-weight:bold;">Advance Details</span>
<div>
<table border="0" width="100%" style="font-size:11px;margin-top:10px">

<tr>
	<td>
        <label>Actual Freight:</label>
        <?php echo $af; ?>
    </td>
	
	<td>
		<label>Loading:</label>
        <?php echo $load; ?>
	</td>
	
	<td>
		<label>Diesel Inc. :</label>
        <?php echo $dsl_inc; ?>
	</td>
	
	<td>	
        <label>Other:</label>
        <?php echo $other; ?>
    </td>
	
	<td>
		<label>TDS:</label>
        <?php echo $tds; ?>
    </td>
	
	<td>
		<label>Total Fright :</label>
        <?php echo $tf; ?>
    </td>
       
</tr>		

<tr>		
	<td>
		<label>Total Advance:</label>
        <?php echo $total_adv; ?>
    </td>
		
		<td>
           <label>Cash :</label>
           <?php echo $cash; ?>
        </td>
	
	<td>
		<label>Cheque:</label>
        <?php echo $cheque; ?>
     </td>
	 
	 <td>
		<label>Cheque No:</label>
        <?php echo $cheque_no; ?>
     </td>

	<td>
	      <label>Diesel :</label>
          <?php echo $diesel; ?>
    </td>	
	 
  <td>
       <label>RTGS/NEFT:</label>
       <?php echo $rtgs; ?>
   </td>
		
		
</tr>

<tr>
		<td>
           <label>Advance to:</label>
           <?php echo $adv_to; ?>
        </td>

		<td>
           <label>Advance Date:</label>
           <?php echo $adv_date1; ?>
        </td>
		
		<td colspan="2">
           <label>Narration:</label>
           <?php echo $adv_narr; ?>
		</td>

</tr>		
</table>
</div>	
<br />
<span style="color:blue;font-size:13px;font-weight:bold;">Balance Details</span>
<div>
<table border="0" width="100%" style="font-size:11px;margin-top:5px;">
<tr>
	
	
	<td>
           <label>Balance:</label>
           <?php echo $balance; ?>
     </td>
	 
     <td>
           <label>Unloading:</label>
           <?php echo $unloading; ?>
     </td>

	<td>
		   <label>Other:</label>
           <?php echo $other_charge; ?>
    </td>
	
	<td>
		   <label>Claim:</label>
           <?php echo $claim; ?>
    </td>

	<td>
		   <label>Late POD:</label>
           <?php echo $late_pod; ?>
    </td>
	
	<td>
           <label>Total Balance:</label>
           <?php echo $total_balance; ?>
    </td>
</tr>
	
<tr>

	<td>	
           <label>Cash Amount:</label>
           <?php echo $cash2; ?>
    </td>
	
	<td>
           <label>Cheq Amount:</label>
           <?php echo $cheque2; ?>
    </td>
	
	<td colspan="2">
           <label>Cheque No:</label>
           <?php echo $cheque_no2; ?>
    </td>
	
	<td>
           <label>Diesel Amt</label>
          <?php echo $diesel2; ?>
    </td>
	
	<td>
           <label>RTGS/NEFT:</label>
          <?php echo $rtgs2; ?>
    </td>
	
</tr>
<tr>
	<td>
           <label>Balance TO:</label>
           <?php echo $bal_to; ?>
    </td>
	
	<td>	
           <label>Balance Date:</label>
           <?php echo $bal_date1; ?>
    </td>
	
	<td colspan="2">	
           <label>Narration:</label>
           <?php echo $bal_narr; ?>
    </td>
	
	<td style="color:red">	
           <label>POD Branch:</label>
           <?php echo $pod_by."(".$pod_date.")"; ?>
    </td>
	
	<td style="color:red">	
           <label>Balance Branch:</label>
           <?php echo $branch_bal; ?>
    </td>
</tr>
</table>		   
</div>
        </div></div>
		</div>
<script type="text/javascript">
lrFetch();
function lrFetch()
{ 
	$("#new").show();
	jQuery.ajax({
	url: "./ajax/readRecords.php",
	data: 'fids=' + $("#fids").val(),
	type: "POST",
	success: function(data) {
	$(".records_content").html(data);
	$("#new").hide();
	},
	error: function() {}
	});
}					
</script>	
		<br />
		<div class="container-fluid">
        <span style="font-weight:bold;font-size:12px;font-family: 'Verdana', cursive;">Freight MEMO LR Details: </span>
        <div class="records_content table-responsive" style="font-family:Verdana; color:#000; font-size:12px"></div>

<table border="0" style="width:100%">
	<tr>
		<td align="center"><label style="font-size:13px;">Cashier Sign</label></td>
		<td align="center"><label style="font-size:13px;">Adv Rcvr</label></td>
		<td align="center"><label style="font-size:13px;">Bal Rcvr</label></td>
	</tr>	
	<tr>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $sign_cash; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $sign_adv; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $sign_bal; ?>"></td>
	</tr>	
</table>
</div> 
</div>
	
</body>
</html>