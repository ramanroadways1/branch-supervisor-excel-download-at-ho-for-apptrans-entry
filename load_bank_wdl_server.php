<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);
$company = escapeString($conn,$_POST['company']);
$timestamp = date("Y-m-d H:i:s");

if($company=='BOTH'){
	$company_var = "";
}else{
	$company_var = "AND c.comp='$company'";
}

	if($branch=='ALL')
	{
		$qry = Qry($conn,"SELECT c.user,c.date,c.comp,c.desct,IF(c.comp='RRPL',c.credit,credit2) AS amount,e.name 
		FROM cashbook AS c 
		LEFT OUTER JOIN emp_attendance AS e ON e.code = c.user_code
		WHERE c.date BETWEEN '$from_date' AND '$to_date' $company_var AND c.user IN($limit) AND c.vou_type='CREDIT ADD BALANCE' ORDER BY c.id ASC");
	}
	else
	{
		$qry = Qry($conn,"SELECT c.user,c.date,c.comp,c.desct,IF(c.comp='RRPL',c.credit,credit2) AS amount,e.name 
		FROM cashbook AS c 
		LEFT OUTER JOIN emp_attendance AS e ON e.code = c.user_code
		WHERE c.date BETWEEN '$from_date' AND '$to_date' $company_var AND c.user='$branch' AND c.vou_type='CREDIT ADD BALANCE' 
		ORDER BY c.id ASC");
	}


if(numRows($qry)==0)
{
	echo "
	<font color='red'><center>No result found..</center></font>
	<script>
		$('#loadicon').fadeOut();
	</script>";
}

echo "<table id='example' class='display nowrap table table-bordered' style='font-size:11px;'>
		<thead>
			<tr>
				<th>#</th>
				<th>Branch</th>
				<th>Branch_user</th>
				<th>Date</th>
				<th>Company</th>
				<th style='width:250px'>Particulars</th>
				<th>Amount</th>
			</tr>
		</thead>
		</tbody>	
		";
			
		$sn=1;	
		
		while($row = fetchArray($qry))
		{
			echo "<tr>
				<td>$sn</td>
				<td>$row[user]</td>
				<td>$row[name]</td>
				<td>".date("d-m-y",strtotime($row['date']))."</td>
				<td>$row[comp]</td>
				<td>$row[desct]</td>
				<td>$row[amount]</td>
			</tr>";
			
			$sn++;	
		}
		echo "
		</tbody>
		</table>";


echo "<script>
	$('#loadicon').fadeOut();
</script>";
?>