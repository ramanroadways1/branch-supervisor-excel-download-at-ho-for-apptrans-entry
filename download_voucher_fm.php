<?php 
require_once './connect.php';

$branch = $_POST['branch'];
$type = $_POST['type'];

if(empty($branch) || empty($type))
{
	echo "<script>
		alert('Branch not found !');
		window.close();
	</script>";
	exit();
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$output = '';

if($type=='ADVANCE')
{
	$query = "SELECT f.frno,f.newdate,c.truck_no,f.branch,c.company,f.ptob,
		f.adv_date,f.pto_adv_name,f.adv_pan,f.chqno,f.disadv,f.narre,f.re_download,c.lrno,c.fstation,
		c.tstation,c.actualf,c.loading,c.dsl_inc,c.gps,c.claim,c.other,c.tds,c.total_freight,c.total_adv,
		c.cash,c.cheque,c.diesel,c.rtgs,c.balance,rtgs.crn as ref_no,rtgs.bank as utr_no,rtgs.utr_date 
		FROM freight_memo_adv_cache AS f 
		LEFT OUTER JOIN freight_memo_adv_cache_lr AS c ON c.frno = f.frno 
		LEFT OUTER JOIN rtgs_fm AS rtgs ON rtgs.fno = f.frno AND rtgs.type='ADVANCE' 
		WHERE f.colset=1 AND f.branch='$branch'";
}
else
{
	$query = "SELECT f.frno,f.newdate,c.truck_no,f.branch,c.company,
		f.bal_date,f.pto_bal_name,f.bal_pan,f.paycheqno,f.paydsl,f.narra,f.re_download,c.lrno,
		c.balance,c.unloading as unloading,c.detention,c.gps_rent,c.gps_deposit_return,c.gps_device_charge,
		c.bal_tds as bal_tds,c.other2 as other_charge,
		c.bal_claim as claim,c.late_pod as late_pod,c.total_balance as total_bal,
		c.cash2 as cash2,c.cheque2 as cheque2,c.diesel2 as diesel2,
		c.rtgs2 as rtgs2,rtgs.crn as ref_no,rtgs.bank as utr_no,rtgs.utr_date 
		FROM freight_memo_bal_cache AS f 
		LEFT OUTER JOIN freight_memo_bal_cache_lr AS c ON c.frno = f.frno
		LEFT OUTER JOIN rtgs_fm AS rtgs ON rtgs.fno = f.frno AND rtgs.type='BALANCE' 
		WHERE f.colset=1 AND f.branch='$branch'";
}

$result = Qry($conn,$query);

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
	
if($type=='ADVANCE')
{
	
if(numRows($result)==0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Vou No</th>  						 
						 <th>FM Date</th>  
						 <th>Truck No</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
						 <th>Adv Date</th>  
                         <th>Adv Party Name</th>  
                         <th>PAN No</th>  
                         <th>Narration</th>  
                         <th>LR No</th>  
                         <th>From</th>  
                         <th>To</th>  
                         <th>Actual Freight</th>  
                         <th>Load(+)</th>  
                         <th>Diesel Inc(+)</th>  
                         <th>Gps(-)</th>  
                         <th>Claim(-)</th>  
                         <th>Other(-)</th>  
                         <th>TDS(-)</th>  
                         <th>Total Freight</th>  
                         <th>Advance Amt</th>  
                         <th>Cash</th>  
                         <th>Cheq</th>  
						 <th>Cheq No</th>  
                         <th>Diesel</th>  
                         <th>Rtgs</th>  
                         <th>Balance</th>   
                         <th>Ref_no</th>   
                         <th>UTR_no</th>   
                         <th>UTR_date</th>   
                         <th>Diesel_narr.</th>   
                    </tr>';
					
  while($row = fetchArray($result))
  {
	  
  if($row['disadv']>0)
  {
	  $get_diesel_nrr = Qry($conn,"SELECT GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr FROM diesel_fm WHERE fno='$row[frno]' AND 
	  type='ADVANCE' GROUP by fno");
	  $row_diesel_nrr = fetchArray($get_diesel_nrr);
	  $diesel_narr = $row_diesel_nrr['diesel_narr'];
  }
  else
  {
	  $diesel_narr="";
  }
	 
	  if($row['re_download']=='1'){
			 $output.='<tr style="background:yellow">';  
		}
		else{
			 $output.='<tr>';  
		}
		
   $output .= '
							<td>'.$row["frno"].'</td>  
							<td>'.$row["newdate"].'</td>
							<td>'.$row["truck_no"].'</td>
							<td>'.$row["branch"].'</td>
							<td>'.$row["company"].'</td>
							<td>'.$row["adv_date"].'</td>
							<td>'.$row["pto_adv_name"].'</td>
							<td>'.$row["adv_pan"].'</td>
							<td>'.$row["narre"].'</td>  
						   <td>'.$row["lrno"].'</td>
							<td>'.$row["fstation"].'</td>  
							<td>'.$row["tstation"].'</td>  
						   <td>'.$row["actualf"].'</td>
						   <td>'.$row["loading"].'</td>
						   <td>'.$row["dsl_inc"].'</td>
						   <td>'.$row["gps"].'</td>
						   <td>'.$row["claim"].'</td>
						   <td>'.$row["other"].'</td>
						   <td>'.$row["tds"].'</td>
						   <td>'.$row["total_freight"].'</td>
						   <td>'.$row["total_adv"].'</td>
						   <td>'.$row["cash"].'</td>
						   <td>'.$row["cheque"].'</td>
						   <td>'.$row["chqno"].'</td>
						   <td>'.$row["diesel"].'</td>
						   <td>'.$row["rtgs"].'</td>
						   <td>'.$row["balance"].'</td>
						   <td>'.$row["ref_no"].'</td>
						   <td>'.$row["utr_no"].'</td>
						   <td>'.$row["utr_date"].'</td>
						   <td>'.$diesel_narr.'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_ADV_'.$date.'_'.$branch.'.xls');
  echo $output;
  
$update = Qry($conn,"UPDATE freight_form SET adv_download='$timestamp' WHERE id in(SELECT tab_id FROM freight_memo_adv_cache WHERE 
colset='1' AND branch='$branch')");

if(!$update){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}  
  
$delete_cache = Qry($conn,"DELETE FROM freight_memo_adv_cache WHERE colset='1' AND branch='$branch'");

if(!$delete_cache){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$delete_cache2 = Qry($conn,"DELETE FROM freight_memo_adv_cache_lr WHERE frno IN(SELECT frno FROM freight_memo_adv_cache 
WHERE colset='1' AND branch='$branch')");

if(!$delete_cache2){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

}
else
{
	if(numRows($result)==0)
	{
		echo "<script>
			alert('No result found !');
			window.close();
		</script>";
		exit();
	}


	$output .= '
   <table border="1">  
                    <tr>  
                         <th>Vou No</th>  						 
						 <th>FM Date</th>  
						 <th>Truck No</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
						 <th>Bal Date</th>  
                         <th>Bal Party Name</th>  
                         <th>PAN No</th>  
                         <th>Narration</th>  
                         <th>LR_No</th>  
                         <th>Balance</th>  
                         <th>Unloading(+)</th>
						 <th>Detention(+)</th>  
						 <th>Gps_Deposit_Return(+)</th>  
                         <th>Gps_Rent(-)</th>  
                         <th>Gps_Device_Chrage(-)</th>  
                         <th>TDS(-)</th>  
                         <th>Other Charges(-)</th>  
                         <th>Claim(-)</th>  
                         <th>Late POD(-)</th>  
                         <th>Total Balance</th>  
                         <th>Cash</th>  
                         <th>Cheq</th>  
						 <th>Cheq No</th>  
                         <th>Diesel</th>  
                         <th>Rtgs</th>  
						 <th>Ref_no</th>   
                         <th>UTR_no</th>   
                         <th>UTR_date</th>   
                         <th>Diesel_narr.</th>   
                    </tr>
  ';
  while($row = fetchArray($result))
  {
	  
  if($row['paydsl']>0)
  {
	  $get_diesel_nrr = Qry($conn,"SELECT GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr FROM diesel_fm WHERE fno='$row[frno]' AND 
	  type='BALANCE' GROUP by fno");
	  $row_diesel_nrr = fetchArray($get_diesel_nrr);
	  $diesel_narr = $row_diesel_nrr['diesel_narr'];
  }
  else
  {
	  $diesel_narr="";
  }
  
  if($row['re_download']=='1'){
			 $output.='<tr style="background:yellow">';  
		}
		else{
			 $output.='<tr>';  
		}
		
   $output .= '
   
					<td>'.$row["frno"].'</td>  
							<td>'.$row["newdate"].'</td>
							<td>'.$row["truck_no"].'</td>
							<td>'.$row["branch"].'</td>
							<td>'.$row["company"].'</td>
							<td>'.$row["bal_date"].'</td>
							<td>'.$row["pto_bal_name"].'</td>
							<td>'.$row["bal_pan"].'</td>
							<td>'.$row["narra"].'</td>  
						   <td>'.$row["lrno"].'</td>
						   <td>'.$row["balance"].'</td>
						   <td>'.$row["unloading"].'</td>
						   <td>'.$row["detention"].'</td>
						   <td>'.$row["gps_deposit_return"].'</td>
						   <td>'.$row["gps_rent"].'</td>
						   <td>'.$row["gps_device_charge"].'</td>
						   <td>'.$row["bal_tds"].'</td>
						   <td>'.$row["other_charge"].'</td>
						   <td>'.$row["claim"].'</td>
						   <td>'.$row["late_pod"].'</td>
						   <td>'.$row["total_bal"].'</td>
						   <td>'.$row["cash2"].'</td>
						   <td>'.$row["cheque2"].'</td>
						   <td>'.$row["paycheqno"].'</td>
						   <td>'.$row["diesel2"].'</td>
						   <td>'.$row["rtgs2"].'</td>
						   <td>'.$row["ref_no"].'</td>
						   <td>'.$row["utr_no"].'</td>
						   <td>'.$row["utr_date"].'</td>
						   <td>'.$diesel_narr.'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_BAL_'.$date.'_'.$branch.'.xls');
  echo $output;
  
$update = Qry($conn,"UPDATE freight_form SET bal_download='$timestamp' WHERE id in(SELECT tab_id FROM freight_memo_bal_cache 
WHERE colset='1' AND branch='$branch')");

if(!$update){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}  
  
$delete_cache = Qry($conn,"DELETE FROM freight_memo_bal_cache WHERE colset='1' AND branch='$branch'");

if(!$delete_cache){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$delete_cache2 = Qry($conn,"DELETE FROM freight_memo_bal_cache_lr WHERE frno IN(SELECT frno FROM freight_memo_bal_cache 
WHERE colset='1' AND branch='$branch')");

if(!$delete_cache2){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

}

?>