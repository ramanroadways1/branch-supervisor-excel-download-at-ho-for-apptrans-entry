<?php
require_once 'connect.php';

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

if($type=='ADVANCE')
{
	$table_name = "freight_memo_adv_cache";
}
else
{
	$table_name = "freight_memo_bal_cache";
}

$qry = Qry($conn,"UPDATE `$table_name` SET colset='0' WHERE id='$id'");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

echo "<script>
	$('#approve_button_$id').attr('disabled',false);
	$('#approve_button_$id').html('Approve');
	
	$('#reject_button_$id').attr('disabled',true);
	$('#reject_button_$id').html('Rejected');
	$('#loadicon').hide();
</script>";
?>