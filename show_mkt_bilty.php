<?php
require_once 'connect.php';
$date2 = date("Y-m-d"); 
?>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.btn * { visibility: hidden; }
.container-fluid * { visibility: visible}
}
</style>
<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>
<?php
$lrno=strtoupper(mysqli_real_escape_string($conn,$_POST['mbno']));

$qry=mysqli_query($conn,"SELECT company,date,lrdate,bilty_no,billing_type,veh_placer,plr,broker,billing_party,tno,frmstn,tostn,cwt,rate,tamt,branch FROM mkt_bilty WHERE bilty_no='$lrno'");
if($qry)
{
	if(mysqli_num_rows($qry)==0)
	{
		echo "<center><h3>Invalid Entry</h3></center>";
		mysqli_close($conn);
		exit();
	}
	
	$row = mysqli_fetch_array($qry);
	$lrdate = date('d/m/y', strtotime($row['lrdate']));
	$mbdate = date('d/m/y', strtotime($row['date']));
	$plr = $row['plr'];
	$tno = $row['tno'];
	$frm = $row['frmstn'];
	$to = $row['tostn'];
	$cwt = $row['cwt'];
	$rate = $row['rate'];
	$tamt = $row['tamt'];
	$company = $row['company'];
	$branch = $row['branch'];
	
	if($company=="RRPL")
	{
		$path1 = "../b5aY6EZzK52NA8F/rrpl.PNG";
	}
	else
	{
		$path1 = "../b5aY6EZzK52NA8F/rr.PNG";
	}

	echo "
<div class='container-fluid' style='font-family:Verdana;font-size:12px;'>
<center><h3>Market - Bilty</h3></center>
<br />
<div class='col-md-8 col-md-offset-2'>
<table class='table table-bordered' style='font-size:12px;'>
<tr>
	<td colspan='2'>
	<img style='width:350px;height:80px' src='$path1' />
	<td>
	<b>Company :</b> $company
	<br>
	<br>
	<b>LR Date :</b> $lrdate
	</td>
	</tr>

	<tr>

		<td><b>Bilty No :</b> $lrno</td>

		<td><b>Sys Date :</b> $mbdate</td>

		<td><b>Truck No :</b> $tno</td>

	</tr>	

	
	<tr>

		<td><b>From Station :</b> $frm</td>

		<td><b>To Station :</b> $to</td>

		<td><b>Party LR No :</b> $plr</td>

	</tr>	

	<tr>
		<td><b>Charge Weight :</b> $cwt</td>	
		<td><b>Rate :</b> $rate</td>
		<td><b>Total Amount :</b> $tamt</td>
	</tr>	
	
	<tr>

		<td><b>Billing Type :</b> $row[billing_type]</td>

		<td><b>Vehicle Placer :</b> $row[veh_placer]</td>

		<td><b>Broker Name :</b> $row[broker]</td>

	</tr>	
	
	<tr>

		<td colspan='3'><b>Billing PARTY :</b> $row[billing_party]</td>

	</tr>	

	</table>
</div>
</div>

<center>
<button class='btn btn-danger' style='font-family:Verdana;letter-spacing:1px;color:#FFF;' onclick='print();'>Print Bilty</button>
</center>
<br />
<br />";	

}
else
{
echo mysqli_error($conn);
}
?>