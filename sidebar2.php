<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<style>
::-webkit-scrollbar {
    width: 11px;
}
 
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
 
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
body{overflow-x:hidden;}
.nav>li>a{text-transform:lowercase; font-size:19px; font-weight:normal; font-family: 'Baumans', cursive;}
.navbar-header{font-family: 'Verdana', cursive;}
.navbar-inverse{background-color:#07F;
border-bottom:0px solid #ddd;
}
</style>
<style>

#title_msg  {
    display: none;
}

#title_msg2 {
    display: none;
}

@media screen and (max-width: 768px) {
    #title_msg {
        display: block;
    }


.navbar-default .navbar-nav>li>a{
color:#000;
}

.navbar-default .navbar-nav>li{
border-bottom:1px solid #ccc;
}

.navbar-default .navbar-nav>li>a:hover{
background-color:#000;
color:#fff;
}

}

@media screen and (min-width: 769px) {
    #title_msg2 {
        display: block;
    }

}


#cd{
	color:black;
	letter-spacing:1px;
	padding:7px;
	font-weight:bold;
	font-size:15px;
	margin-left:5px;
	font-family:Arial;
	 transition: color 0.3s linear;
   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
	
}
#cd:hover{
	background-color:#07F;
	color:#eee;
	
}
#newid>li>a:hover{
	background:#888;
	color:#fff;
	
}
#newid>li>a{
 transition: color 0.3s linear;
 color:#06F;

   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
}
</style>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./"><span></span>R<span style="color:white;text-transform:lowercase">aman</span> 
				R<span style="color:white;text-transform:lowercase">oadways</span> P<span style="color:white;text-transform:lowercase">vt</span> 
				L<span style="color:white;text-transform:lowercase">td</span></a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <svg class="glyph stroked male-user">
                            <use xlink:href="#stroked-male-user"></use>
                        </svg> <?php echo $_SESSION['ho']; ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                       <!-- <li>
                            <a href="#">
                                <svg class="glyph stroked male-user">
                                    <use xlink:href="#stroked-male-user"></use>
                                </svg> Profile</a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="glyph stroked gear">
                                    <use xlink:href="#stroked-gear"></use>
                                </svg> Settings</a>
                        </li> -->
                        <li>
                            <a href="logout.php">
                                <svg class="glyph stroked cancel">
                                    <use xlink:href="#stroked-cancel"></use>
                                </svg> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-2 col-lg-2 sidebar">
    <br>
    <ul id="newid" class="nav menu" style="font-weight:bold; letter-spacing:0.5px;">
       <li class=""><a href="index.php">Dashboard</a></li>
			<li><a href="balanceshow.php">Accounts Balance</a></li>
			
<li class="dropdown" id="<?php if($pname=='fexp.php' || $pname=='ftruck.php') {echo "active";} ?>">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:18px"><span style="text-transform:uppercase">V</span>iew / <span style="text-transform:uppercase">D</span>ownload <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" style="width:100%;background:#CCC">
							<li id="credit"><a id="cd" href="exp_vou/">Expense Voucher</a></li>
							<li id="credit"><a id="cd" href="truck_vou/">Truck Voucher</a></li>
							<!--<li id="credit"><a id="cd" href="mkt_bilty/">Market Bilty</a></li>-->
							<li id="credit"><a id="cd" href="FM_ADV/">Advance FM</a></li>
							<li id="credit"><a id="cd" href="FM_BAL/">Balance FM</a></li>
						</ul>
</li>		

			<li><a href="dispatch.php">Dispatch</a></li>
			<li><a href="cash_book.php">Cash Book</a></li>
			<li><a href="pass_book.php">Pass Book </a></li>
			<li><a href="diesel_book.php">Diesel Book </a></li>
			
</li>

<!--			
  			<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size:18px"><span style="text-transform:uppercase">M</span>anage <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" style="width:100%;background:#CCC">
							<li id="credit"><a id="cd" href="modifyvou.php">Modify Voucher</a></li>
							<li id="credit"><a id="cd" href="drivers.php">Manage Drivers </a></li>
							<li id="credit"><a id="cd" href="brokers.php">Manage Broker </a></li>
							<li id="credit"><a id="cd" href="trucks.php">Manage Trucks </a></li>
						</ul>
			</li>
			

<li><a href="downloads.php">Download CSV</a></li>
<li><a href="password.php">Change Password</a></li>

<li><a href="dispatch.php">Dispatch Report </a></li>-->
<li><a href="logout.php">Log out</a></li>  
    </ul>

</div>