<?php
require_once 'connect.php';

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

if($type=='exp_vou')
{
	$table_name = "exp_vou_cache";
}
else
{
	$table_name = "truck_vou_cache";
}

$qry = Qry($conn,"UPDATE `$table_name` SET colset='1' WHERE id='$id'");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

echo "<script>
	$('#approve_button_$id').attr('disabled',true);
	$('#approve_button_$id').html('Approved');
	
	$('#reject_button_$id').attr('disabled',false);
	$('#reject_button_$id').html('Reject');
	$('#loadicon').hide();
</script>";
?>