<?php
require_once 'connect.php';

$id = escapeString($conn,$_POST['id']);
$type = escapeString($conn,$_POST['type']);

if($type=='ADVANCE')
{
	$qry = Qry($conn,"UPDATE ship.freight_memo_adv SET colset='1' WHERE id='$id'");
	$table_name = "freight_memo_adv";
}
else
{
	$qry = Qry($conn,"UPDATE ship,freight_memo_bal SET colset='1' WHERE id='$id'");
	$table_name = "freight_memo_bal";
}

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

echo "<script>
	$('#approve_button_$id').attr('disabled',true);
	$('#approve_button_$id').html('Approved');
	
	$('#reject_button_$id').attr('disabled',false);
	$('#reject_button_$id').html('Reject');
	$('#loadicon').hide();
</script>";
?>