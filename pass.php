<?php
require_once 'connection.php';

$branch=mysqli_real_escape_string($conn,strtoupper($_SESSION['user']));
$company=$_POST['company'];

$output='';

$date2 = date("Y-m-d"); 

if($_POST['range']=='FULL')
{
	if($company=='RRPL')
	{
$result=mysqli_query($conn,"SELECT vou_no,lrno,vou_type,date,comp,desct,chq_no,debit,credit,balance FROM passbook WHERE user='$branch' AND comp='RRPL' ORDER BY id asc");
	}
	else
	{
$result=mysqli_query($conn,"SELECT vou_no,lrno,vou_type,date,comp,desct,chq_no,debit2,credit2,balance2 FROM passbook WHERE user='$branch' AND comp='RAMAN_ROADWAYS' ORDER BY id asc");		
	}
}
else
{
$from_date=date('Y-m-d', strtotime($_POST['range'], strtotime($date2)));
$to_date=date("Y-m-d");	

if($company=='RRPL')
	{
$result=mysqli_query($conn,"SELECT vou_no,lrno,vou_type,date,comp,desct,chq_no,debit,credit,balance FROM passbook WHERE date BETWEEN '$from_date' AND '$to_date' AND user='$branch' AND comp='RRPL' ORDER BY id asc");
	}
	else
	{
$result=mysqli_query($conn,"SELECT vou_no,lrno,vou_type,date,comp,desct,chq_no,debit2,credit2,balance2 FROM passbook WHERE date BETWEEN '$from_date' AND '$to_date' AND user='$branch' AND comp='RAMAN_ROADWAYS' ORDER BY id asc");		
	}
}

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
		alert('No result found...');
		window.location.href='./passbook.php';
		</script>";
		mysqli_close($conn);
		exit();
}

if(isset($_POST['download']))
{
	 $output .= '
   <table border="1">  
       <tr>  
        <th>Vou No</th>
		<th>LR No</th>
		<th>Vou Type</th>
		<th>Date</th>
		<th>Company</th>
		<th>Particulars</th>
		<th>Cheq No</th>
		<th>Debit</th>
		<th>Credit</th>
		<th>Balance</th>
	</tr>
  ';
  while($row=mysqli_fetch_array($result))
  {
  if($company=='RRPL')
  {
	$dr=$row['debit'];  
	$cr=$row['credit'];  
	$bal=$row['balance'];  
  }
  else if($company=='RAMAN_ROADWAYS')
  {
	$dr=$row['debit2'];  
	$cr=$row['credit2'];  
	$bal=$row['balance2'];   
  }
	  
   $output .= '
				<tr> 
							<td>'.$row["vou_no"].'</td> 
							<td>'.$row["lrno"].'</td> 
							<td>'.$row["vou_type"].'</td> 
							<td>'.$row["date"].'</td>  
							<td>'.$row["comp"].'</td>  
							<td>'.$row["desct"].'</td>  
							<td>'.$row["chq_no"].'</td>  
							<td>'.$dr.'</td>  
							<td>'.$cr.'</td>  
							<td>'.$bal.'</td>  
				</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Passbook_'.$company.'.xls');
  echo $output;
  exit();
}
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
.form-control
{
	border:1px solid #000;
}
</style> 

</head>

<body style="background-color:#078388;font-family:Verdana">   

<a href="./passbook.php"><button class="btn btn-warning" style="color:#000;margin-top:10px;margin-left:10px;letter-spacing:1px"><b>Go Back</b></button></a>
<a href="./"><button class="btn btn-warning" style="color:#000;margin-top:10px;margin-left:10px;letter-spacing:1px"><b>Dashboard</b></button></a>  

        <div class="container-fluid">
            <div class="col-md-12">
                <center>
				<h4 style="font-family:Baumans;color:#FFF;font-size:24px;">Passbook - Summary</h4>
                    </center>
                    <div style="overflow-x:auto" class="panel-body table-responsive">                         
<?php
echo "<table class='table table-bordered' style='font-size:12px;background-color:#FFF;'>";
echo "
<tr>
<th>Vou No</th>
<th>LR No</th>
<th>Vou Type</th>
<th>Date</th>
<th>Company</th>
<th>Particulars</th>
<th>Cheq No</th>
<th>Debit</th>
<th>Credit</th>
<th>Balance</th>
</tr>";

while($row=mysqli_fetch_array($result))
  {
if($company=='RRPL') 
{
	$dr=$row['debit'];  
	$cr=$row['credit'];  
	$bal=$row['balance'];  
}
else
{
	$dr=$row['debit2'];  
	$cr=$row['credit2'];  
	$bal=$row['balance2'];   		
}	  
$dt1=date('d-M-y', strtotime($row['date']));  

echo "<tr>";
$vou_type=$row['vou_type'];
$vou_no=$row['vou_no'];

if($vou_type=="Freight_Memo")
{
echo "<td>
<form action='smemo2.php' target='_blank' method='POST'>
<input type='hidden' name='idmemo' value='$vou_no' />
<input type='hidden' name='key' value='FM' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else if($vou_type=="Expense_Voucher"){
echo "<td>
<form action='showvou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else if($vou_type=="Truck_Voucher"){
echo "<td>
<form action='showvou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else
{
echo "<td>$vou_no</td>";
}
echo "
<td>$row[lrno]</td>
<td>$vou_type</td>
<td>$dt1</td>
<td>$row[comp]</td>
<td>$row[desct]</td>
<td>$row[chq_no]</td>
<td>$dr</td>
<td>$cr</td>
<td>$bal</td>
</tr>";
}
echo "</table>";
?>             
                </div>
            </div>
        </div>
       
</body>
</html>