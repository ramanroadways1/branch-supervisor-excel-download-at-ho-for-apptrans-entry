<style>
::-webkit-scrollbar{
    width: 4px;
    height: 4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

body{overflow-x:hidden;}
.nav>li>a{font-size:12.5px; font-family: 'Open Sans', sans-serif !important;}
.navbar-header{font-family: 'Open Sans', sans-serif !important;}
.navbar-inverse{background-color:#299C9B;}

.navbar-default .navbar-nav>li>a{
color:#000;
}

.navbar-default .navbar-nav>li{
border-bottom:1px solid #ccc;
}

.navbar-default .navbar-nav>li>a:hover{
background-color:#000;
color:#fff;
}

#cd{
	color:black;
	padding:8px;
	font-size:12px;
	font-family:'Open Sans', sans-serif !important;
	transition: color 0.3s linear;
   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
	
}

#cd:hover{
	background-color:#07F;
	color:#FFF;
}

#newid>li>a:hover{
	background:#299C9B;
	color:#fff;
	
}
#newid>li>a{
 transition: color 0.3s linear;
 color:#333;
 
-webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
}

#newid>#active{
	background:lightblue;
}
</style>

<?php 
$pname = basename($_SERVER['PHP_SELF']);
?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
			
<a class="navbar-brand" href="<?php echo $baseUrl1 ?>" style="letter-spacing:0px;font-family: 'Open Sans', sans-serif !important">
<!--<span style="color:#FFF" class="glyphicon glyphicon-home"></span> &nbsp; UMMEED (RRPL)-->
<img style="margin-top:-13px;margin-left:-15px;width:160px;height:48px;" src="<?php echo $baseUrl_main ?>logo_final.jpg" /> 
</a>
            <ul class="user-menu">
				<li class="dropdown pull-right" style="font-size:13px;letter-spacing:0px;font-family: 'Open Sans', sans-serif !important">
					<a href="#" id="user_link" class="dropdown-toggle" data-toggle="dropdown">
						<span class="fa fa-user-circle-o" style="font-size:17px"></span> &nbsp;<?php echo strtoupper($_SESSION['ho']); ?> &nbsp;<span class="fa fa-angle-down"></span></a> 
					<ul class="dropdown-menu" role="menu">
						<li><a style="font-size:13px;" href="<?php echo $baseUrl1 ?>change_password.php"><span class="fa fa-user-secret"></span> Change Password </a></li>
						<li><a style="font-size:13px;" href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span> Log out </a></li>
					</ul>
				</li>
			</ul>
        </div>
</nav>

<div id="sidebar-collapse" class="sidebar form-group col-md-2 col-sm-3">

   <ul id="newid" class="nav menu" style="font-weight:; letter-spacing:0.5px">
			<li id="<?php if($pname=='index.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>"><span class="fa fa-home" style="font-size:16px;"></span>  Dashboard</a></li>
			
	<li id="<?php if($pname=='balanceshow.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>balanceshow.php"><span class="fa fa-file-text" style="font-size:14px;"></span>&nbsp; Branch Balance</a></li>

	<li class="dropdown" id="<?php if($pname=='exp_vou.php' || $pname=='truck_vou.php' || $pname=='fm_adv.php' || $pname=='fm_bal.php') {echo "active";} ?>">
		<a class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-tachometer"></span>&nbsp; View/Download <span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu" style="width:100%;background:#EEE">
			<li><a id="cd" href="<?php echo $baseUrl1 ?>exp_vou.php">Expense Voucher</a></li>
			<li><a id="cd" href="<?php echo $baseUrl1 ?>truck_vou.php">Truck Voucher</a></li>
			<li><a id="cd" href="<?php echo $baseUrl1 ?>fm_adv.php">Advance FM</a></li>
			<li><a id="cd" href="<?php echo $baseUrl1 ?>fm_bal.php">Balance FM</a></li>
		</ul>
	</li>
			

<!--<li id="<?php if($pname=='lr_entry.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>lr_entry.php"><span class="fa fa-book"></span>&nbsp; LR Entry</a></li>-->
<li id="<?php if($pname=='cash_book.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>cash_book.php"><span class="fa fa-book"></span>&nbsp; Cash Book</a></li>
<li id="<?php if($pname=='pass_book.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>pass_book.php"><span class="fa fa-book"></span>&nbsp; Pass Book</a></li>
<li id="<?php if($pname=='diesel_book.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>diesel_book.php"><span class="fa fa-battery-full"></span>&nbsp; Diesel Book</a></li>
<li id="<?php if($pname=='neft_book.php') {echo "active";} ?>"><a href="<?php echo $baseUrl1 ?>neft_book.php"><span class="fa fa-university"></span>&nbsp; NEFT Sheet</a></li>

<li id="<?php if($pname=='bank_wdl.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>bank_wdl.php"><span class="fa fa-university" style="font-size:14px;"></span>&nbsp; Bank Withdrawal</a></li>

<li id="<?php if($pname=='edit_pan.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>edit_pan.php"><span class="fa fa-edit" style="font-size:14px;"></span>&nbsp; Edit PAN</a></li>				

	<li id="<?php if($pname=='view_declaration.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>view_declaration.php"><span class="fa fa-file-text" style="font-size:14px;"></span>&nbsp; Declaration Form</a></li>	
	
<li id="<?php if($pname=='forfeit_balance.php') {echo "active";} ?>">
				<a href="<?php echo $baseUrl1 ?>forfeit_balance.php"><span class="fa fa-refresh" style="font-size:14px;"></span>&nbsp; Forfeit Balance</a></li>
				
				<li><a href="<?php echo $baseUrl1 ?>logout.php"><span class="fa fa-sign-out"></span>&nbsp; Log out</a></li>
		</ul>
</div>