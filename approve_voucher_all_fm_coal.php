<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$type = escapeString($conn,$_POST['type']);

if($type=='ADVANCE')
{
	$qry = Qry($conn,"UPDATE  ship.freight_memo_adv SET colset='1' WHERE colset_d!='1' AND rtgs_adv='1' AND branch='$branch'");
	$table_name = "freight_memo_adv";
	$rtgs_col = "rtgs_adv";
}
else
{
	$qry = Qry($conn,"UPDATE ship.freight_memo_bal t1 
				INNER JOIN ship.freight_memo_adv t2 
				ON t1.fm_no = t2.fm_no
				SET t1.colset = '1' 
				WHERE t1.colset_d!='1' AND t1.rtgs_bal='1' AND t1.fm_no = t2.fm_no AND t2.colset_d='1' AND t2.branch='$branch'");
				
				
	// $qry = Qry($conn,"UPDATE ship.freight_memo_bal SET colset='1' WHERE colset_d!='1' AND rtgs_bal='1' AND branch='$branch'");
	// $table_name = "freight_memo_bal";
	// $rtgs_col = "rtgs_bal";
}

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

echo "<script>
	$('#approve_all_button').attr('disabled',true);
	$('#approve_all_button').html('All Approved');
	
	$('.approve_button').attr('disabled',true);
	$('.approve_button').html('Approved');
	
	$('.reject_button').attr('disabled',false);
	$('.reject_button').html('Reject');
	$('#loadicon').hide();
</script>";
?>