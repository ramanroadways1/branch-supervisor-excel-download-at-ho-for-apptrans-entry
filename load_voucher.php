<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$type = escapeString($conn,$_POST['type']);
$timestamp = date("Y-m-d H:i:s");

if($type=='exp_vou')
{
	$qry = Qry($conn,"INSERT INTO exp_vou_cache(tab_id,user,vno,newdate,date,comp,des,desid,amt,amt_w,chq,chq_no,chq_bnk_n,neft_bank,
	neft_acname,neft_acno,neft_ifsc,pan,narrat,empcode,vehno,etype,cash_sign,rcvr_sign,timestamp,upload,colset,colset_d,
	timestamp_download,re_download) SELECT id,user,vno,newdate,date,comp,des,desid,amt,amt_w,chq,chq_no,chq_bnk_n,neft_bank,
	neft_acname,neft_acno,neft_ifsc,pan,narrat,empcode,vehno,etype,cash_sign,rcvr_sign,timestamp,upload,colset,colset_d,
	timestamp_download,IF(colset='1','1','0') FROM mk_venf WHERE colset_d=0 AND user='$branch'");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$update = Qry($conn,"UPDATE mk_venf SET colset='1',colset_d='1',timestamp_download='$timestamp' WHERE id in(SELECT tab_id FROM 
	exp_vou_cache WHERE user='$branch')");
	if(!$update){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$get_cache = Qry($conn,"SELECT id,vno,newdate,date,comp,des,amt,chq,colset,re_download FROM exp_vou_cache WHERE user='$branch'");
	if(!$get_cache){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache)==0)
	{
		echo "
		<br />
		<center><h4 style='color:red'>No Pending Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
	}
	else
	{
		echo "<form action='download_voucher.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	 
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Vou_date</th>
				<th>Created_On</th>
				<th>Company</th>
				<th>Exp_Head</th>
				<th>Amount</th>
				<th>Pay.Mode</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			if($row['re_download']=="1"){
				echo "<tr class='bg-warning'>";
			}
			else{
				echo "<tr>";
			}
			
			echo "
				<td>$sn</td>
				<td>$row[vno]</td>
				<td>$row[newdate]</td>
				<td>$row[date]</td>
				<td>$row[comp]</td>
				<td>$row[des]</td>
				<td>$row[amt]</td>
				<td>$row[chq]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
	}
}
else
{
	$qry = Qry($conn,"INSERT INTO truck_vou_cache(tab_id,user,company,tdvid,date,newdate,truckno,dname,amt,amtw,mode,chq_no,
	chq_bank,ac_name,ac_no,bank,ifsc,pan,dest,nar,naro,timestamp,colset,colset_d,timestamp_download,re_download) 
	SELECT id,user,company,tdvid,date,newdate,truckno,dname,amt,amtw,mode,chq_no,chq_bank,ac_name,ac_no,bank,ifsc,pan,dest,
	nar,naro,timestamp,colset,colset_d,timestamp_download,IF(colset='1','1','0') FROM mk_tdv WHERE colset_d=0 
	AND user='$branch'");
	
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$update = Qry($conn,"UPDATE mk_tdv SET colset='1',colset_d='1',timestamp_download='$timestamp' WHERE id in(SELECT tab_id FROM 
	truck_vou_cache WHERE user='$branch')");
	if(!$update){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$get_cache = Qry($conn,"SELECT id,tdvid,company,date,newdate,truckno,dname,amt,mode,colset,re_download FROM truck_vou_cache WHERE user='$branch'");
	if(!$get_cache){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache)==0)
	{
		echo "
		<br />
		<center><h4 style='color:red'>No Pending Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
	}
	else
	{
		echo "<form action='download_voucher.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Truck_No</th>
				<th>Vou_date</th>
				<th>Created_On</th>
				<th>Company</th>
				<th>Driver_Name</th>
				<th>Amount</th>
				<th>Pay.Mode</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			if($row['re_download']=="1"){
				echo "<tr class='bg-warning'>";
			}
			else{
				echo "<tr>";
			}
			
			echo "
				<td>$sn</td>
				<td>$row[tdvid]</td>
				<td>$row[truckno]</td>
				<td>$row[newdate]</td>
				<td>$row[date]</td>
				<td>$row[company]</td>
				<td>$row[dname]</td>
				<td>$row[amt]</td>
				<td>$row[mode]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
	}
}

echo "<script>
	$('#loadicon').fadeOut();
</script>";
?>

<script>
function ApproveAll(branch)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher_all.php",
			data: 'branch=' + branch + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}

function Approve(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}

function Reject(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./reject_voucher.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}
</script>