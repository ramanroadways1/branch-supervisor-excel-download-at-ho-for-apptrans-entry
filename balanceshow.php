<?php
require_once 'connect.php';
?>	
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
}
</style>

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">			

<div class="row">
	<div class="form-group col-md-12">
	<br />
		<h4 style="letter-spacing:1px;color:#FFF;font-weight:bold;font-size:18px">Branch Cash balance: </h4>
<table class='table table-bordered table-striped' style='color:#000;font-size:13px'>
	<tr style="background:#299C9B;font-size:13px;color:#FFF">
		<th>Branch </th>
        <th>RRPL Cash</th>
		<th>RR Cash</th>
	</tr>	
<?php
$sql = Qry($conn,"SELECT username,balance,balance2 FROM user WHERE username in($limit) order by username asc");

while($row = fetchArray($sql))
{
      echo "<tr>
		<td>$row[username]</td>
		<td>$row[balance]</td>
		<td>$row[balance2]</td>
	</tr>";
}
echo "</table>";        
?>
</div>
</div>
</div>
</div>

</body>
</html>