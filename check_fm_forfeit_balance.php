<?php 
require_once './connect.php';

$fm_no = escapeString($conn,strtoupper($_POST['fm_no']));
$timestamp = date("Y-m-d H:i:s");

if($fm_no=='')
{
	echo "<option value=''>--select--</option>
	<script>
		alert('Invalid FM number !');
		$('#loadicon').fadeOut('slow');
		$('#fm_no').attr('readonly',false);
	</script>";
	exit();
}

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $fm_no))
{
	echo "<script>
		alert('Error : Check Freight memo number !');
		$('#loadicon').fadeOut('slow');
		$('#fm_no').attr('readonly',false);
	</script>";
	exit();
}
	
$get_fm = Qry($conn,"SELECT f.id,f.bid,f.oid,b.name as broker_name,b.pan as broker_pan,o.name as owner_name,o.pan as owner_pan 
FROM freight_form AS f 
LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
WHERE f.frno = '$fm_no'");

if(!$get_fm){
	
	echo "<option value=''>--select--</option>
	<script>
		alert('Error !');
		$('#loadicon').fadeOut('slow');
		$('#fm_no').attr('readonly',false);
	</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_fm)==0){
	
	echo "<option value=''>--select--</option>
	<script>
		alert('Invalid FM number !');
		$('#loadicon').fadeOut('slow');
		$('#fm_no').attr('readonly',false);
	</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row = fetchArray($get_fm);

echo "
	<option value=''>--select--</option>
	<option value='BROKER_$row[bid]_$row[id]'>Broker : $row[broker_name] ($row[broker_pan])</option>
	<option value='OWNER_$row[oid]_$row[id]'>Owner : $row[owner_name] ($row[owner_pan])</option>
	<script>
		$('#loadicon').fadeOut('slow');
		$('#fm_no').attr('readonly',true);
		$('#fm_no').attr('onblur','');
	</script>";
?>
