<?php
require_once 'connect.php';	

$ho = $_SESSION['ho'];
$q_99 = mysqli_query($conn,"SELECT title,branch FROM user WHERE username='$ho'");
$row_99 = mysqli_fetch_array($q_99);

$limit= $row_99['title'];
$branches= $row_99['branch'];

$sql = "SELECT username FROM user WHERE username in($limit) AND role='2' ORDER BY username ASC";
$result = mysqli_query($conn,$sql);
$max = date("Y-m-d");
?>	
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>RRPL</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control{
	border:1px solid #000;
	background:#FFF;
}
label{color:#000;font-family:Verdana;font-size:13px;}
</style>
</head>

<body style="background:lightblue;font-family:Verdana">
<?php include 'sidebar.php';?>
<div class="col-sm-12 col-lg-5 col-lg-offset-4">
<br />
	<center>
        <h3 style="color:#000">View Passbook</h3>
	</center>
	<br />
<form action="pass_book2.php" method="POST">  	
<div class="row">
	<div class="form-group">
		<label class="col-md-3 control-label">Company <font color="red">*</font></label>
	
	<div class="col-md-8">
		<select class="form-control" name="company" required="required">
		   <option value="">Select Company</option>
		   <option value="RRPL">RRPL</option>
		   <option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</OPTION>
		</select>
	</div>
    </div>
</div>
<br />
<div class="row">
	<div class="form-group">
	<label class="col-md-3 control-label">Branch <font color="red">*</font></label>
    <div class="col-md-8">
	<select class="form-control" name="branch" required="required">
		<option value="">Select Branch</option>
		<option value="ALL">All Branches</option>
	<?php 
	while($row=mysqli_fetch_array($result))
	{ 
	echo "<option value='".$row['username']."'>".$row['username']."</option>";
	}
	?>          
	</select>
	</div>
	</div>
</div>
<br />							
<div class="row">
	<div class="form-group">
	<label class="col-md-3 control-label">From Date <font color="red">*</font></label>
	<div class="col-md-8">
		<input name="from" type="date" max="<?php echo $max; ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	</div>
</div>
	<br />
<div class="row">
	<div class="form-group">
	<label class="col-md-3 control-label">To Date <font color="red">*</font></label>
	<div class="col-md-8">
		<input name="to" type="date" max="<?php echo $max; ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
	</div>
	</div>						
</div>						
<br />
<div class="row">
	<div class="form-group">
	<div class="col-md-11">
		<button type="submit" style="letter-spacing:1px;" class="btn btn-danger pull-right" name="submit">View Passbook</button>
	</div>
	</div>
</div>
</form>
</div>
</body>
</html>