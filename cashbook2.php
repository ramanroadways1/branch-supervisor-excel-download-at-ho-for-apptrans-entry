<?php
require_once 'connect.php';

$ho = $_SESSION['ho'];
$q_99 = mysqli_query($conn,"SELECT title,branch FROM user WHERE username='$ho'");
$row_99 = mysqli_fetch_array($q_99);

$limit= $row_99['title'];
$branches= $row_99['branch'];

$from_date=$_POST['from'];
$to_date=$_POST['to'];
$branch=$_POST['branch'];
$company=$_POST['company'];
?>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RRPL</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 			
</head>
<body style="background-color:lightblue;font-family:Verdana">   
<a href="./cash_book.php"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:1px">Go Back</button></a>
<a href="./"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:1px">Dashboard</button></a>  
<div class="container-fluid">
            <div class="col-md-12 col-sm-12">
                <center>
					<span style="font-size:20px;font-family:Verdana">Cashbook Summary</span>
				</center>
					<div class="panel-body table-responsive" style="font-family:Verdana;font-size:13px">                         
<a href="download_cashbook.php?branch=<?php echo $branch; ?>&from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&company=<?php echo $company; ?>">
<button class="btn btn-danger pull-right">Download Cashbook</button></a>
<br />
<br />
<br />
<?php
if($company=='RRPL') 
{
if($branch=="ALL")	
{
$result = mysqli_query($conn,"SELECT * FROM cashbook WHERE date between '$from_date' AND '$to_date' AND user in($limit) AND comp='RRPL' AND (debit!='0' OR credit!='0') ORDER BY id ASC");
}
else
{
$result = mysqli_query($conn,"SELECT * FROM cashbook WHERE date between '$from_date' AND '$to_date' AND user='$branch' AND comp='RRPL' AND (debit!='0' OR credit!='0') ORDER BY id ASC");	
}
if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
	alert('No cashbook entry found..'); 
	location.href='cash_book.php'; 
	</script>";
	exit();
}
echo "<table class='table table-bordered' style='font-size:14px'>";
echo "<tr>";
echo "<td><strong>";
echo "Vou No";
echo "</td></strong>";
echo "<td><strong>";
echo "Branch";
echo "</td></strong>";
echo "<td><strong>";
echo "Vou Type";
echo "</td></strong>";
echo "<td><strong>";
echo "SysDate";
echo "</td></strong>";
echo "<td><strong>";
echo "Vou Date";
echo "</td></strong>";
echo "<td><strong>";
echo "Company";
echo "</td></strong>";
echo "<td><strong>";
echo "Particulars";
echo "</td></strong>";
echo "<td><strong>";
echo "Debit";
echo "</td></strong>";
echo "<td><strong>";
echo "Credit";
echo "</td></strong>";
echo "<td><strong>";
echo "Balance";
echo "</td></strong>";
echo "</tr>";

while($row = mysqli_fetch_array($result))
  {
$dt1 = date('d-M-y', strtotime($row['date']));  

echo "<tr>";
$vou_type=$row['vou_type'];
$vou_no=$row['vou_no'];

if($vou_type=="Freight_Memo") {
$qry11=mysqli_query($conn,"SELECT newdate FROM freight_form where frno='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];	
echo "<td>
<form action='show_fm.php' target='_blank' method='POST'>
<input type='hidden' name='idmemo' value='$vou_no' />
<input type='hidden' name='key' value='FM' />
<input class='btn btn-primary btn-sm' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else if($vou_type=="Truck_Voucher")
{
$qry11=mysqli_query($conn,"SELECT newdate FROM mk_tdv where tdvid='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];		
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else if($vou_type=="Expense_Voucher")
{
$qry11=mysqli_query($conn,"SELECT newdate FROM mk_venf where vno='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else
{
$vou_date=$dt1;	
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}

$dt2 = date('d-M-y', strtotime($vou_date));  

echo "<td>";
echo $row['user'];
echo "</td>";
echo "<td>";
echo $vou_type;
echo "</td>";
echo "<td>";
echo $dt1;
echo "</td>";
echo "<td>";
echo $dt2;
echo "</td>";
echo "<td>";
echo $row['comp'];
echo "</td>";
echo "<td>";
echo $row['desct'];
echo "</td>";
echo "<td>";
echo $row['debit'];
echo "</td>";
echo "<td>";
echo $row['credit'];
echo "</td>";
echo "<td>";
echo $row['balance'];
echo "</td>";
  }
echo "</tr>";
echo "</table>";
}
else{
if($branch=="ALL")	
{
$result = mysqli_query($conn,"SELECT * FROM cashbook WHERE date between '$from_date' AND '$to_date' AND user in($limit) AND comp='RAMAN_ROADWAYS' AND (debit2!='0' OR credit2!='0') ORDER BY id ASC");
}
else
{
$result = mysqli_query($conn,"SELECT * FROM cashbook WHERE date between '$from_date' AND '$to_date' AND user='$branch' AND comp='RAMAN_ROADWAYS' AND (debit2!='0' OR credit2!='0') ORDER BY id ASC");	
}
if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
	alert('No cashbook entry found..'); 
	location.href='cash_book.php'; 
	</script>";
	exit();
}

echo "<table class='table table-bordered' style='font-size:14px'>";
echo "<tr>";
echo "<td><strong>";
echo "Vou No";
echo "</td></strong>";
echo "<td><strong>";
echo "Branch";
echo "</td></strong>";
echo "<td><strong>";
echo "Vou Type";
echo "</td></strong>";
echo "<td><strong>";
echo "SysDate";
echo "</td></strong>";
echo "<td><strong>";
echo "Vou Date";
echo "</td></strong>";
echo "<td><strong>";
echo "Company";
echo "</td></strong>";
echo "<td><strong>";
echo "Particulars";
echo "</td></strong>";
echo "<td><strong>";
echo "Debit";
echo "</td></strong>";
echo "<td><strong>";
echo "Credit";
echo "</td></strong>";
echo "<td><strong>";
echo "Balance";
echo "</td></strong>";
echo "</tr>";

while($row = mysqli_fetch_array($result))
 {
$dt1 = date('d-M-y', strtotime($row['date']));  
echo "<tr>";
$vou_type=$row['vou_type'];
$vou_no=$row['vou_no'];

if($vou_type=="Freight_Memo") {
$qry11=mysqli_query($conn,"SELECT newdate FROM freight_form where frno='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];	
echo "<td>
<form action='show_fm.php' target='_blank' method='POST'>
<input type='hidden' name='idmemo' value='$vou_no' />
<input type='hidden' name='key' value='FM' />
<input class='btn btn-primary btn-sm' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else if($vou_type=="Truck_Voucher")
{
$qry11=mysqli_query($conn,"SELECT newdate FROM mk_tdv where tdvid='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];		
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else if($vou_type=="Expense_Voucher")
{
$qry11=mysqli_query($conn,"SELECT newdate FROM mk_venf where vno='$vou_no'");
$row11=mysqli_fetch_array($qry11);
$vou_date=$row11['newdate'];
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}
else
{
$vou_date=$dt1;	
echo "<td>
<form action='show_vou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<input class='btn btn-success btn-sm' style='color:black' type='submit' name='submit' value='$vou_no' />
</form>
</td>";
}

$dt2 = date('d-M-y', strtotime($vou_date)); 

echo "<td>";
echo $row['user'];
echo "</td>";
echo "<td>";
echo $vou_type;
echo "</td>";
echo "<td>";
echo $dt1;
echo "</td>";
echo "<td>";
echo $dt2;
echo "</td>";
echo "<td>";
echo $row['comp'];
echo "</td>";
echo "<td>";
echo $row['desct'];
echo "</td>";
echo "<td>";
echo $row['debit2'];
echo "</td>";
echo "<td>";
echo $row['credit2'];
echo "</td>";
echo "<td>";
echo $row['balance2'];
echo "</td>";
  }
echo "</tr>";
echo "</table>";
}

?>             
                </div>
            </div>
        </div>
</body>
</html>