<?php
require_once 'connect.php';

if(!isset($_POST['submit'])){
	echo "<script>
		window.location.href='./';
	</script>";
	exit();
}

$branch = escapeString($conn,$_POST['branch']);
$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);
$company = escapeString($conn,$_POST['company']);
?>
<!doctype html>
<html lang="en">
<head>	
	<title>Raman Roadways Pvt Ltd</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
	
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
	
	<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" rel="stylesheet">
</head>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<body style="background:;font-family: 'Open Sans', sans-serif !important">

<a href="./"><button type="button" style="margin-left:10px;margin-top:10px" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>

<div class="container-fluid">
	<div class="row">
		<div class="form-group col-md-12">
			<center>
				<font size="3"><span style="color:blue">Bank Withdrawal :</span> <?php echo $branch; ?> Branch !</font> 
			</center>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12 table-responsive" id="result_div"></div>
	</div>
</div>

<script type="text/javascript">
function FetchData(){
	$("#loadicon").show();
		jQuery.ajax({
			url: "./load_bank_wdl_server.php",
			data: 'branch=' + '<?php echo $branch; ?>' + '&from_date=' + '<?php echo $from_date; ?>' + '&to_date=' + '<?php echo $to_date; ?>' + '&company=' + '<?php echo $company; ?>',
			type: "POST",
			success: function(data){
				$("#result_div").html(data);
				 $('#example').DataTable({ 
				 dom: 'Bfrtip',
					buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
					],
                 "destroy": true, //use for reinitialize datatable
              });
			},
			error: function() {}
	});
}
 $(document).ready(function(){
 FetchData();
});

// $(document).ready(function() {
    // $('#example').DataTable( {
        // dom: 'Bfrtip',
        // buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    // } );
// } );
</script>


</body>
</html>

<div id="result_div2"></div>