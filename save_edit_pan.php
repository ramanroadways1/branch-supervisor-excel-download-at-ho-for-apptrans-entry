<?php
require_once("./connect.php");

$party_type = escapeString($conn,($_POST['party_type']));
$broker_id = escapeString($conn,($_POST['broker_id']));
$owner_id = escapeString($conn,($_POST['owner_id']));
$current_pan = escapeString($conn,($_POST['current_pan']));
$new_pan = escapeString($conn,($_POST['new_pan']));

if(strlen($new_pan) != 10)
{
	echo "<script>
			alert('Check PAN nummber !');
			$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
		</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $new_pan))
{
	echo "<script>
			alert('Invalid PAN nummber !');
			$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
		</script>";
	exit();
}

if($party_type!='OWNER' AND $party_type!='BROKER')
{
	echo "<script>
		alert('Warning : Invalid party type !');
		$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
	</script>";
	exit();
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

if($party_type=='OWNER')
{
	$table_name = "mk_truck";	
	$table_id = $owner_id;	
	
	$qry = Qry($conn,"SELECT pan FROM mk_truck WHERE id='$table_id'");

	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
}
else
{
	$table_name = "mk_broker";
	$table_id = $broker_id;	

	$qry = Qry($conn,"SELECT pan FROM mk_broker WHERE id='$table_id'");

	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "<script>
		alert('Party memo found !!');
		$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
	</script>";
	exit();
}

$row = fetchArray($qry);

if($row['pan'] != $current_pan)
{
	echo "<script>
		alert('Warning : Party not verified !');
		$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
	</script>";
	exit();
}

if($current_pan == $new_pan)
{
	echo "<script>
		alert('Warning : Nothing to update !');
		$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', true);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

if($party_type=="OWNER")
{
	$update_owner_pan = Qry($conn,"UPDATE mk_truck SET pan='$new_pan' WHERE pan='$current_pan'");
	
	if(!$update_owner_pan){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}
else
{
	$update_broker_pan = Qry($conn,"UPDATE mk_broker SET pan='$new_pan' WHERE pan='$current_pan'");
	
	if(!$update_broker_pan){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}

$update_freight_memo_adv = Qry($conn,"UPDATE freight_form SET adv_pan='$new_pan' WHERE adv_pan='$current_pan'");
	
if(!$update_freight_memo_adv){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	$flag = false;
}

$update_freight_memo_bal = Qry($conn,"UPDATE freight_form SET bal_pan='$new_pan' WHERE bal_pan='$current_pan'");
	
if(!$update_freight_memo_bal){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	$flag = false;
}

$update_rtgs = Qry($conn,"UPDATE rtgs_fm SET pan='$new_pan' WHERE pan='$current_pan'");
	
if(!$update_rtgs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	$flag = false;
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Updated successfully !');
		window.location.href='./edit_pan.php';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error while processing request !');
		$('#loadicon').fadeOut('slow');
		$('#pan_update_btn').attr('disabled', false);
	</script>";
	exit();
}	
?>