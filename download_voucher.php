<?php 
require_once './connect.php';

$branch = $_POST['branch'];
$type = $_POST['type'];
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$output = '';

if($type=='exp_vou')
{
	$query = "SELECT user,vno,newdate,comp,des,amt,chq,chq_no,chq_bnk_n,neft_bank,neft_acname,neft_acno,neft_ifsc,pan,narrat,
	re_download FROM exp_vou_cache WHERE colset='1' AND user='$branch'";
}
else
{
	$query = "SELECT user,company,tdvid,newdate,truckno,dname,amt,mode,chq_no,chq_bank,ac_name,ac_no,bank,ifsc,pan,dest,naro,re_download FROM 
	truck_vou_cache WHERE colset='1' AND user='$branch'";
}

$result = Qry($conn,$query);

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
	
if($type=='exp_vou')
{
	
if(numRows($result)==0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Branch</th>  
                         <th>Vou No</th>  
                         <th>Vou Date</th>  
                         <th>Company</th>  
                         <th>Description</th>  
                         <th>Amount</th>  
                         <th>Cash/Cheque/Rtgs</th>  
                         <th>Chq No</th>  
                         <th>Chq Bank</th>  
                         <th>Ac Name</th>  
                         <th>Ac No.</th>  
                         <th>Bank</th>  
                         <th>IFSC Code</th>  
                         <th>PAN No.</th>  
                         <th>Narration</th>  
                    </tr>';
					
  while($row = fetchArray($result))
  {
	   if($row['chq']=='NEFT')
	  {
		  $acno="'".$row['neft_acno'];
	  }
	  else
	  {
		  $acno='';
	  }
	
		if($row['re_download']=='1'){
			 $output.='<tr style="background:yellow">';  
		}
		else{
			 $output.='<tr>';  
		}
  
			 $output .= '<td>'.$row["user"].'</td>  
							<td>'.$row["vno"].'</td>  
							<td>'.$row["newdate"].'</td>  
						   <td>'.$row["comp"].'</td>  
						   <td>'.$row["des"].'</td>
						   <td>'.$row["amt"].'</td>
						   <td>'.$row["chq"].'</td>
						   <td>'.$row["chq_no"].'</td>
						   <td>'.$row["chq_bnk_n"].'</td>
						   <td>'.$row["neft_acname"].'</td>
						   <td>'.$acno.'</td>
						   <td>'.$row["neft_bank"].'</td>
						   <td>'.$row["neft_ifsc"].'</td>
						   <td>'.$row["pan"].'</td>
						   <td>'.$row["narrat"].'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Exp_vou_'.$date.'_'.$branch.'.xls');
  echo $output;
  
$update = Qry($conn,"UPDATE mk_venf SET timestamp_download='$timestamp' WHERE id in(SELECT tab_id FROM exp_vou_cache WHERE colset='1' 
AND user='$branch')");

if(!$update){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}  
  
$delete_cache = Qry($conn,"DELETE FROM exp_vou_cache WHERE colset='1' AND user='$branch'");

if(!$delete_cache){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

}
else
{
	$output .= '
   <table border="1">  
                    <tr>  
                         <th>Branch</th>  
						 <th>Company</th>  
                         <th>Vou No</th>  
                         <th>Vou Date</th>  
                         <th>Truck No</th>  
                         <th>Driver Name</th>  
                         <th>Amount</th>  
                         <th>Cash/Cheque</th>  
                         <th>Cheq No</th>  
                         <th>Cheq Bank</th>  
                         <th>Ac Name</th>  
                         <th>Ac No</th>  
                         <th>Bank Name</th>  
                         <th>IFSC Code</th>  
                         <th>Pan No.</th>  
                         <th>Destination</th>  
                         <th>Other Remark</th>  
                    </tr>
  ';
  while($row = fetchArray($result))
  {
	  if($row['mode']=='NEFT')
	  {
		  $acno="'".$row['ac_no'];
	  }
	  else
	  {
		  $acno='';
	  }
	  
	  if($row['re_download']=='1'){
			 $output.='<tr style="background:yellow">';  
		}
		else{
			 $output.='<tr>';  
		}
		
   $output .= '
							<td>'.$row["user"].'</td>  
							<td>'.$row["company"].'</td>  
							<td>'.$row["tdvid"].'</td>  
						   <td>'.$row["newdate"].'</td>  
						   <td>'.$row["truckno"].'</td>
						   <td>'.$row["dname"].'</td>
						   <td>'.$row["amt"].'</td>
						   <td>'.$row["mode"].'</td>
						   <td>'.$row["chq_no"].'</td>
						   <td>'.$row["chq_bank"].'</td>
						   <td>'.$row["ac_name"].'</td>
						   <td>'.$acno.'</td>
						   <td>'.$row["bank"].'</td>
						   <td>'.$row["ifsc"].'</td>
						   <td>'.$row["pan"].'</td>
						   <td>'.$row["dest"].'</td>
						   <td>'.$row["naro"].'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Truck_vou_'.$date.'_'.$branch.'.xls');
  echo $output;
  
$update = Qry($conn,"UPDATE mk_tdv SET timestamp_download='$timestamp' WHERE id in(SELECT tab_id FROM truck_vou_cache WHERE colset='1' 
AND user='$branch')");

if(!$update){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}  
  
$delete_cache = Qry($conn,"DELETE FROM truck_vou_cache WHERE colset='1' AND user='$branch'");

if(!$delete_cache){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

}

?>