<?php
require_once '../_connect.php';

// $sql ="SELECT id,lrno,lr_type,truck_no,DATE_FORMAT(date,'%d-%m-%y') as date,fstation,tstation,CONCAT(consignor,'<br>',consignee) as party,
// CONCAT(do_no,'<br>',invno,'<br>',shipno) as nos,wt12,weight,item FROM lr_sample_pending WHERE branch='$branch' ORDER BY date ASC";

$sql = "SELECT id,tno,wheeler,name,CONCAT('1-',mo1,'<br>','2-',mo2) as mobile,pan,full as addr,dec_upload_time,branch FROM mk_truck";

$table = "(
    ".$sql."
) temp";
  
$primaryKey = 'id';
  
$columns = array(
    array( 'db' => $primaryKey, 'dt' => 0),
    array( 'db' => 'tno', 'dt' => 1),
    array( 'db' => 'wheeler', 'dt' => 2),
    array( 'db' => 'name', 'dt' => 3), 
    array( 'db' => 'mobile', 'dt' => 4), 
    array( 'db' => 'pan', 'dt' => 5),
    array( 'db' => 'addr', 'dt' => 10), 
    array( 'db' => 'dec_upload_time', 'dt' => 9), 
    array( 'db' => 'branch', 'dt' => 11) 
);
 
 $sql_details = array(
    'user' => $username,
    'pass' => $password,
    'db'   => $db_name,
    'host' => $host
);
 
require('../b5aY6EZzK52NA8F/scripts/ssp.class.php');
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

?>