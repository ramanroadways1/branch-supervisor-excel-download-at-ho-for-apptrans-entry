<?php				
require_once './connect.php';

$vou_no = escapeString($conn,strtoupper($_POST['idmemo']));

$fetch_data = Qry($conn,"SELECT f.newdate,f.truck_no,f.branch,f.company,f.actualf,
f.newtds,f.dsl_inc,f.gps,f.adv_claim,f.newother,f.tds,f.totalf,f.totaladv,f.ptob,
f.adv_date,f.cashadv,f.chqadv,f.chqno,f.disadv,f.rtgsneftamt,f.narre,
f.baladv,f.unloadd,f.detention,f.gps_rent,f.gps_device_charge,f.bal_tds,f.otherfr,f.claim,f.late_pod,f.totalbal,f.paidto,f.bal_date,
f.paycash,f.paycheq,f.paycheqno,f.paydsl,f.newrtgsamt,f.narra,f.sign_cash,f.sign_adv,f.sign_bal,f.pod_date,f.branch_bal,
b.name as b_name,b.mo1 as b_mobile,b.pan as b_pan,
d.name as d_name,d.mo1 as d_mobile,d.pan as d_lic,
o.name as o_name,o.mo1 as o_mobile,o.pan as o_pan
FROM freight_form as f
		LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
		LEFT OUTER JOIN mk_driver AS d ON d.id = f.did
		LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
		WHERE f.frno='$vou_no'");

if(!$fetch_data)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch_data)==0)
{
	Redirect("Freight Memo not found.","./");
	exit();
}
	
$row = fetchArray($fetch_data);
		
// if($branch!=$row['branch_bal'] AND $branch!=$row['branch'])
// {
	// echo "<script>
		// alert('Freight Memo belongs to $row[branch] Branch.');
		// window.close();
	// </script>";
	// exit();
// }
	
		$data_table='<table class="table table-bordered" style="margin-top:5px;font-size:12px;width:100%">
			<tr>   
				<th>Actual<br>Freight</th>
				<th>Loading(+)</th>
				<th>Dsl_Inc.(+)</th>
				<th>GPS(-)</th>
				<th>Claim(-)</th>
				<th>Other(-)</th>
				<th>TDS(-)</th>
				<th>Total<br>Freight</th>
				<th>Adv.to</th>
				<th>Cash</th>
				<th>Cheq.</th>
				<th>Diesel</th>
				<th>RTGS/NEFT</th>
				<th>TotalAdv.</th>
				<th>Balance</th>
			</tr>
			
			<tr>
				<td>'.$row['actualf'].'</td>
				<td>'.$row['newtds'].'</td>
				<td>'.$row['dsl_inc'].'</td>
				<td>'.$row['gps'].'</td>
				<td>'.$row['adv_claim'].'</td>
				<td>'.$row['newother'].'</td>
				<td>'.$row['tds'].'</td>
				<td>'.$row['totalf'].'</td>
				<td>'.$row['ptob'].'</td>
				<td>'.$row['cashadv'].'</td>
				<td>'.$row['chqadv']."(".$row['chqno'].")".'</td>
				<td>'.$row['disadv'].'</td>
				<td>'.$row['rtgsneftamt'].'</td>
				<td>'.$row['totaladv'].'</td>
				<td>'.$row['baladv'].'</td>
                
		</tr>
		<tr>
			<th>Narration</th>
			<td colspan="14">'.$row['narre'].'</td>
		</tr>	
	</table>';
	
$data_table_balance='<table class="table table-bordered" style="margin-top:5px;font-size:12px;width:100%">
			<tr>   
				<th>Un_Loading(+)</th>
				<th>Detention(+)</th>
				<th>GPS_Rent(-)</th>
				<th>GPS_Device<br>Charge(-)</th>
				<th>Claim(-)</th>
				<th>Other(-)</th>
				<th>TDS(-)</th>
				<th>Late POD(-)</th>
				<th>Total<br>Balance</th>
				<th>Bal.to</th>
				<th>Cash</th>
				<th>Cheq.</th>
				<th>Diesel</th>
				<th>RTGS/NEFT</th>
			</tr>
			
		<tr>
				<td>'.$row['unloadd'].'</td>
				<td>'.$row['detention'].'</td>
				<td>'.$row['gps_rent'].'</td>
				<td>'.$row['gps_device_charge'].'</td>
				<td>'.$row['claim'].'</td>
				<td>'.$row['otherfr'].'</td>
				<td>'.$row['bal_tds'].'</td>
				<td>'.$row['late_pod'].'</td>
				<td>'.$row['totalbal'].'</td>
				<td>'.$row['paidto'].'</td>
				<td>'.$row['paycash'].'</td>
				<td>'.$row['paycheq']."(".$row['paycheqno'].")".'</td>
				<td>'.$row['paydsl'].'</td>
				<td>'.$row['newrtgsamt'].'</td>
		</tr>
		<tr>
			<th>Narration</th>
			<td colspan="14">'.$row['narra'].'</td>
		</tr>	
	</table>';
	

$vou_date = date('d/m/y', strtotime($row['newdate']));
$adv_date = date('d/m/y', strtotime($row['adv_date']));
$bal_date = date('d/m/y', strtotime($row['bal_date']));
?>
<!DOCTYPE html>
<html lang="en">
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">

<div id="window_loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<style>
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}
.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>	

<style type="text/css">
@media print
{
body {
   zoom:70%;
 }	
body * { visibility: hidden; }
.container-fluid * { visibility: visible; }
.container-fluid { position: absolute; top: 0; left: 0; }
}
</style>

</head>

<form action="./vou_print_bal.php" target="_blank" id="myForm22" method="POST">
	<input type="hidden" value="<?php echo $vou_no ?>" name="frno">		
</form>
	
<body style="overflow-x: auto !important;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<a href="./"><button style="margin-left:10px;margin-top:10px;" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-chevron-left"></span> Dashboard</button></a>
<button onclick="document.getElementById('myForm22').submit();" type="button" style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary">Print Vouchers</button>
<button onclick="print()" style="margin-top:10px;margin-left:10px" class="btn btn-sm btn-primary">Print Freight Memo</button>

<div class="container-fluid">

<div class="row">
	
	<div class="form-group col-md-4"></div>		
	
	<div class="form-group col-md-4">
		<center><span>Freight Memo : <?php echo strtoupper($vou_no); ?> </span> </center>
	</div>
	
	 <div class="form-group col-md-4" style="font-size:12px;">
		<span class="pull-right">Vou. Date : <?php echo $vou_date; ?>, Adv. Date : <?php echo $adv_date; ?>, Bal. Date : <?php echo $bal_date; ?></span>
	</div>

</div>
 
<br />
<div class="row">

<div class="col-md-12 table-responsive" style='overflow-x:auto'>
<table border="0" style="width:100%;font-size:12px">
<tr>
	<td>
		<label>Truck No:</label>
		<?php echo $row['truck_no'];?>
	</td>  

	<td>
		<label>Company:</label>
		<?php echo $row['company']; ?>
	</td>

	<td style="text-align:center">
		<label>Branch:</label>
		<?php echo $row['branch']; ?>
	</td>
</tr>	

<tr>
		<td>
			<label>Broker:</label>
			<?php echo $row['b_name'];?>
		</td>
		
		<td>
			<label>Broker Mo.:</label>
			<?php echo $row['b_mobile'];?>
		</td>

		<td style="text-align:center">
			<label>Broker PAN:</label>
			<?php echo $row['b_pan'];?>
		</td>
	</tr>
 
 <tr>      
		<td>
			<label>Driver:</label>
			<?php echo $row['d_name']; ?>
		</td>
		
		<td>
			<label>Driver Mo.:</label>
			<?php echo $row['d_mobile']; ?>
		</td>

		<td style="text-align:center">
			<label>Driver LIC.:</label>
			<?php echo $row['d_lic']; ?>
		</td>
	</tr>		

	<tr>
		<td>
			<label>Owner:</label>
			<?php echo $row['o_name'];?>
		</td>

		<td>
			<label>Owner Mobile:</label>
			<?php echo $row['o_mobile'];?>
		</td>
		
		<td style="text-align:center">
			<label>Owner PAN:</label>
			<?php echo $row['o_pan'];?>
		</td>
	</tr>	

</table>
</div>
</div>

<br />

<div class="row">
	<div class="form-group col-md-12" style="font-size:13px">
		<span style="color:blue;font-size:13px;">Advance Details -</span>
		<div style="overflow-x:auto" class="table-responsive"><?php echo $data_table; ?></div>
	</div>
</div>

<br />

<div class="row">
	<div class="form-group col-md-12" style="font-size:13px">
		<span style="color:blue;font-size:13px;">Balance Details -</span>
		<div style="overflow-x:auto" class="table-responsive"><?php echo $data_table_balance; ?></div>
	</div>
</div>

<?php
if(($row['disadv']+$row['paydsl'])>0)
{
$qry_fetch_diesel = Qry($conn,"SELECT d.qty,d.rate,d.disamt,d.dsl_by,d.dcard,d.dcom,d.type,pump.name as pump_name FROM 
diesel_fm AS d 
LEFT OUTER JOIN diesel_pump AS pump ON pump.code=d.dcard 
WHERE d.fno='$vou_no' GROUP BY d.fno ORDER BY d.id ASC");

if(!$qry_fetch_diesel){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry_fetch_diesel)==0){
	Redirect("Diesel not found.","./");
	exit();
}

echo '
<div class="row">
	<div class="col-md-12" style="font-size:12px;">
		<span style="color:blue;font-size:13px;">Diesel Details -</span>
			<table class="table table-bordered" style="margin-top:5px">
				<tr>  
					<th>Qty</th>
					<th>Rate</th>
					<th>Amount</th>
					<th>Adv/Bal</th>
					<th>Card/Pump</th>
					<th>Card No/Pump Code</th>
					<th>Fuel Company</th>
					<th>Diesel Narration</th>
				</tr>';
while($row_adv_diesel=fetchArray($qry_fetch_diesel))
{
	if($row_adv_diesel['dsl_by']=='PUMP'){
		$card_no_pump_name = $row_adv_diesel['pump_name'];
	}
	else{
		$card_no_pump_name = $row_adv_diesel['dcard'];
	}
	
	echo "<tr>
				<td>$row_adv_diesel[qty]</td>
				<td>$row_adv_diesel[rate]</td>
				<td>$row_adv_diesel[disamt]</td>
				<td>$row_adv_diesel[type]</td>
				<td>$row_adv_diesel[dsl_by]</td>
				<td>$card_no_pump_name</td>
				<td>$row_adv_diesel[dcom]</td>
				<td>$row_adv_diesel[dcom]-$row_adv_diesel[dcard], Rs $row_adv_diesel[disamt]/-</td>
		</tr>";
}
echo "</table>
	</div>
</div>";
}
?>

<div class="row">
	<div class="form-group col-md-12">
		<h4 style="color:blue;font-size:13px;">LR Details -</h4>
		<div style="overflow-x:auto" class="records_content table-responsive"></div>
	</div>
</div>

<div class="row">
	<div class="form-group col-md-12">

		<table border="0" style="width:100%">
			<tr>
				<td align="center"><label style="font-size:13px;">Cashier</label></td>
				<td align="center"><label style="font-size:13px;">Advance Receiver</label></td>
				<td align="center"><label style="font-size:13px;">Balance Receiver</label></td>
			</tr>	
			<tr>
				<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $row['sign_cash']; ?>"></td>
				<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $row['sign_adv']; ?>"></td>
				<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="../b5aY6EZzK52NA8F/<?php echo $row['sign_bal']; ?>"></td>
			</tr>	
		</table>
	</div> 
</div> 

</div>
	
</body>
</html>

<script>
function lrFetch()
{ 
	jQuery.ajax({
		url: "./ajax/readRecords.php",
		data: 'fids=' + '<?php echo $vou_no; ?>' + '&page_name=' + '<?php echo basename($_SERVER["PHP_SELF"]); ?>',
		type: "POST",
		success: function(data) {
			$(".records_content").html(data);
		},
	error: function() {}
	});
}					
lrFetch();
</script>