<?php
require_once 'connect.php';
?>	
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
}
</style>

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">			

<div class="row">
	<br />
	<br />
	
	<div class="form-group col-md-12">
		<center>
			<h4 style="letter-spacing:1px;color:#FFF;font-weight:bold;font-size:16px">Forfeit - FM Payment</h4>
		</center>	
	</div>	

<script>	
function ResetCreditor()
{
	$('#creditor_party').html('<option value="">--select--</option>');
}

function CheckFm(fm_no)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "./check_fm_forfeit_balance.php",
		data: 'fm_no=' + fm_no,
		type: "POST",
		success: function(data) {
			$("#creditor_party").html(data);
		},
		error: function() {}
		});
}

function LoadFMData(party)
{
	var fm_no = $('#fm_no').val();
	
	if(fm_no=='')
	{
		$('#fm_no').focus();
		$('#creditor_party').val();
		$('#creditor_party').html('<option value="">--select--</option>');
	}
	else
	{
		if(party!='')
		{
			$("#loadicon").show();
			jQuery.ajax({
			url: "./get_fm_forfeit_balance.php",
			data: 'fm_no=' + fm_no + '&party=' + party,
			type: "POST",
			success: function(data) {
				$("#fm_result").html(data);
			},
			error: function() {}
			});
		}
	}
}
</script>	

<div class="form-group col-md-12">
	
	<div class="form-group col-md-4 col-md-offset-2">
		<label>FM Number <font color="red">*</font></label>
		<input type="text" oninput="ResetCreditor()" onblur="CheckFm(this.value)" name="fm_no" id="fm_no" class="form-control" required="required">
	</div>
	
	<div class="form-group col-md-4">
		<label>Select Creditor <font color="red">*</font></label>
		<select onchange="LoadFMData(this.value)" name="creditor" class="form-control" id="creditor_party" required="required">
			<option value="">--select--</option>
		</select>
	</div>

</div>
	
	<div class="form-group col-md-12 table-responsive" id="fm_result">
	</div>

<div id="fm_result2"></div>

</div>
</div>
</div>

</body>
</html>

<script>
function ForfeitFMAmount(id,frno,creditor_type)
{
	$("#loadicon").show();
			jQuery.ajax({
			url: "./save_forfeit_balance.php",
			data: 'id=' + id + '&frno=' + frno + '&creditor_type=' + creditor_type,
			type: "POST",
			success: function(data) {
				$("#fm_result2").html(data);
			},
			error: function() {}
			});
}
</script>