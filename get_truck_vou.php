<?php
require_once 'connect.php';

if(!isset($_POST['submit'])){
	echo "<script>
		window.location.href='./truck_vou.php';
	</script>";
	exit();
}

$branch = escapeString($conn,$_POST['branch']);
?>
<!doctype html>
<html lang="en">
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
	<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<body style="background:;font-family: 'Open Sans', sans-serif !important">

<a href="./truck_vou.php"><button type="button" style="margin-left:10px;margin-top:10px" class="btn btn-danger">Go back</button></a>

<div class="container-fluid">
	<div class="row">
		<div class="form-group col-md-12">
			<center><font size="3"><span style="color:blue">Truck Vouhers :</span> <?php echo $branch; ?> Branch !</font></center>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12" id="result_div"></div>
	</div>
</div>

<script type="text/javascript">
function FetchData(){
	$("#loadicon").show();
		jQuery.ajax({
			url: "./load_voucher.php",
			data: 'branch=' + '<?php echo $branch; ?>' + '&type=' + 'truck_vou',
			type: "POST",
			success: function(data){
				$("#result_div").html(data);
			},
			error: function() {}
	});
}
 $(document).ready(function(){
 FetchData();
});
</script>

</body>
</html>

<div id="result_div2"></div>