<?php
require_once 'connect.php';
?>	
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
}

:-internal-autofill-previewed {
  font-size: 12px !important;
}
</style>

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">			

<div class="row">

<script>
$(function() {
		$("#broker_name").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_broker.php',
		select: function (event, ui) { 
           $('#broker_name').val(ui.item.value);   
           $('#broker_id').val(ui.item.id);     
           $('#current_pan').val(ui.item.pan);     
           return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Broker does not exists.');
			$("#broker_name").val('');
			$("#broker_name").focus();
			$("#broker_id").val('');
			$("#current_pan").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
		
$(function() {
		$("#vehicle_no").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_market_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#vehicle_no').val(ui.item.value);   
            $('#owner_id').val(ui.item.oid);   
            $('#current_pan').val(ui.item.owner_pan);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#vehicle_no').val("");  
			$("#vehicle_no").focus();			
			$('#owner_id').val("");   
			$('#current_pan').val("");   
			alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});		
</script>

<form autocomplete="off" id="UpdateForm">
<div class="form-group col-md-4 col-md-offset-4">
	
	<br />
	<br />
	
	<div class="form-group col-md-12">
		<center>
			<h4 style="letter-spacing:1px;color:#FFF;font-weight:bold;font-size:16px">Edit PAN Number</h4>
		</center>	
	</div>
	
	<div class="form-group col-md-12">
		<label>Party Type <font color="red">*</font></label>
		<select style="font-size:12px !important" name="party_type" onchange="PartyType(this.value)" class="form-control" required="required">
			<option style="font-size:12px !important" value="">Select an option</option>
			<option style="font-size:12px !important" value="BROKER">Broker</option>
			<option style="font-size:12px !important" value="OWNER">Vehicle Owner</option>
		</select>
	</div>
	
<script>	
function PartyType(elem)
{
	$('#current_pan').val('');
	$('#new_pan').val('');
	$('#broker_id').val('');
	$('#owner_id').val('');
	
	if(elem=='BROKER')
	{
		$('#broker_div').show();
		$('#vehicle_div').hide();
		
		$('#broker_name').attr('required',true);
		$('#vehicle_no').attr('required',false);
	}
	else if(elem=='OWNER')
	{
		$('#broker_div').hide();
		$('#vehicle_div').show();
		
		$('#broker_name').attr('required',false);
		$('#vehicle_no').attr('required',true);
	}
	else
	{
		$('#broker_div').hide();
		$('#vehicle_div').hide();
		
		$('#broker_name').attr('required',true);
		$('#vehicle_no').attr('required',true);
	}
}
</script>	
	
	<div class="form-group col-md-12" id="broker_div" style="display:none">
		<label>Broker Name <font color="red">*</font></label>
		<input style="font-size:12px !important" type="text" class="form-control" required="required" id="broker_name" name="broker_name" />
	</div>
	
	<input type="hidden" id="broker_id" name="broker_id">
	<input type="hidden" id="owner_id" name="owner_id">
	
	<div class="form-group col-md-12" id="vehicle_div" style="display:none">
		<label>Vehicle Number <font color="red">*</font></label>
		<input style="font-size:12px !important" type="text" class="form-control" required="required" id="vehicle_no" name="vehicle_no" />
	</div>
	
	<div class="form-group col-md-12">
		<label>Current PAN <font color="red">*</font></label>
		<input style="font-size:12px !important" readonly id="current_pan" type="text" class="form-control" required="required" name="current_pan" />
	</div>
	
	<div class="form-group col-md-12">
		<label>Enter New PAN <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePan()" maxlength="10" minlength="10" style="text-transform:uppercase;font-size:12px !important" id="new_pan" type="text" class="form-control" required="required" name="new_pan" />
	</div>
	
	<div class="form-group col-md-12">
		<button type="submit" class="btn btn-sm btn-danger" name="submit" id="pan_update_btn"><span class="fa fa-edit"></span>&nbsp; Confirm Update</button> 
	</div>
</form>
	
</div>
</div>
</div>
</div>

</body>
</html>

<div id="result_form"></div>

<script>
$(document).ready(function (e) {
	$("#UpdateForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#pan_update_btn").attr("disabled", true);
	$.ajax({
        	url: "./save_edit_pan.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result_form").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});


function ValidatePan() { 
  var Obj = document.getElementById("new_pan");
  if(Obj.value!=""){
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            if (ObjVal.search(panPat) == -1) {
                document.getElementById("new_pan").setAttribute("style","background-color:red;color:#FFF");
				$("#pan_update_btn").attr("disabled", true);
                Obj.focus();
                return false;
            }
			else
            {
				document.getElementById("new_pan").setAttribute("style","background-color:green;color:#FFF");
				$("#pan_update_btn").attr("disabled", false);
            }
        }
		else
		{
			document.getElementById("new_pan").setAttribute("style","background-color:white;");
			$("#pan_update_btn").attr("disabled", false);
		}
  }
</script>