<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php");

date_default_timezone_set('Asia/Kolkata');

if(!isset($_SESSION['ho']))
{
	echo "<script type='text/javascript'>
		window.location.href='/';
	</script>";
	exit();
}

$conn=mysqli_connect($host,$username,$password,$db_name);

if(!$conn){
    echo "<span style='font-family:Verdana'>Database Connection failed: <b><font color='red'>" .mysqli_connect_error()."</b></font></span>";
	exit();
}

$timestamp = date("Y-m-d H:i:s");

$username = $_SESSION['ho'];

$get_my_id = Qry($conn,"SELECT id FROM user WHERE username='$username' AND branch_inactive!='1'");
if(!$get_my_id){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

if(numRows($get_my_id)==0)
{
	session_destroy();
	echo "<script type='text/javascript'>
		window.location.href='/';
	</script>";
	exit();
}

$row_get_my_id = fetchArray($get_my_id);
$my_id = $row_get_my_id['id'];

$get_my_branches = Qry($conn,"SELECT GROUP_CONCAT(CONCAT(\"'\",username,\"'\") SEPARATOR ',') as my_branches FROM `user` WHERE role='2' 
AND branch_inactive!='1' AND branch_supervisor_ho='$my_id'");

if(!$get_my_branches){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_my_branches = fetchArray($get_my_branches);
$limit = $row_my_branches['my_branches'];
?>