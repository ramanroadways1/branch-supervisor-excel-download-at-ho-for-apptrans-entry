<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$type = escapeString($conn,$_POST['type']);
$timestamp = date("Y-m-d H:i:s");

if($type=='ADVANCE')
{
	$get_cache_data = Qry($conn,"SELECT id,fm_no,IF(adv_party='1','BROKER','OWNER') as adv_to,party_name,total_freight,total_adv,
	date,colset FROM ship.freight_memo_adv WHERE colset_d!='1' AND rtgs_adv='1' AND branch='$branch'");
	
	if(!$get_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache_data)==0)
	{
		echo "<center><span style='color:red'>No result found !</span></center>";
		echo "<script>
			$('#loadicon').fadeOut();
		</script>";
		exit();
	}
	
	echo "<form action='download_voucher_fm_coal.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	 
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Freight</th>
				<th>Advance</th>
				<th>Adv.To</th>
				<th>Adv.Date</th>
				<th>Adv.Party</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache_data))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			echo "
			<tr>
				<td>$sn</td>
				<td>$row[fm_no]</td>
				<td>$row[total_freight]</td>
				<td>$row[total_adv]</td>
				<td>$row[adv_to]</td>
				<td>$row[date]</td>
				<td>$row[party_name]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
		
		echo "<script>
			$('#loadicon').fadeOut();
		</script>";
}
else
{
	$get_cache_data = Qry($conn,"SELECT id,fm_no,IF(bal_party='1','BROKER','OWNER') as bal_to,party_name,total_bal,date,
	colset FROM ship.freight_memo_bal WHERE colset_d!='1' AND rtgs_bal='1' AND branch='$branch'");
	
	if(!$get_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache_data)==0)
	{
		echo "<center><span style='color:red'>No result found !</span></center>";
		echo "<script>
			$('#loadicon').fadeOut();
		</script>";
		exit();
	}
	
	echo "<form action='download_voucher_fm_coal.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	 
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Balance</th>
				<th>Bal.To</th>
				<th>Bal.Date</th>
				<th>Bal.Party</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache_data))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			echo "
			<tr>
				<td>$sn</td>
				<td>$row[fm_no]</td>
				<td>$row[total_bal]</td>
				<td>$row[bal_to]</td>
				<td>$row[date]</td>
				<td>$row[party_name]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
		
		echo "<script>
			$('#loadicon').fadeOut();
		</script>";
}
?>

<script>
function ApproveAll(branch)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher_all_fm_coal.php",
			data: 'branch=' + branch + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}

function Approve(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher_fm_coal.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}

function Reject(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./reject_voucher_fm_coal.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}
</script>