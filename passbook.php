<?php
require_once 'connection.php';

$branch=mysqli_real_escape_string($conn,strtoupper($_SESSION['user']));

$date2 = date("Y-m-d"); 
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>
 
    <style>
        body {
            overflow-x: auto;
        }
		label{font-size:12px;color:#FFF}
    </style>
</head>

<body style="background:#078388;font-family:Verdana">
	
	<div class="container-fluid">
		<div class="col-md-2">
			<?php include 'sidebar.php';?>
		</div>
	
<div class="col-md-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
           <center>
		   <br />
        <h3 style="color:#FFF;font-size:25px;font-family:Baumans;letter-spacing:3px">View - Passbook</h3>
		</center>

<form style="color:#000;" action="pass.php" class="form-horizontal" method="post">	
		
	<script>
		function Range(opt)
		{
			if(opt.value=='FULL' || opt.value=='-119 days' || opt.value=='-89 days' || opt.value=='-59 days')
			{
			 $('#view_div').hide();
			}
			else
			{
			$('#view_div').show();
			}
		}
	</script>	
		
	<div class="form-group col-md-12">
		<label>Company <font color="red">*</font></label>
			<select class="form-control" style="border:1px solid #000" name="company" required="required">
				<option value="">Select Company</option>
				<option value="RRPL">RRPL</option>
				<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
			</select>
	</div>

		<div class="form-group col-md-12">
		<label>Select date range <font color="red">*</font></label>
			<select class="form-control" onchange="Range(this)" style="border:1px solid #000" name="range" required="required">
				<option value="">Select an option</option>
				<option value="-0 days">Today's</option>
				<option value="-1 days">Last 2 days</option>
				<option value="-4 days">Last 5 days</option>
				<option value="-6 days">Last 7 days</option>
				<option value="-9 days">Last 10 days</option>
				<option value="-14 days">Last 15 days</option>
				<option value="-29 days">Last 30 days</option>
				<option value="-59 days">Last 60 days</option>
				<option value="-89 days">Last 90 days</option>
				<option value="-119 days">Last 120 days</option>
				<option value="FULL">FULL EXCEL</option>
			</select>
		</div>	

		<div class="form-group col-md-6" id="view_div">	
			<input type="hidden" name="branch" value="<?php echo $branch; ?>" />	
			<button id="view" type="submit" style="color:#000;letter-spacing:1px;" class="btn btn-warning" name="submit">View Passbook</button>
		</div>
		
		<div class="form-group col-md-6">	
			<button id="download" type="submit" style="color:#000;letter-spacing:1px;" class="btn btn-warning" name="download">Export Excel</button>
		</div>
		
</form>
                
    </div>
	
   </div>
 </body>
</html>