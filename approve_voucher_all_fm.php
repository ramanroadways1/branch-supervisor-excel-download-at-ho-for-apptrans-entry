<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$type = escapeString($conn,$_POST['type']);

if($type=='ADVANCE')
{
	$table_name = "freight_memo_adv_cache";
}
else
{
	$table_name = "freight_memo_bal_cache";
}

$qry = Qry($conn,"UPDATE `$table_name` SET colset='1' WHERE branch='$branch'");

if(!$qry){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

echo "<script>
	$('#approve_all_button').attr('disabled',true);
	$('#approve_all_button').html('All Approved');
	
	$('.approve_button').attr('disabled',true);
	$('.approve_button').html('Approved');
	
	$('.reject_button').attr('disabled',false);
	$('.reject_button').html('Reject');
	$('#loadicon').hide();
</script>";
?>