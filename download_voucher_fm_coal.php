<?php 
require_once './connect.php';

$branch = $_POST['branch'];
$type = $_POST['type'];
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");
$output = '';

if($type=='ADVANCE')
{
	$query= "SELECT f.fm_no as frno,f.tno as truck_no,f.company,f.date as newdate,l.lr_date,stn.name as fstation,stn2.name as tstation,
	con1.name as con1,con2.name as con2, 
	f.weight,f.rate,f.freight as actualf,f.lrnos as lrno,a.party_name as pto_adv_name,a.party_pan as adv_pan,a.loading as loadd,a.dsl_inc,
	a.other as newother,a.commission_amount,a.tds as newtds,a.gps_deposit,a.total_freight as totalf,
	a.total_adv as advamt,a.cash,a.cheque,a.chqno,
	a.diesel,a.rtgs,r.crn,r.bank,r.utr_date,a.branch,a.date as adv_date,a.narration as narre 
	FROM ship.freight_memo as f
	left outer join ship.freight_memo_adv as a on a.fm_no=f.fm_no
	left outer join ship.lr_entry as l on FIND_IN_SET(f.lr_ids, l.id)
	left outer join station as stn on stn.id=l.from_loc
	left outer join station as stn2 on stn2.id=l.to_loc
	left outer join consignor as con1 on con1.id=l.con1
	left outer join consignee as con2 on con2.id=l.con2
	left outer join rtgs_fm as r on r.fno=a.fm_no and r.type='ADVANCE'
	WHERE a.colset_d='0' AND a.colset='1' AND a.rtgs_adv='1' AND a.branch='$branch'";
}
else
{
	$query= "SELECT f.fm_no as frno,f.tno as truck_no,f.company,f.date as newdate,
	f.weight,f.lrnos as lrno,b.party_name as pto_bal_name,b.party_pan as bal_pan,
    (a.total_freight-a.total_adv) as balamt,
    b.unloading,b.other as other_charge,
	b.tds as bal_tds,b.claim,b.gps_charges,b.gps_deposit_return,b.late_pod,b.total_bal,
    b.cash as cash2,b.cheque as cheque2,b.chqno as paycheqno,b.date as bal_date,
	b.diesel as diesel2,b.rtgs as rtgs2,r.crn,r.bank,r.utr_date,b.branch,b.narration as narra,pod.lr_weight,pod.rcvd_weight 
	FROM ship.freight_memo as f
	left outer join ship.freight_memo_adv as a on a.fm_no=f.fm_no
	left outer join ship.freight_memo_bal as b on b.fm_no=f.fm_no
    left outer join ship.rcv_pod as pod on pod.fm_no=b.fm_no
	left outer join rtgs_fm as r on r.fno=a.fm_no and r.type='BALANCE'
	WHERE b.colset_d='0' AND b.colset='1' AND a.colset_d='1' AND b.rtgs_bal='1' AND b.branch='$branch'";
}

$result = Qry($conn,$query);

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}
	
if($type=='ADVANCE')
{
	
if(numRows($result)==0)
{
	echo "<script>
		alert('No result found !');
		window.close();
	</script>";
	exit();
}

 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Vou No</th>  						 
						 <th>FM Date</th>  
						 <th>Truck No</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
						 <th>Adv Date</th>  
                         <th>Adv Party Name</th>  
                         <th>PAN No</th>  
                         <th>Narration</th>  
                         <th>LR No</th>  
                         <th>From</th>  
                         <th>To</th>  
                         <th>Actual Freight</th>  
                         <th>Load(+)</th>  
                         <th>Diesel Inc(+)</th>  
                         <th>Gps Deposit(-)</th>  
                         <th>Claim(-)</th>  
                         <th>Other(-)</th>  
                         <th>Commission(-)</th>  
                         <th>TDS(-)</th>  
                         <th>Total Freight</th>  
                         <th>Advance Amt</th>  
                         <th>Cash</th>  
                         <th>Cheq</th>  
						 <th>Cheq No</th>  
                         <th>Diesel</th>  
                         <th>Rtgs</th>  
                         <th>Balance</th>   
                         <th>Ref_no</th>   
                         <th>UTR_no</th>   
                         <th>UTR_date</th>   
                         <th>Diesel_narr.</th>   
                    </tr>';
					
  while($row = fetchArray($result))
  {
	   $diesel_amount = $row['diesel'];
	   $balance_amount=$row["totalf"]-$row["advamt"];
	  
	  if($diesel_amount>0){
		  $get_diesel_nrr = Qry($conn,"SELECT GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr FROM diesel_fm WHERE 
		  fno='$row[frno]' AND type='ADVANCE' GROUP by fno");
		  
		  $row_diesel_nrr = fetchArray($get_diesel_nrr);
		  $diesel_narr = $row_diesel_nrr['diesel_narr'];
	  }
	  else{
		  $diesel_narr="";
	  }
	 
	 $output .= '<tr>
							<td>'.$row["frno"].'</td>  
							<td>'.$row["newdate"].'</td>
							<td>'.$row["truck_no"].'</td>
							<td>'.$row["branch"].'</td>
							<td>'.$row["company"].'</td>
							<td>'.$row["adv_date"].'</td>
							<td>'.$row["pto_adv_name"].'</td>
							<td>'.$row["adv_pan"].'</td>
							<td>'.$row["narre"].'</td>  
						   <td>'.$row["lrno"].'</td>
							<td>'.$row["fstation"].'</td>  
							<td>'.$row["tstation"].'</td>  
						   <td>'.$row["actualf"].'</td>
						   <td>'.$row["loadd"].'</td>
						   <td>'.$row["dsl_inc"].'</td>
						   <td>'.$row["gps_deposit"].'</td>
						   <td>0</td>
						   <td>'.$row["newother"].'</td>
						   <td>'.$row["commission_amount"].'</td>
						   <td>'.$row["newtds"].'</td>
						   <td>'.$row["totalf"].'</td>
						   <td>'.$row["advamt"].'</td>
						   <td>'.$row["cash"].'</td>
						   <td>'.$row["cheque"].'</td>
						   <td>'.$row["chqno"].'</td>
						   <td>'.$row["diesel"].'</td>
						   <td>'.$row["rtgs"].'</td>
						   <td>'.$balance_amount.'</td>
						   <td>'.$row["crn"].'</td>
						   <td>'.$row["bank"].'</td>
						   <td>'.$row["utr_date"].'</td>
						   <td>'.$diesel_narr.'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_ADV_'.$date.'_'.$branch.'.xls');
  echo $output;
  
	$update = Qry($conn,"UPDATE ship.freight_memo_adv SET colset_d='1',download_time='$timestamp' WHERE colset_d!='1' AND 
	colset='1' AND rtgs_adv='1' AND branch='$branch'");

	if(!$update){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}  
}
else
{
	if(numRows($result)==0)
	{
		echo "<script>
			alert('No result found !');
			window.close();
		</script>";
		exit();
	}


	$output .= '
   <table border="1">  
                    <tr>  
                         <th>Vou No</th>  						 
						 <th>FM Date</th>  
						 <th>Truck No</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
						 <th>Bal Date</th>  
                         <th>Bal Party Name</th>  
                         <th>PAN No</th>  
                         <th>Narration</th>  
                         <th>LR_No</th>  
                         <th>LR_Weight</th>  
                         <th>Unload_Weight</th>  
                         <th>Balance</th>  
                         <th>Unloading/Detention(+)</th>  
                         <th>Gps_Deposit_Return(+)</th>  
                         <th>Gps_Device_Chrage(-)</th>  
                         <th>TDS(-)</th>  
                         <th>Other Charges(-)</th>  
                         <th>Claim(-)</th>  
                         <th>Late POD(-)</th>  
                         <th>Total Balance</th>  
                         <th>Cash</th>  
                         <th>Cheq</th>  
						 <th>Cheq No</th>  
                         <th>Diesel</th>  
                         <th>Rtgs</th>  
						 <th>Ref_no</th>   
                         <th>UTR_no</th>   
                         <th>UTR_date</th>   
                         <th>Diesel_narr.</th>   
                    </tr>
  ';
  while($row = fetchArray($result))
  {
	  
	$diesel_amount = $row['diesel2'];
	$lr_weight=$row['lr_weight'];
	$unload_weight=$row['rcvd_weight'];
	  
  if($diesel_amount>0)
  {
	  $get_diesel_nrr = Qry($conn,"SELECT GROUP_CONCAT(dsl_nrr SEPARATOR ', ') as diesel_narr FROM diesel_fm WHERE fno='$row[frno]' AND 
	  type='BALANCE' GROUP by fno");
	  $row_diesel_nrr = fetchArray($get_diesel_nrr);
	  $diesel_narr = $row_diesel_nrr['diesel_narr'];
  }
  else
  {
	  $diesel_narr="";
  }
  
  $output .= '<tr>
   
					<td>'.$row["frno"].'</td>  
							<td>'.$row["newdate"].'</td>
							<td>'.$row["truck_no"].'</td>
							<td>'.$row["branch"].'</td>
							<td>'.$row["company"].'</td>
							<td>'.$row["bal_date"].'</td>
							<td>'.$row["pto_bal_name"].'</td>
							<td>'.$row["bal_pan"].'</td>
							<td>'.$row["narra"].'</td>  
						   <td>'.$row["lrno"].'</td>
						   <td>'.$lr_weight.'</td>
						   <td>'.$unload_weight.'</td>
						   <td>'.$row["balamt"].'</td>
						   <td>'.$row["unloading"].'</td>
						   <td>'.$row["gps_deposit_return"].'</td>
						   <td>'.$row["gps_charges"].'</td>
						   <td>'.$row["bal_tds"].'</td>
						   <td>'.$row["other_charge"].'</td>
						   <td>0</td>
						   <td>'.$row["late_pod"].'</td>
						   <td>'.$row["total_bal"].'</td>
						   <td>'.$row["cash2"].'</td>
						   <td>'.$row["cheque2"].'</td>
						   <td>'.$row["paycheqno"].'</td>
						   <td>'.$row["diesel2"].'</td>
						   <td>'.$row["rtgs2"].'</td>
						   <td>'.$row["crn"].'</td>
						   <td>'.$row["bank"].'</td>
						   <td>'.$row["utr_date"].'</td>
						   <td>'.$diesel_narr.'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=FM_BAL_'.$date.'_'.$branch.'.xls');
  echo $output;
  
	$update = Qry($conn,"UPDATE ship.freight_memo_bal t1 
			INNER JOIN ship.freight_memo_adv t2 
			ON t1.fm_no = t2.fm_no
			SET t1.colset_d = '1',t1.download_time='$timestamp' 
			WHERE t1.colset_d!='1' AND t1.colset='1' AND t1.rtgs_bal='1' AND t1.fm_no = t2.fm_no AND t2.colset_d='1' 
			AND t2.branch='$branch'");

	if(!$update){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}  
}
?>