<?php
require_once 'connect.php';

$date=date('Y-m-d');

$result=Qry($conn,"SELECT SUM(truck_vou) AS total_tdv,SUM(truck_vou_amount) As tdv_amount,SUM(exp_vou) as total_exp,
SUM(exp_vou_amount) as exp_amount,SUM(fm_adv) as total_adv,SUM(fm_adv_amount) as adv_amount,SUM(fm_bal) as total_bal,
SUM(fm_bal_amount) as bal_amount FROM today_data WHERE date='$date' AND branch in($limit)"); 

if(!$result){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row = fetchArray($result); 
?>
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
}
</style>

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">			
	
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="<?php echo $baseUrl_main; ?>svg/exp_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $row['tdv_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $row['total_tdv']; ?></b> 
							Exp Vou.</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="<?php echo $baseUrl_main; ?>svg/truck_vou.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $row['exp_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $row['total_exp']; ?></b> 
							Truck Vou.</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="<?php echo $baseUrl_main; ?>svg/fm_adv.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $row['adv_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $row['total_adv']; ?></b> 
							FM Adv</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
				<div class="panel panel-red panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="<?php echo $baseUrl_main; ?>svg/fm_bal.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5">
							<div class="large" style="font-size:18px;color:#000">&#8377; <?php echo $row['bal_amount']; ?></div>
							<div class="text-muted" style="color:#000; font-size:11px; padding-top:6px;"><b><?php echo $row['total_bal']; ?></b> 
							FM Bal</div>
						</div>
					</div>
				</div>
			</div>	
          </div>
	<br />
	
<div class="row">	
	
	<form method="post" action="../_view/vouchers.php" target="_blank">
	<div class="form-group col-md-3 col-sm-12">
		<label>Truck Vou </label>
		<input class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" placeholder="Vou No" name="vou_no" required />
		<button style="margin-top:5px !important;" type="submit" name="submit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
    </div>
	</form>
		
	<form method="post" action="../_view/vouchers.php" target="_blank">	
	<div class="form-group col-md-3 col-sm-12">
		<label>Exp Vou</label>
		<input class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" placeholder="Exp_Vou id" name="vou_no" required />
		<button style="margin-top:5px !important;" type="submit" name="submit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
	</div> 
	</form>
		
	<form method="post" action="../_view/freight_memo.php" target="_blank">
	<div class="form-group col-md-3 col-sm-12">
		<label>Search FM/OLR/MKT Bilty</label>
		<input class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" placeholder="Vou no or LR no" name="value1" required />
		<input type="hidden" name="key" value="SEARCH" />
		<button style="margin-top:5px !important;" type="submit" name="submit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
	</div>
	</form>

	<form method="post" action="../coal/view_vou.php" target="_blank">
	<div class="form-group col-md-3 col-sm-12">
		<label>Search LR (COAL)</label>
		<input class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" placeholder="LR No" name="lrno" required />
		<input type="hidden" name="key" value="LR" />
		 <button style="margin-top:5px !important;" type="submit" name="submit" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
	</div>
	</form>

	</div>
	</div>
</div>

<?php /*
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="panel panel-default chat"  style="border:1px solid #000;">
<div style="color:#000;padding-top:5px;background:#0AF;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:14px; border-bottom:1px solid #888; text-align:center;">
					<i><b>RRPL</b> RECENT <b>CASHBOOK</b> DEBITS</i></div>
	<div class="panel-body" style="overflow-x:auto;">
<?php
$castbl = Qry($conn,"SELECT debit,user,vou_no,vou_type FROM cashbook WHERE user in($limit) AND comp='RRPL' AND debit>'0' ORDER BY id DESC LIMIT 10");
echo    "<table class='table table-bordered' style='font-family:Verdana;font-size:10px'>";
echo      "<tr>";
echo        "<th>Amount</th>";
echo        "<th>Branch</th>";
echo        "<th>Vou No</th>";
echo        "<th>Vou Type</th>";
echo      "</tr>";
while($rowtl = fetchArray($castbl))
  {
echo      "<tr>";
echo        "<td>";
echo        $rowtl['debit'];         
echo        "</td>";
echo        "<td>";
echo        $rowtl['user'];         
echo        "</td>";
echo        "<td>";
echo        $rowtl['vou_no'];         
echo        "</td>";
echo        "<td>";
echo        $rowtl['vou_type'];         
echo        "</td>";
echo      "</tr>";
}
echo  "</table>";
?>
					</div>
				</div>
				
			</div><!--/.col-->
			
		<div class="col-md-6 col-sm-12">
	
				<div class="panel panel-default chat" style="border:1px solid #000;">
<div style="color:#000;padding-top:5px;background:#0AF;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:14px; border-bottom:1px solid #888; text-align:center;">
<i><b>RRPL</b> RECENT <b>PASSBOOK </b>DEBITS</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$cas = Qry($conn,"SELECT debit,user,vou_no,vou_type FROM passbook WHERE user in($limit) AND comp='RRPL' AND debit>'0' ORDER BY id DESC LIMIT 10");

echo    "<table class='table table-bordered' style='font-family:Verdana;font-size:10px'>";
echo      "<tr>";
echo        "<th>Amount</th>";
echo        "<th>Branch</th>";
echo        "<th>Vou No</th>";
echo        "<th>Vou Type</th>";
echo      "</tr>";

while($rowt = fetchArray($cas))
  {
echo      "<tr>";
echo        "<td>";
echo        $rowt['debit'];         
echo        "</td>";
echo        "<td>";
echo        $rowt['user'];         
echo        "</td>";
echo        "<td>";
echo        $rowt['vou_no'];         
echo        "</td>";
echo        "<td>";
echo        $rowt['vou_type'];         
echo        "</td>";
echo      "</tr>";

}
echo    "</tbody>";
echo  "</table>";
?>
					</div>
					
					
				</div>
				
			</div><!--/.col-->
		</div>
		
		<div class="row">
			<div class="col-md-6 col-sm-12">
			<div class="panel panel-default chat"  style="border:1px solid #000;">
<div style="color:#000;padding-top:5px;background:#0AF;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:14px; border-bottom:1px solid #888; text-align:center;">
<i><b>RR</b> RECENT <b>CASHBOOK</b> DEBITS</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
					<?php

$cstbl = Qry($conn,"SELECT debit2,user,vou_no,vou_type FROM cashbook WHERE user in($limit) AND comp='RAMAN_ROADWAYS' AND debit2>'0' ORDER BY id DESC LIMIT 10");

echo    "<table class='table table-bordered' style='font-family:Verdana;font-size:10px'>";
echo      "<tr>";
echo        "<th>Amount</th>";
echo        "<th>Branch</th>";
echo        "<th>Voucher No</th>";
echo        "<th>Voucher Type</th>";
echo      "</tr>";

while($owtl = fetchArray($cstbl))
  {
echo      "<tr>";
echo        "<td>";
echo        $owtl['debit2'];         
echo        "</td>";
echo        "<td>";
echo        $owtl['user'];         
echo        "</td>";
echo        "<td>";
echo        $owtl['vou_no'];         
echo        "</td>";
echo        "<td>";
echo        $owtl['vou_type'];         
echo        "</td>";
echo      "</tr>";
}
echo  "</table>";
?>
					</div>
				</div>
				
			</div><!--/.col-->
			
		<div class="col-md-6 col-sm-12">
			
				<div class="panel panel-default chat" style="border:1px solid #000;">
<div style="color:#000;padding-top:5px;background:#0AF;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:14px; border-bottom:1px solid #888; text-align:center;">
<i><b>RR</b> RECENT <b>PASSBOOK</b> DEBITS</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$ctbl = Qry($conn,"SELECT debit2,user,vou_no,vou_type FROM passbook WHERE user in($limit) AND comp='RAMAN_ROADWAYS' AND debit2>'0' ORDER BY id DESC LIMIT 10");
echo    "<table class='table table-bordered' style='font-family:Verdana;font-size:10px'>";
echo      "<tr>";
echo        "<th>Amount</th>";
echo        "<th>Branch</th>";
echo        "<th>Voucher No</th>";
echo        "<th>Voucher Type</th>";
echo      "</tr>";

while($rol = fetchArray($ctbl))
  {
echo      "<tr>";
echo        "<td>";
echo        $rol['debit2'];         
echo        "</td>";
echo        "<td>";
echo        $rol['user'];         
echo        "</td>";
echo        "<td>";
echo        $rol['vou_no'];         
echo        "</td>";
echo        "<td>";
echo        $rol['vou_type'];         
echo        "</td>";
echo      "</tr>";

}
echo  "</table>";
?>
					</div>
				</div>
			</div><!--/.col-->
		</div>
</div>
<?php
*/
?>
</body>
</html>