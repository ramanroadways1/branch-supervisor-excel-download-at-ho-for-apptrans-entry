<?php 
require_once '../connect.php';

$branch = $_REQUEST['branch'];

$output = '';

$query = "SELECT company,date,lrdate,bilty_no,plr,tno,frmstn,tostn,cwt,rate,tamt,branch FROM mkt_bilty WHERE colset_d!='1' AND colset='1' AND branch='$branch' ORDER BY id ASC";
$result = mysqli_query($conn,$query);
if(!$result)
{
	echo mysqli_error($conn);
	exit();
}
if(mysqli_num_rows($result) > 0)
 {
 $output .= '
   <table border="1">  
                    <tr>  
						 <th>Branch</th>  
                         <th>Company</th>  
                         <th>Bilty No</th>  
                         <th>LR Date</th>  
                         <th>Sys Date</th>  
                         <th>Party Lrno</th>  
                         <th>From</th>  
                         <th>To</th>  
                         <th>Truck No</th>  
                         <th>Chrg Wt</th>  
                         <th>Rate</th>  
                         <th>Total Amt</th>  
                    </tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
    <tr>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["company"].'</td>  
							<td>'.$row["bilty_no"].'</td>  
							<td>'.$row["lrdate"].'</td>  
							<td>'.$row["date"].'</td>  
						   <td>'.$row["plr"].'</td>  
						  <td>'.$row["frmstn"].'</td>
						   <td>'.$row["tostn"].'</td>
						   <td>'.$row["tno"].'</td>
						  <td>'.$row["cwt"].'</td>
						   <td>'.$row["rate"].'</td>
						   <td>'.$row["tamt"].'</td>
		</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Mkt_Bilty_'.$today1.'_'.$branch.'.xls');
  echo $output;
  
  $q = mysqli_query($conn,"UPDATE mkt_bilty SET colset_d='1',timestamp_download='$timestamp' WHERE colset_d!='1' AND colset='1' 
  AND branch='$branch'");
if(!$q)
{
	echo mysqli_error($conn);
}
}
 else
 {
	 echo "<script>
		alert('No result found..');
		window.location.href='./';
		</script>";
 }
?>