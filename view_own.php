<?php
require_once 'connect.php';
$memo=mysqli_real_escape_string($conn,strtoupper($_POST['memo']));
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>
</head>

<style> 
.form-control{
	text-transform:uppercase;
	border:1px solid #000;
}
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style> 

<style type="text/css" media="print">
 /* @media print {

  // body {
   // zoom:80%;
 // }
// }*/

</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.container-fluid * { visibility: visible;font-size:10px; font-family:Verdana;}
.container-fluid { position: absolute; top: 0; left: 0;}
}
</style>

<body style="background:#01ABAA">

<a href="./"><button style="font-family:Verdana;margin-top:10px;margin-left:10px" class="btn btn-danger">Dashboard</button></a>
<div class="container-fluid">
<span style="font-family:Verdana;font-size:22px;color:#fff;"><center>OWN Truck LR - <span style="color:#000"><?php echo $memo; ?></span></center></span>
        <div class="row">
	<div class="col-md-12">
<div class="row">
    <div class="col-md-12">
      <br />
      <div class="table-responsive" style="overflow-x:auto">
	<table class="table table-bordered" style="width:100%;background:#FFF;font-size:10px;font-family:Verdana">								   
<tr>
	<th>SrNo</th>
	<th>LR No</th>
	<th>Truck No</th>
	<th>Date</th>
	<th>From</th>
	<th>To</th>
	<th>Act Wt</th>
	<th>Chrg Wt</th>
	<th>Crossing</th>
</tr>
<?php 
$qmemo=mysqli_query($conn,"SELECT lrno,truck_no,date,fstation,tstation,wt12,weight,crossing FROM freight_form_lr where frno='$memo'");
$qmemo2=mysqli_query($conn,"select sum(wt12),sum(weight) from freight_form_lr where frno='$memo'");
$row2=mysqli_fetch_array($qmemo2);
if(mysqli_num_rows($qmemo)>0)
    {
		$id1=1;
		
    	while($rmemo = mysqli_fetch_assoc($qmemo))
    	{ 
	$date11 = date('d-m-y', strtotime($rmemo['date']));

	echo '<tr>
				<td>'.$id1.'</td>
				<td>'.$rmemo['lrno'].'</td>
				<td>'.$rmemo['truck_no'].'</td>
				<td>'.$date11.'</td>
				<td>'.$rmemo['fstation'].'</td>
				<td>'.$rmemo['tstation'].'</td>
				<td>'.$rmemo['wt12'].'</td>
				<td>'.$rmemo['weight'].'</td>
				<td>'.$rmemo['crossing'].'</td>
			</tr>
                ';

			$id1++;	
				
	}
 echo  '<tr>
				<td colspan=6><b>The Total Calculation of Freight Form LR :</b></td>				
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2['sum(wt12)']).'</b></td>
				<td style="letter-spacing:1px;"><b>'.sprintf("%.2f",$row2['sum(weight)']).'</b></td>
				<td style="letter-spacing:1px;"></td>
				
    		</tr>';
  }
    else
    {
    	// records now found 
    	echo '<tr><td colspan="16"><b>Records not found..!</b></td></tr>';
    }
echo "</table>";
?>
</div>
	</div>
		</div>
			<br />
			<div class="row">
	
		</div>
		</div>
		</div>
		</div>
		
		<center>
<button onclick="print();" style="font-family:Verdana" class="btn btn-primary btn-lg">Print This</button>
</center>
		
<style>
body {
    position: relative
}
#floatingRectangle {
    z-index: 1;
    position: absolute;
    right:0;
}
.form-control{border:1px solid #000;}
</style>
 
</body>
</html>