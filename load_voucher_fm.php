<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$type = escapeString($conn,$_POST['type']);
$timestamp = date("Y-m-d H:i:s");

if($type=='ADVANCE')
{
	$get_cache_data = Qry($conn,"SELECT id,frno,newdate,totalf,totaladv,ptob,adv_date,pto_adv_name,baladv,colset,re_download 
	FROM freight_memo_adv_cache WHERE branch='$branch'");
	
	if(!$get_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache_data)>0)
	{
		$ext_warning = "<center><span style='color:red'>Pending data found in Cache !</span></center>";
	}
	else
	{
		
	$dlt_cache_data = Qry($conn,"DELETE FROM freight_memo_adv_cache WHERE branch='$branch'");
	
	if(!$dlt_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$dlt_cache_data_lr = Qry($conn,"DELETE FROM freight_memo_adv_cache_lr WHERE branch='$branch'");
	
	if(!$dlt_cache_data_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$qry = Qry($conn,"INSERT INTO freight_memo_adv_cache(tab_id,frno,branch,newdate,actualf,newtds,dsl_inc,gps,adv_claim,
	newother,tds,totalf,totaladv,ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,chqno,disadv,rtgsneftamt,rtgs_adv,narre,
	baladv,re_download) SELECT id,frno,branch,newdate,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,
	ptob,adv_date,pto_adv_name,adv_pan,cashadv,chqadv,chqno,disadv,rtgsneftamt,rtgs_adv,narre,baladv,
	IF(colset_adv='1','1','0') FROM freight_form WHERE colset_d_adv=0 AND branch='$branch' AND rtgs_adv='1'");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(AffectedRows($conn)==0)
	{
		echo "
		<br />
		<center><h4 style='color:red'>No Pending Advance Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
		exit();
	}
	
	$qry_lr = Qry($conn,"INSERT INTO freight_memo_adv_cache_lr(table_id,frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,
	consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp) 
	SELECT id,frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,
	crossing,cross_to,cross_to_branch,cross_station,cross_tno,timestamp FROM freight_form_lr WHERE frno 
	in(SELECT frno from freight_memo_adv_cache WHERE branch='$branch')");
	
	if(!$qry_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$update_downloaded = Qry($conn,"UPDATE freight_form SET colset_adv='1',colset_d_adv='1',adv_download='$timestamp' WHERE 
	id in(SELECT tab_id FROM freight_memo_adv_cache WHERE branch='$branch')");
	
	if(!$update_downloaded){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$get_fm_data = Qry($conn,"SELECT frno,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,cashadv,chqadv,disadv,
	rtgsneftamt,baladv,re_download FROM freight_memo_adv_cache WHERE branch='$branch'");
	
	if(!$get_fm_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_fm_data)==0)
	{
		echo "<br />
		<center><h4 style='color:red'>No Pending Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
		exit();
	}
	
		while($row_fm = fetchArray($get_fm_data))
		{
			$db_af=$row_fm['actualf'];
			$load=$row_fm['newtds'];
			$dsl_inc=$row_fm['dsl_inc'];
			$gps=$row_fm['gps'];
			$claim=$row_fm['adv_claim'];
			$other=$row_fm['newother'];
			$tds=$row_fm['tds'];	
			$cash=$row_fm['cashadv'];	
			$disamt=$row_fm['disadv'];	
			$chqamt=$row_fm['chqadv'];	
			$rtgs=$row_fm['rtgsneftamt'];	
			$db_advance=$cash+$disamt+$chqamt+$rtgs;	
			
			$get_lr_data = Qry($conn,"SELECT id,table_id,lrno,actualf FROM freight_memo_adv_cache_lr WHERE frno='$row_fm[frno]'");
			if(!$get_lr_data){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}
			
			if(numRows($get_lr_data)==1)
			{
				$db_af_one=($db_af+$load+$dsl_inc)-($gps+$claim+$other+$tds);	
				$db_advance=$cash+$disamt+$chqamt+$rtgs;
				$db_balance=$db_af_one-$db_advance;
				
				$update_data=Qry($conn,"UPDATE freight_memo_adv_cache_lr SET loading='$load',dsl_inc='$dsl_inc',gps='$gps',
				claim='$claim',other='$other',tds='$tds',total_freight='$db_af_one',total_adv='$db_advance',cash='$cash',
				cheque='$chqamt',diesel='$disamt',rtgs='$rtgs',balance='$db_balance' WHERE frno='$row_fm[frno]'");
				if(!$update_data){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
				
				$row_lr_data = fetchArray($get_lr_data);
				
				$update_data_lr_table = Qry($conn,"UPDATE freight_form_lr SET total_freight='$db_af_one',
				total_advance='$db_advance',balance_amount='$db_balance' WHERE id='$row_lr_data[table_id]'");
				
				if(!$update_data_lr_table){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
			}
			else
			{
				while($row_lr_data = fetchArray($get_lr_data))
				{
					$db_lr=$row_lr_data['lrno'];
					$lr_actualf=$row_lr_data['actualf'];
					$lr_id=$row_lr_data['id'];
							
					$x=($lr_actualf*100)/$db_af;

					$load_db=sprintf("%.2f",((($load)*$x)/100));
					$dsl_inc_db=sprintf("%.2f",((($dsl_inc)*$x)/100));
					$gps_db=sprintf("%.2f",((($gps)*$x)/100));
					$claim_db=sprintf("%.2f",((($claim)*$x)/100));
					$other_db=sprintf("%.2f",((($other)*$x)/100));
					$tds_db=sprintf("%.2f",((($tds)*$x)/100));
					$cash_db=sprintf("%.2f",((($cash)*$x)/100));
					$diesel_db=sprintf("%.2f",((($disamt)*$x)/100));
					$cheq_db=sprintf("%.2f",((($chqamt)*$x)/100));
					$rtgs_db=sprintf("%.2f",((($rtgs)*$x)/100));

					$totalf_db=sprintf("%.2f",(($lr_actualf+$load_db+$dsl_inc_db)-($gps_db+$claim_db+$other_db+$tds_db)));
					$advance_db=sprintf("%.2f",($cash_db+$cheq_db+$diesel_db+$rtgs_db));
					$balance_db=sprintf("%.2f",($totalf_db-$advance_db));
					
					$update_data=Qry($conn,"UPDATE freight_memo_adv_cache_lr SET loading='$load_db',dsl_inc='$dsl_inc_db',
					gps='$gps_db',claim='$claim_db',other='$other_db',tds='$tds_db',total_freight='$totalf_db',
					total_adv='$advance_db',cash='$cash_db',cheque='$cheq_db',diesel='$diesel_db',rtgs='$rtgs_db',
					balance='$balance_db' WHERE id='$lr_id'");
					if(!$update_data){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					} 
					
					$update_data_lr_table2 = Qry($conn,"UPDATE freight_form_lr SET total_freight='$totalf_db',
					total_advance='$advance_db',balance_amount='$balance_db' WHERE id='$row_lr_data[table_id]'");
					
					if(!$update_data_lr_table2){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
				}
				
				$check_sum = Qry($conn,"SELECT SUM(loading) as load_lr,SUM(dsl_inc) as dsl_inc_lr,SUM(gps) as gps_lr,
					SUM(claim) as claim_lr,SUM(other) as other_lr,SUM(tds) as tds_lr,SUM(cash) as cash_lr,
					SUM(cheque) as cheque_lr,SUM(diesel) as diesel_lr,SUM(rtgs) as rtgs_lr FROM freight_memo_adv_cache_lr 
					WHERE frno='$row_fm[frno]'");
					if(!$check_sum){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
					
					$row_sum=fetchArray($check_sum);

					$adv_update_flag=false;
					
					if($load!=$row_sum['load_lr'])
					{
						$load_diff=sprintf("%.2f",($load-$row_sum['load_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$load_diff=0;
					}
					
					if($gps!=$row_sum['gps_lr'])
					{
						$gps_diff=sprintf("%.2f",($gps-$row_sum['gps_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$gps_diff=0;
					}
					
					if($claim!=$row_sum['claim_lr'])
					{
						$claim_diff=sprintf("%.2f",($claim-$row_sum['claim_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$claim_diff=0;
					}

					if($dsl_inc!=$row_sum['dsl_inc_lr'])
					{
						$dsl_inc_diff=sprintf("%.2f",($dsl_inc-$row_sum['dsl_inc_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$dsl_inc_diff=0;
					}

					if($other!=$row_sum['other_lr'])
					{
						$other_diff=sprintf("%.2f",($other-$row_sum['other_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$other_diff=0;
					}

					if($tds!=$row_sum['tds_lr'])
					{
						$tds_diff=sprintf("%.2f",($tds-$row_sum['tds_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$tds_diff=0;
					}

					if($cash!=$row_sum['cash_lr'])
					{
						$cash_diff2=sprintf("%.2f",($cash-$row_sum['cash_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$cash_diff2=0;
					}

					if($chqamt!=$row_sum['cheque_lr'])
					{
						$cheque_diff=sprintf("%.2f",($chqamt-$row_sum['cheque_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$cheque_diff=0;
					}

					if($disamt!=$row_sum['diesel_lr'])
					{
						$diesel_diff=sprintf("%.2f",($disamt-$row_sum['diesel_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$diesel_diff=0;
					}

					if($rtgs!=$row_sum['rtgs_lr'])
					{
						$rtgs_diff=sprintf("%.2f",($rtgs-$row_sum['rtgs_lr']));
						$adv_update_flag=true;
					}
					else
					{
						$rtgs_diff=0;
					}
					
					$diff1 = ($load_diff+$dsl_inc_diff)-($gps_diff+$claim_diff+$other_diff+$tds_diff);
					$diff2 = ($cash_diff2+$cheque_diff+$diesel_diff+$rtgs_diff);
					
					if($adv_update_flag)
					{
						$get_desc_id = Qry($conn,"SELECT id,table_id FROM freight_memo_adv_cache_lr WHERE frno='$row_fm[frno]' 
						ORDER BY actualf DESC,id DESC LIMIT 1");
						
						if(!$get_desc_id){
							echo getMySQLError($conn);
							errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
							exit();
						}
						
						$row_my_id = fetchArray($get_desc_id);
						
						$update_data_lr = Qry($conn,"UPDATE freight_memo_adv_cache_lr SET loading=loading+'$load_diff',
						dsl_inc=dsl_inc+'$dsl_inc_diff',gps=gps+'$gps_diff',claim=claim+'$claim_diff',
						other=other+'$other_diff',tds=tds+'$tds_diff',total_freight=total_freight+'$diff1',
						total_adv=total_adv+'$diff2',cash=cash+'$cash_diff2',cheque=cheque+'$cheque_diff',
						diesel=diesel+'$diesel_diff',rtgs=rtgs+'$rtgs_diff',balance=total_freight-total_adv 
						WHERE id='$row_my_id[id]'");
						
						if(!$update_data_lr){
							echo getMySQLError($conn);
							errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
							exit();
						}
					
						$update_data_lr_table = Qry($conn,"UPDATE freight_form_lr SET total_freight='$db_af_one',
						total_advance='$db_advance',balance_amount='$db_balance' WHERE id='$row_my_id[table_id]'");
						
						if(!$update_data_lr_table){
							echo getMySQLError($conn);
							errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
							exit();
						}
					}
			}
		}
		
	$get_cache_data = Qry($conn,"SELECT id,frno,newdate,totalf,totaladv,ptob,adv_date,pto_adv_name,baladv,colset,re_download 
	FROM freight_memo_adv_cache WHERE branch='$branch'");
		
	if(!$get_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
		
		$ext_warning = "";
	}
	
	echo $ext_warning;
		
	echo "<form action='download_voucher_fm.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	 
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Vou_date</th>
				<th>Freight</th>
				<th>Advance</th>
				<th>Adv.To</th>
				<th>Adv.Date</th>
				<th>Adv.Party</th>
				<th>Balance</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache_data))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			if($row['re_download']=="1"){
				echo "<tr class='bg-warning'>";
			}
			else{
				echo "<tr>";
			}
			
			echo "
				<td>$sn</td>
				<td>$row[frno]</td>
				<td>$row[totalf]</td>
				<td>$row[newdate]</td>
				<td>$row[totaladv]</td>
				<td>$row[ptob]</td>
				<td>$row[adv_date]</td>
				<td>$row[pto_adv_name]</td>
				<td>$row[baladv]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
}
else
{
	$get_cache_data = Qry($conn,"SELECT id,frno,newdate,totalf,totaladv,totalbal,paidto,bal_date,pto_bal_name,colset,
	re_download FROM freight_memo_bal_cache WHERE branch='$branch'");
	if(!$get_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_cache_data)>0)
	{
		$ext_warning = "<center><span style='color:red'>Pending data found in Cache !</span></center>";
	}
	else
	{
		
	$dlt_cache_data = Qry($conn,"DELETE FROM freight_memo_bal_cache WHERE branch='$branch'");
	
	if(!$dlt_cache_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$dlt_cache_data_lr = Qry($conn,"DELETE FROM freight_memo_bal_cache_lr WHERE branch='$branch'");
	
	if(!$dlt_cache_data_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$qry = Qry($conn,"INSERT INTO freight_memo_bal_cache(tab_id,frno,newdate,truck_no,branch,company,actualf,newtds,dsl_inc,gps,adv_claim,
	newother,tds,totalf,totaladv,cashadv,chqadv,disadv,rtgsneftamt,baladv,unloadd,detention,gps_deposit_return,gps_rent,gps_device_charge,bal_tds,otherfr,
	claim,late_pod,totalbal,paidto,bal_date,pto_bal_name,bal_pan,paycash,paycheq,paycheqno,paydsl,newrtgsamt,narra,re_download) SELECT 
	id,frno,newdate,truck_no,branch_bal,company,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,totaladv,cashadv,chqadv,disadv,
	rtgsneftamt,baladv,unloadd,detention,gps_deposit_return,gps_rent,gps_device_charge,bal_tds,otherfr,claim,late_pod,totalbal,paidto,bal_date,pto_bal_name,
	bal_pan,paycash,paycheq,paycheqno,paydsl,newrtgsamt,narra,IF(colset_bal='1','1','0') 
	FROM freight_form WHERE colset_d_bal=0 AND branch_bal='$branch' AND rtgs_bal='1' AND colset_d_adv='1'");
	 
	if(!$qry){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(AffectedRows($conn)==0)
	{
		echo "
		<br />
		<center><h4 style='color:red'>No Pending Balance Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
		exit();
	}
	
	$qry_lr = Qry($conn,"INSERT INTO freight_memo_bal_cache_lr(table_id,frno,company,branch,date,create_date,truck_no,lrno,
	fstation,tstation,consignor,consignee,wt12,weight,ratepmt,fix_rate,actualf,total_freight,total_adv,balance) SELECT id,
	frno,company,branch,date,create_date,truck_no,lrno,fstation,tstation,consignor,consignee,wt12,weight,ratepmt,fix_rate,
	actualf,total_freight,total_advance,balance_amount FROM freight_form_lr WHERE 
	frno in(SELECT frno from freight_memo_bal_cache WHERE branch='$branch')");
	
	if(!$qry_lr){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$update_downloaded = Qry($conn,"UPDATE freight_form SET colset_bal='1',colset_d_bal='1',bal_download='$timestamp' WHERE 
	id in(SELECT tab_id FROM freight_memo_bal_cache WHERE branch='$branch')");
	if(!$update_downloaded){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	$get_fm_data = Qry($conn,"SELECT frno,actualf,newtds,dsl_inc,gps,adv_claim,newother,tds,totalf,cashadv,chqadv,disadv,
	rtgsneftamt,baladv,unloadd,detention,gps_deposit_return,gps_rent,gps_device_charge,bal_tds,otherfr,claim,late_pod,totalbal,paycash,paycheq,paydsl,
	newrtgsamt FROM freight_memo_bal_cache WHERE branch='$branch'");
	
	if(!$get_fm_data){
		echo getMySQLError($conn);
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}
	
	if(numRows($get_fm_data)==0)
	{
		echo "<br />
		<center><h4 style='color:red'>No Pending Voucher found !</h4></center>
		<script>$('#loadicon').fadeOut();</script>
		";
		exit();
	}
	
		while($row_fm = fetchArray($get_fm_data))
		{
			$db_af=$row_fm['actualf'];
			$load=$row_fm['newtds'];
			$dsl_inc=$row_fm['dsl_inc'];
			$gps=$row_fm['gps'];
			$claim=$row_fm['adv_claim'];
			$other=$row_fm['newother'];
			$tds=$row_fm['tds'];	
			$cash=$row_fm['cashadv'];	
			$disamt=$row_fm['disadv'];	
			$chqamt=$row_fm['chqadv'];	
			$rtgs=$row_fm['rtgsneftamt'];	
			$total_freight_new = ($db_af+$load+$dsl_inc-($gps+$claim+$other+$tds));
			$db_advance=$cash+$disamt+$chqamt+$rtgs;
			$balance_new = $total_freight_new-$db_advance;
			$unloading=$row_fm['unloadd'];	
			$detention=$row_fm['detention'];	
			$gps_rent=$row_fm['gps_rent'];	
			$gps_deposit_return=$row_fm['gps_deposit_return'];	
			$gps_device_charge=$row_fm['gps_device_charge'];	
			$bal_tds=$row_fm['bal_tds'];	
			$other2=$row_fm['otherfr'];	
			$bal_claim=$row_fm['claim'];	
			$late_pod=$row_fm['late_pod'];	
			$total_balance=$row_fm['totalbal'];	
			$cash2=$row_fm['paycash'];	
			$diesel2=$row_fm['paydsl'];	
			$cheque2=$row_fm['paycheq'];	
			$rtgs2=$row_fm['newrtgsamt'];	
			
			$get_lr_data = Qry($conn,"SELECT id,lrno,actualf,total_freight,total_adv,balance FROM freight_memo_bal_cache_lr 
			WHERE frno='$row_fm[frno]'");
			
			if(!$get_lr_data){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				exit();
			}
			
			if(numRows($get_lr_data)==1)
			{
				$db_af_one=($db_af+$load+$dsl_inc)-($gps+$claim+$other+$tds);	
				$db_advance=$cash+$disamt+$chqamt+$rtgs;
				$db_balance=$db_af_one-$db_advance;
				$db_total_balance=$cash2+$diesel2+$cheque2+$rtgs2;
				
				$update_data=Qry($conn,"UPDATE freight_memo_bal_cache_lr SET total_freight='$total_freight_new',
				total_adv='$db_advance',balance='$balance_new',unloading='$unloading',detention='$detention',gps_deposit_return='$gps_deposit_return',
				gps_rent='$gps_rent',gps_device_charge='$gps_device_charge',bal_tds='$bal_tds',other2='$other2',
				bal_claim='$bal_claim',late_pod='$late_pod',total_balance='$db_total_balance',cash2='$cash2',
				cheque2='$cheque2',diesel2='$diesel2',rtgs2='$rtgs2' WHERE frno='$row_fm[frno]'");
				
				if(!$update_data){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
			}
			else
			{
				while($row_lr_data = fetchArray($get_lr_data))
				{
					$db_lr=$row_lr_data['lrno'];
					$lr_actualf=$row_lr_data['actualf'];
					$lr_id=$row_lr_data['id'];
					
					// get balance amount 
					
					$x=($lr_actualf*100)/$db_af;
					
					$load_db=sprintf("%.2f",((($load)*$x)/100));
					$dsl_inc_db=sprintf("%.2f",((($dsl_inc)*$x)/100));
					$gps_db=sprintf("%.2f",((($gps)*$x)/100));
					$claim_db=sprintf("%.2f",((($claim)*$x)/100));
					$other_db=sprintf("%.2f",((($other)*$x)/100));
					$tds_db=sprintf("%.2f",((($tds)*$x)/100));
					$cash_db=sprintf("%.2f",((($cash)*$x)/100));
					$diesel_db=sprintf("%.2f",((($disamt)*$x)/100));
					$cheq_db=sprintf("%.2f",((($chqamt)*$x)/100));
					$rtgs_db=sprintf("%.2f",((($rtgs)*$x)/100));

					$totalf_db=sprintf("%.2f",(($lr_actualf+$load_db+$dsl_inc_db)-($gps_db+$claim_db+$other_db+$tds_db)));
					$advance_db=sprintf("%.2f",($cash_db+$cheq_db+$diesel_db+$rtgs_db));
					$balance_db=sprintf("%.2f",($totalf_db-$advance_db));
					
					// get balance amount 
					
					$bal_amt_lr=$balance_db;
							
					$x=($bal_amt_lr*100)/$balance_new;

					$unloading_db=sprintf("%.2f",((($unloading)*$x)/100));
					$detention_db=sprintf("%.2f",((($detention)*$x)/100));
					$gps_deposit_return_db=sprintf("%.2f",((($gps_deposit_return)*$x)/100));
					$gps_rent_db=sprintf("%.2f",((($gps_rent)*$x)/100));
					$gps_device_charge_db=sprintf("%.2f",((($gps_device_charge)*$x)/100));
					$bal_tds_db=sprintf("%.2f",((($bal_tds)*$x)/100));
					$other2_db=sprintf("%.2f",((($other2)*$x)/100));
					$bal_claim_db=sprintf("%.2f",((($bal_claim)*$x)/100));
					$late_pod_db=sprintf("%.2f",((($late_pod)*$x)/100));
					
					$cash2_db=sprintf("%.2f",((($cash2)*$x)/100));
					$diesel2_db=sprintf("%.2f",((($diesel2)*$x)/100));
					$cheque2_db=sprintf("%.2f",((($cheque2)*$x)/100));
					$rtgs2_db=sprintf("%.2f",((($rtgs2)*$x)/100));

					$total_balance_db=sprintf("%.2f",(($bal_amt_lr+$unloading_db+$detention_db+$gps_deposit_return_db)-($gps_rent_db+$gps_device_charge_db+$bal_tds_db+$other2_db+$bal_claim_db+$late_pod_db)));
				
					$update_data=Qry($conn,"UPDATE freight_memo_bal_cache_lr SET total_freight='$totalf_db',
					total_adv='$advance_db',balance='$balance_db',unloading='$unloading_db',
					detention='$detention_db',gps_deposit_return='$gps_deposit_return_db',gps_rent='$gps_rent_db',gps_device_charge='$gps_device_charge_db',
					bal_tds='$bal_tds_db',other2='$other2_db',bal_claim='$bal_claim_db',late_pod='$late_pod_db',
					total_balance='$total_balance_db',cash2='$cash2_db',cheque2='$cheque2_db',diesel2='$diesel2_db',
					rtgs2='$rtgs2_db' WHERE id='$lr_id'");
					
					if(!$update_data){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					} 
				}
				
				$check_sum = Qry($conn,"SELECT SUM(total_freight) as total_freight_lr,SUM(total_adv) as total_adv_lr,
				SUM(balance) as balance_lr,SUM(unloading) as unloadd_lr,SUM(detention) as detention_lr,SUM(gps_deposit_return) as gps_deposit_return_lr,
				SUM(gps_rent) as gps_rent_lr,SUM(gps_device_charge) as gps_charge_lr,SUM(bal_tds) as bal_tds_lr,
				SUM(other2) as otherfr_lr,SUM(bal_claim) as claim_lr,
				SUM(late_pod) as late_pod_lr,SUM(total_balance) as total_balance_lr,SUM(cash2) as paycash_lr,
				SUM(cheque2) as paycheq_lr,SUM(diesel2) as paydsl_lr,SUM(rtgs2) as rtgs_lr FROM freight_memo_bal_cache_lr 
				WHERE frno='$row_fm[frno]'");
					
				if(!$check_sum){
					echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					exit();
				}
					
					$row_sum=fetchArray($check_sum);

					$bal_update_flag=false;
					
					if($total_freight_new!=$row_sum['total_freight_lr']){
						$total_freight_lr_diff=sprintf("%.2f",($total_freight_new-$row_sum['total_freight_lr']));
						$bal_update_flag = true;
					}
					else{
						$total_freight_lr_diff=0;
					}
					
					if($db_advance!=$row_sum['total_adv_lr']){
						$total_adv_lr_diff=sprintf("%.2f",($db_advance-$row_sum['total_adv_lr']));
						$bal_update_flag = true;
					}
					else{
						$total_adv_lr_diff=0;
					}
					
					if($balance_new!=$row_sum['balance_lr']){
						$balance_lr_diff=sprintf("%.2f",($balance_new-$row_sum['balance_lr']));
						$bal_update_flag = true;
					}
					else{
						$balance_lr_diff=0;
					}
					
					
					if($unloading!=$row_sum['unloadd_lr']){
						$unloading_diff=sprintf("%.2f",($unloading-$row_sum['unloadd_lr']));
						$bal_update_flag = true;
					}
					else{
						$unloading_diff=0;
					}
					
					if($detention!=$row_sum['detention_lr'])
					{
						$detention_diff=sprintf("%.2f",($detention-$row_sum['detention_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$detention_diff=0;
					}
					
					if($gps_deposit_return!=$row_sum['gps_deposit_return_lr'])
					{
						$gps_deposit_return_diff = sprintf("%.2f",($gps_deposit_return-$row_sum['gps_deposit_return_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$gps_deposit_return_diff=0;
					}
					
					if($gps_rent!=$row_sum['gps_rent_lr'])
					{
						$gps_rent_diff=sprintf("%.2f",($gps_rent-$row_sum['gps_rent_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$gps_rent_diff=0;
					}

					if($gps_device_charge!=$row_sum['gps_charge_lr'])
					{
						$gps_dev_diff=sprintf("%.2f",($gps_device_charge-$row_sum['gps_charge_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$gps_dev_diff=0;
					}

					if($bal_tds!=$row_sum['bal_tds_lr'])
					{
						$bal_tds_diff=sprintf("%.2f",($bal_tds-$row_sum['bal_tds_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$bal_tds_diff=0;
					}

					if($other2!=$row_sum['otherfr_lr'])
					{
						$other2_diff=sprintf("%.2f",($other2-$row_sum['otherfr_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$other2_diff=0;
					}
					
					if($bal_claim!=$row_sum['claim_lr'])
					{
						$bal_claim_diff=sprintf("%.2f",($bal_claim-$row_sum['claim_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$bal_claim_diff=0;
					}
					
					if($late_pod!=$row_sum['late_pod_lr'])
					{
						$late_pod_diff=sprintf("%.2f",($late_pod-$row_sum['late_pod_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$late_pod_diff=0;
					}
					
					if($total_balance!=$row_sum['total_balance_lr'])
					{
						$total_balance_lr_diff=sprintf("%.2f",($total_balance-$row_sum['total_balance_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$total_balance_lr_diff=0;
					}

					if($cash2!=$row_sum['paycash_lr'])
					{
						$cash2_diff=sprintf("%.2f",($cash2-$row_sum['paycash_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$cash2_diff=0;
					}

					if($diesel2!=$row_sum['paydsl_lr'])
					{
						$diesel2_diff=sprintf("%.2f",($diesel2-$row_sum['paydsl_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$diesel2_diff=0;
					}

					if($cheque2!=$row_sum['paycheq_lr'])
					{
						$cheque2_diff=sprintf("%.2f",($cheque2-$row_sum['paycheq_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$cheque2_diff=0;
					}

					if($rtgs2!=$row_sum['rtgs_lr'])
					{
						$rtgs2_diff=sprintf("%.2f",($rtgs2-$row_sum['rtgs_lr']));
						$bal_update_flag = true;
					}
					else
					{
						$rtgs2_diff=0;
					}
					
				if($bal_update_flag)
				{
					$update_data_lr = Qry($conn,"UPDATE freight_memo_bal_cache_lr SET total_freight=total_freight+'$total_freight_lr_diff',
					total_adv=total_adv+'$total_adv_lr_diff',balance=balance+'$balance_lr_diff',unloading=unloading+'$unloading_diff',
					detention=detention+'$detention_diff',gps_deposit_return=gps_deposit_return+'$gps_deposit_return_diff',
					gps_rent=gps_rent+'$gps_rent_diff',gps_device_charge=gps_device_charge+'$gps_dev_diff',
					bal_tds=bal_tds+'$bal_tds_diff',other2=other2+'$other2_diff',bal_claim=bal_claim+'$bal_claim_diff',
					late_pod=late_pod+'$late_pod_diff',total_balance=total_balance+'$total_balance_lr_diff',
					cash2=cash2+'$cash2_diff',cheque2=cheque2+'$cheque2_diff',diesel2=diesel2+'$diesel2_diff',
					rtgs2=rtgs2+'$rtgs2_diff' WHERE frno='$row_fm[frno]' ORDER BY balance DESC,id DESC LIMIT 1");
					
					if(!$update_data_lr){
						echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						exit();
					}
				}
			}
		}
		
		$get_cache_data = Qry($conn,"SELECT id,frno,newdate,totalf,totaladv,totalbal,paidto,bal_date,pto_bal_name,colset,re_download 
		FROM freight_memo_bal_cache WHERE branch='$branch'");
		
		if(!$get_cache_data){
			echo getMySQLError($conn);
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			exit();
		}
		
		$ext_warning = "";
	}
	
	echo $ext_warning;
		
	echo "<form action='download_voucher_fm.php' method='POST' target='_blank'>
			<input type='hidden' name='branch' value='$branch'>
			<input type='hidden' name='type' value='$type'>
			<button type='submit' class='btn btn-sm btn-primary'>Download</button>
		</form>	 
		<table class='table table-bordered table-striped' style='font-size:13px;'>
			<tr>
				<th>#</th>
				<th>Vou_No</th>
				<th>Vou_date</th>
				<th>Freight</th>
				<th>Advance</th>
				<th>Balance</th>
				<th>Bal.To</th>
				<th>Bal.Date</th>
				<th>Bal.Party</th>
				<th><button type='button' id='approve_all_button' onclick=ApproveAll('$branch') class='btn btn-sm btn-success'>Approve all</button></th>
			</tr>";
			
		$sn=1;	
		while($row = fetchArray($get_cache_data))
		{
			if($row['colset']=="1")
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' disabled onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approved</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Reject</button>";
			}
			else
			{
				$button1 = "<button type='button' id='approve_button_$row[id]' onclick=Approve('$row[id]') class='approve_button btn btn-sm btn-success'>Approve</button>";
				$button2 = "<button type='button' id='reject_button_$row[id]' disabled onclick=Reject('$row[id]') class='reject_button btn btn-sm btn-danger'>Rejected</button>";
			}
			
			if($row['re_download']=="1"){
				echo "<tr class='bg-warning'>";
			}
			else{
				echo "<tr>";
			}
			
			echo "
				<td>$sn</td>
				<td>$row[frno]</td>
				<td>$row[totalf]</td>
				<td>$row[newdate]</td>
				<td>$row[totaladv]</td>
				<td>$row[totalbal]</td>
				<td>$row[paidto]</td>
				<td>$row[bal_date]</td>
				<td>$row[pto_bal_name]</td>
				<td>$button1 $button2</td>
			</tr>";
		$sn++;	
		}
		echo "</table>";
}

echo "<script>
	$('#loadicon').fadeOut();
</script>";
?>

<script>
function ApproveAll(branch)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher_all_fm.php",
			data: 'branch=' + branch + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}

function Approve(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./approve_voucher_fm.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
} 

function Reject(id)
{
	$("#loadicon").show();
		jQuery.ajax({
			url: "./reject_voucher_fm.php",
			data: 'id=' + id + '&type=' + '<?php echo $type; ?>',
			type: "POST",
			success: function(data){
			$("#result_div2").html(data);
		},
		error: function() {}
	});
}
</script>