<?php				
require_once 'connect.php';

if(!isset($_SESSION['ho']))
{
	echo "<script>
	alert('Branch Login Error...');
	window.location.href='./logout.php';
	</script>";
	mysqli_close($conn);
	exit();
}

$idmemo=$_REQUEST['idmemo'];

$qpto=mysqli_query($conn,"SELECT paidto,newdate,adv_date,bal_date,branch_bal,pod_date FROM freight_form WHERE frno='$idmemo'");
$qpto1=mysqli_query($conn,"SELECT branch FROM rcv_pod WHERE frno='$idmemo'");

$rowpto1 = mysqli_fetch_array($qpto1);

$pod_by=$rowpto1['branch'];

	if($qpto)
	{
		$rowpto = mysqli_fetch_array($qpto);
		$pto1 = $rowpto['paidto'];
		$fmdt = $rowpto['newdate'];
		$adv_dt = $rowpto['adv_date'];
		$bal_dt = $rowpto['bal_date'];
		$balance_by = $rowpto['branch_bal'];
		$pod_date = $rowpto['pod_date'];
		$fmdate = date('d-m-y', strtotime($fmdt));
		$adv_date1 = date('d-m-y', strtotime($adv_dt));
		$bal_date1 = date('d-m-y', strtotime($bal_dt));
	}
	else
	{
		echo mysqli_error($conn);
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <style type="text/css">
@media print
{
body * { visibility: hidden; }
#printpage * { visibility: visible; }
#printpage { position: absolute; top: 0; left: 0; }
}
</style>

  </head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body style="overflow-x: scroll !important;">
    <style>
        .navbar-inverse {
            
        }
        @media screen and (max-width: 768px) {
            .asd {
                padding-top: 30px;
            }
    </style>
	
	<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
input[type="text"],input[type="number"]
{
	border:1px solid #000;
}
</style>

    <?php //include 'sidebar2.php';?>
<a href="./">
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;letter-spacing:2px;color:#FFF;font-family:Verdana" class="btn btn-primary">Dashboard</button></a>
<div id="printpage">
    <div class="container-fluid">
<div class="row" style="font-family:Verdana">
	 
	 <center>
	 <div>
	 <span class="" style="font-size:16px;letter-spacing:1px">Freight Memo : <?php echo strtoupper($idmemo); ?></span>
	</div>
<br />	
	 <div class="pull-right">
	<span style="font-size:13px;margin-right:10px">FM Date : <?php echo $fmdate; ?>,</span>
	<span style="font-size:13px;margin-right:5px">Adv Date : <?php echo $adv_date1; ?>,</span>
	<span style="font-size:13px;margin-right:5px">Bal Date : <?php echo $bal_date1; ?></span>
		</div>
		</center>
 </div>
<input type="text" id="fids" name="fids" value="<?php echo $idmemo; ?>" style="visibility:hidden;" />
<div class="row">
<div class="col-md-12" style="letter-spacing:0px; font-size:12px; font-family: 'Verdana', cursive;" >
<?php 
$sql3="SELECT branch,company,truck_no FROM freight_form WHERE frno='$idmemo'";
if($result3 = mysqli_query($conn,$sql3)) {
if(mysqli_num_rows($result3) > 0)  {
$row = mysqli_fetch_array($result3);
?>        
<table border="0" width="100%" style="font-size:12px;">
	<tr>
		<td>
           <label>Truck No: &nbsp;</label>
           <?php echo $row ['truck_no']; ?>
        </td>
		
		<td>
           <label>Company: &nbsp;</label>
           <?php echo $row ['company']; ?>
        </td>
        
		<td>
			<label>Branch: &nbsp;</label>
           <?php echo $row ['branch']; ?>
        </td>
	</tr>		
<?php 
} }
$sql2="SELECT mk_broker.name, mk_broker.mo1, mk_broker.pan, mk_broker.id FROM mk_broker, freight_form WHERE freight_form.bid = mk_broker.id AND freight_form.frno =  '$idmemo'";
if($result2 = mysqli_query($conn,$sql2))  {
if(mysqli_num_rows($result2) > 0)   {
$row = mysqli_fetch_array($result2);

?>     
	<tr>
        <td>   
			<label>Broker: &nbsp;</label>
           <?php echo $row ['name']; ?>
        </td>
		
		<td>
           <label>Broker Mo.: &nbsp;</label>
           <?php echo $row ['mo1']; ?>
        </td>
		
		<td>
           <label>Broker PAN: &nbsp;</label>
           <?php echo $row ['pan']; ?>
        </td>
</tr>		
<?php 
} } 

$sql="SELECT mk_driver.name, mk_driver.mo1, mk_driver.pan, mk_driver.id FROM mk_driver, freight_form WHERE freight_form.did = mk_driver.id AND freight_form.frno = '$idmemo'";

if($result = mysqli_query($conn,$sql))  {
if(mysqli_num_rows($result) > 0)  {
$row = mysqli_fetch_array($result);

?>
	<tr>
		<td>
           <label>Driver: &nbsp;</label>
           <?php echo $row ['name']; ?>
        </td>
        
		<td>
           <label>Driver Mo.: &nbsp;</label>
           <?php echo $row ['mo1']; ?>
		</td>
        
		<td>
			<label>Driver LIC: &nbsp;</label>
           <?php echo $row ['pan']; ?>
        </td>
</tr>		
<?php
} } 

$sql2="SELECT mk_truck.name, mk_truck.mo1, mk_truck.pan, mk_truck.id FROM mk_truck, freight_form WHERE freight_form.oid = mk_truck.id AND freight_form.frno =  '$idmemo'";

if($result2 = mysqli_query($conn,$sql2)) {
if(mysqli_num_rows($result2) > 0)  {
$row = mysqli_fetch_array($result2);

?>        
	<tr>
		<td>
			<label>Owner: &nbsp;</label>
           <?php echo $row ['name']; ?>
        </td>
		
		<td>
           <label>Owner Mobile: &nbsp;</label>
           <?php echo $row ['mo1']; ?>
        </td>
		
		<td>		
           <label>Owner PAN: &nbsp;</label>
           <?php echo $row ['pan']; ?>
        </td>
</tr>		
<?php
 } }
?>

<?php 

$sql45="SELECT * FROM freight_form WHERE frno='$idmemo'";
if($result45 = mysqli_query($conn,$sql45)) {
if(mysqli_num_rows($result45) > 0)   {
$row = mysqli_fetch_array($result45);

?>

</table>
<br />
<span style="color:blue;font-size:13px;font-weight:bold;">Advance Details</span>
<div style="border:1px solid #ddd;padding:5px;">
<table border="0" width="100%" style="font-size:12px;margin-top:10px">

<tr>
	<td width="230px">
        <label>Actual Freight:</label>
        <?php echo $row ['actualf']; ?>
    </td>
	
	<td style="width:230px">
			<label>Total Freight:</label>
           <?php echo $row ['totalf']; ?>
	</td>
	
	<td width="170px">	
        <label>TDS by:</label>
        <?php echo $row ['tdsby']; ?>
    </td>
	
	<td width="130px">
		<label>TDS:</label>
        <?php echo $row ['tds']; ?>
    </td>
	
	<td width="130px">
		<label>Load:</label>
        <?php echo $row ['newtds']; ?>
    </td>
       

	<td width="130px">
			<label>Other:</label>
           <?php echo $row ['newother']; ?>
        </td>
</tr>		

<tr>		
		<td width="200px" id="cash">
           <label>Cash Advance:</label>
           <?php echo $row ['cashadv']; ?>
        </td>
		
		<td width="200px">
           <label>Chq Advance:</label>
           <?php echo $row ['chqadv']; ?>
        </td>
	
	<td width="200px">
		<label>Chq No:</label>
        <?php echo $row ['chqno']; ?>
     </td>

		    <td width="200px">
           <label>RTGS/NEFT:</label>
           <?php echo $row ['rtgsneftamt']; ?>
        </td>
		
		<td width="200px">
	      <label>Diesel Advance:</label>
          <?php echo $row ['disadv']; ?>
    </td>
</tr>

<tr>
	<td>
		<label>Diesel Card No:</label>
        <?php echo $row ['discard']; ?>
    </td>
	<td>
		<label>Diesel Company:</label>
        <?php echo $row ['dcom']; ?>
    </td>

		<td width="150px">
           <label>Paid to:</label>
           <?php echo $row ['ptob']; ?>
        </td>

		<td width="200px">
           <label>Total Adv:</label>
           <?php echo $row ['totaladv']; ?>
        </td>
		
		<td>
           <label>Narration:</label>
           <?php echo $row ['narre']; ?>
		</td>

</tr>		
</table>
</div>	
<br />
<span style="color:blue;font-size:13px;font-weight:bold;">Balance Details</span>
<div style="border:1px solid #ddd;padding:5px;">
<table border="0" width="100%" style="font-size:12px;margin-top:5px;">
<tr>
	
	
	<td>
           <label>Balance:</label>
           <?php echo $row ['baladv']; ?>
     </td>
	 
     <td>
           <label>Unloading:</label>
           <?php echo $row ['unloadd']; ?>
     </td>

	<td>
		   <label>Other:</label>
           <?php echo $row ['otherfr']; ?>
    </td>

	<td>
           <label>Total Balance:</label>
           <?php echo $row ['totalbal']; ?>
    </td>
	<td>	
           <label>Cash Amount:</label>
           <?php echo $row ['paycash']; ?>
    </td>
	
</tr>

<tr> 	
	
	<td>
           <label>Cheq Amount:</label>
           <?php echo $row ['paycheq']; ?>
    </td>
	<td>
           <label>Cheque No:</label>
           <?php echo $row ['paycheqno']; ?>
    </td>
	<td>
           <label>RTGS/NEFT:</label>
          <?php echo $row ['newrtgsamt']; ?>
    </td>

	<td>
	      <label>Diesel Amt:</label>
          <?php echo $row ['paydsl']; ?>
    </td>
	
	<td>
		<label>Diesel Card:</label>
        <?php echo $row ['paydslcard']; ?>
    </td>

</tr>
<tr>
	<td>
		<label>Diesel Comp:</label>
        <?php echo $row ['paydslcom']; ?>
    </td>
	
	<td>
           <label>PAID TO : &nbsp; </label>
           <?php echo $pto1; ?>
    </td>
	
	<td>	
           <label>Narration:</label>
           <?php echo $row ['narra']; ?>
    </td>
	
	<td style="color:red">	
           <label>POD Branch:</label>
           <?php echo $pod_by."(".$pod_date.")"; ?>
    </td>
	
	<td style="color:red">	
           <label>Balance Branch:</label>
           <?php echo $balance_by; ?>
    </td>
</tr>
</table>		   
<?php
} }
?>
        </div>
        </div></div>
		</div>
<script type="text/javascript">
		setInterval(function(){ 
	jQuery.ajax({
			url: "./ajax/read_final.php",
			data: 'fids=' + $("#fids").val(),
			type: "POST",
			success: function(data) {
			$(".records_content").html(data);
			},
			error: function() {}
			});

}, 1000);					
</script>					                             		
		<br />
		<div class="container-fluid">
        <span style="font-weight:bold;font-size:12px;font-family: 'Verdana', cursive;">Freight LR Details: </span>
        <div class="records_content table-responsive" style="font-family:Verdana; color:#000; font-size:12px"></div>

<?php
$sql45="SELECT sign,sign2,sign3 FROM fm_sign WHERE frno='$idmemo'";
if(!mysqli_query($conn,$sql45))
{echo mysqli_error($conn);}
if($result45 = mysqli_query($conn,$sql45)) {

if(mysqli_num_rows($result45) > 0)   {
$row = mysqli_fetch_array($result45);

?>
<table border="0" style="width:100%">
	<tr>
		<td align="center"><label style="font-size:13px;">Cashier Sign</label></td>
		<td align="center"><label style="font-size:13px;">Adv Rcvr</label></td>
		<td align="center"><label style="font-size:13px;">Bal Rcvr</label></td>
	</tr>	
	<tr>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="<?php echo $row ['sign']; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="<?php echo $row ['sign2']; ?>"></td>
		<td align="center"><img class="img-responsive" style="border:0px red solid;width:150px;height:40px" src="<?php echo $row ['sign3']; ?>"></td>
	</tr>	
</table>
<?php
} }
?> 
 </div>    
		   </div>
	<br />	   
	<br />	   
<center><button style="font-family:Verdana;letter-spacing:1px;" class="btn btn-lg btn-danger" onclick="print()">Print Freight Memo</button></center>
<br />
<br />

</body>
</html>