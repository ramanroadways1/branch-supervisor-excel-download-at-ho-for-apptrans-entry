<?php 
require_once '../connect.php';

$date2 = date("Y-m-d"); 
$branch = $_POST['branch']; 

$max = date("Y-m-d");
$min = date("Y-m-d", strtotime("-30 day"));
?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	 <style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;">
<center><img style="margin-top:150px" src="../load.gif" /></center>
</div>

<script type="text/javascript">

		function chkApproval(idnew) {
			jQuery.ajax({
			url: "./check_approval.php",
			data: 'id=' + idnew,
			type: "POST",
			success: function(data) {
			$("#result_chk").html(data);
			},
			error: function() {}
			});
		}
</script>		

<script type="text/javascript">
function approval(idnew){
        $.ajax({
            type: "POST",
            url: "approve.php", // 
            data: 'id=' + idnew, // <---
            success: function(data){
                $("#result_chk").html(data)  
            },
            error: function(){
                alert("failure");
            }
        });
    }
</script>

<script type="text/javascript">
function myFunc(datanew){
        $.ajax({
            type: "POST",
            url: "checkall.php", // 
            data: 'data1=' + datanew, // <---
            success: function(data){
                $("#result_chk").html(data)  
            },
            error: function(){
                alert("failure");
            }
        });
    }
</script>

<script type="text/javascript">
function cancel(idnew){
        $.ajax({
            type: "POST",
            url: "cancel.php", // 
            data: 'id=' + idnew, // <---
            success: function(data){
                $("#result_chk").html(data)  
            },
            error: function(){
                alert("failure");
            }
        });
    }
</script>
</head>
<body style="background-color:#ddd">
<div style="margin-top:15px;" class="pull-left">
<span style="font-size:18px;color:#555;font-family:Verdana;margin-left:15px;margin-top:20px;">
OWN LR FORM : Download <b><span style="color:brown"> <?php echo strtoupper($branch); ?> Branch </b></span></span>
</div>
<div class="pull-right" style="font-family:Verdana">
<a href="./"><button style="margin-right:5px;margin-top:15px;margin-right:5px" class="btn btn-danger">Go back</button></a>
<a href="../" target="_blank"><button style="margin-right:5px;margin-top:15px;margin-right:10px" class="btn btn-primary">Dashboard</button></a>
</div>
<input id="branch" type="hidden" value="<?php echo $branch; ?>" />
<script type="text/javascript">
function auto_load(){
        $.ajax({
          url: "load_lr.php",
		  data: 'branch=' + $("#branch").val(),
          cache: false,
          success: function(data){
             $("#load1").html(data);
          } 
        });
}
 $(document).ready(function(){
 auto_load();
 });
//Refresh auto_load() function after 1000 milliseconds
//setInterval(auto_load,1000);
</script>

<div id="result_chk"></div>

<div class="container-fluid" style="font-family:Verdana">
<br />
<br />
<br />
<div class="row">
<div class="col-sm-12 col-md-12 col-lg-12 table-responsive" id="load1">
</div>

</div>

</div>
</body>