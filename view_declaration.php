<?php
require_once 'connect.php';
?>
<!doctype html>
<html lang="en">

<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../b5aY6EZzK52NA8F/favicon.png" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
<link rel="stylesheet" href="../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../b5aY6EZzK52NA8F/data_table_custom.css" rel="stylesheet" type="text/css" />
</head>

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 
</style>

<style>
/* width */
::-webkit-scrollbar {
  width: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 15px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: gray; 
  border-radius: 15px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: lightblue; 
}
</style>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:.98; cursor: wait">
	<center><img style="margin-top:100px" src="./load.gif" /><br><b>Please wait ...</b></center>
</div>	

<body style="background:;font-family: 'Open Sans', sans-serif !important">

<a href="./"><button type="button" style="margin-left:10px;margin-top:10px;margin-bottom:10px;" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 table-responsive">
		<div class="card-body" style="min-height: 640px; background-color: #fff;"> 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead style="" class="thead-light bg-success">
		        <tr>
					<th>#</th>
					<th>Vehice_number</th>
					<th>Wheeler</th>
					<th>Vehicle_owner</th>
					<th>Mobile</th>
					<th>PAN_no</th>
					<th>Rc_Copy</th>
					<th>PAN Copy</th>
					<th>Decl. Copy</th>
					<th>Decl. upload time</th>
					<th>Address</th>
					<th>Branch</th>
				</tr>
		      </thead> 
		 	</table>
		</div>
		</div>
	</div>
</div>
</body>
</html>

<script type="text/javascript">
function LoadData(){
$("#loadicon").show(); 
 var table = jQuery("#user_data").dataTable({ 
		"scrollY": 500,
        "scrollX": true,
		"lengthMenu": [ [50, 500, 1000, -1], [50, 500, 1000, "All"] ], 
		"bProcessing": true,
		"sPaginationType":"full_numbers",
		"dom": "lBfrtip",
		"ordering": true,
		"buttons": [
		// "copy", "excel", "print", "colvis"
		"excel","colvis"
		],
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=../b5aY6EZzK52NA8F/load_table.gif height=20> </center>"
        },
		"order": [[1, "asc" ]],
		"columnDefs":[
	{ 
    "targets": 0, //Comma separated values
    "visible": false,
    "searchable": false 
	},
	 {
        targets: 6, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-primary" onclick=ViewCopy('+full[0]+',"FRONT")>Front</button>'+
		 '&nbsp; <button type="button" class="btn-xs btn-primary" onclick=ViewCopy('+full[0]+',"REAR")>Rear</button>';
        }
      },
      {
		targets: 7, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-primary" onclick=ViewCopy('+full[0]+',"PAN")>View</button>';
        }
      },
	  {
		targets: 8, // edit column order
        data: null,
        mRender: function(data, type, full){
         return '<button type="button" class="btn-xs btn-primary" onclick=ViewCopy('+full[0]+',"DECL")>View</button>';
        }
      }
		], 
        "serverSide": true,
        "ajax": "load_declaration_server.php",
        "initComplete": function( settings, json ) {
 		$("#loadicon").hide();
 		}
    } );
}
</script>

<script>
LoadData();
</script>

<a href="" style="display:none" target="_blank" id="img_path"></a>

<script type="text/javascript">
function ViewCopy(id,ident)
{
		$("#loadicon").show();
		jQuery.ajax({
		url: "_load_vehicle_files.php",
		data: 'id=' + id + '&ident=' + ident,
		type: "POST",
		success: function(data) {
			$("#result_files").html(data);
		},
		error: function() {}
		});
}
</script>

<div id="result_files"></div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">              
        <div class="modal-body" style="overflow:auto">
          <label style="color:blue"><b>Vehicle Owner : </b><span id="name_box"></span>, <b>Vehicle No :</b> <span id="no_box"></span></label>
		  <img src="" class="imagepreview" style="width: 100%;" >
        </div> 
		
		<div class="modal-footer">
		<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>	
		<a target="_blank" id="FullLink">
			<button type="button" class="btn btn-primary pull-left">View FULL Size</button>
		</a>
        </div> 
	</div>
 </div>
 </div>
 
 <button style="display:none" id="btnmodal1" data-toggle="modal" data-target="#imagemodal"></button>