<?php
require_once 'connection.php';

$branch=mysqli_real_escape_string($conn,strtoupper($_SESSION['user']));

$output='';

$date2 = date("Y-m-d"); 

if($_POST['range']=='FULL')
{
$result=mysqli_query($conn,"SELECT vou_no,vou_type,pay_type,vou_date,lrno,amount,cheq_no,date FROM cheque_book WHERE branch='$branch' ORDER BY id asc");		
}
else
{
$from_date=date('Y-m-d', strtotime($_POST['range'], strtotime($date2)));
$to_date=date("Y-m-d");	

$result=mysqli_query($conn,"SELECT vou_no,vou_type,pay_type,vou_date,lrno,amount,cheq_no,date FROM cheque_book WHERE date between '$from_date' and '$to_date' and branch='$branch' ORDER BY id asc");		
}

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
		alert('No result found...');
		window.location.href='./cheq_book.php';
		</script>";
		mysqli_close($conn);
		exit();
}

if(isset($_POST['download']))
{
	 $output .= '
   <table border="1">  
       <tr>  
        <th>Vou No</th>
		<th>Vou Type</th>
		<th>Adv/Bal</th>
		<th>LR No</th>
		<th>Cheq Date</th>
		<th>Vou Date</th>
		<th>Amount</th>
		<th>Cheq No</th>
	</tr>
  ';
  while($row=mysqli_fetch_array($result))
  {
  $output .= '
				<tr> 
							<td>'.$row["vou_no"].'</td> 
							<td>'.$row["vou_type"].'</td> 
							<td>'.$row["pay_type"].'</td> 
							<td>'.$row["lrno"].'</td>  
							<td>'.$row["date"].'</td>  
							<td>'.$row["vou_date"].'</td>  
							<td>'.$row["amount"].'</td>  
							<td>'.$row["cheq_no"].'</td>  
				</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Cheque_Book.xls');
  echo $output;
  exit();
}
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
.form-control
{
	border:1px solid #000;
}
</style> 

</head>

<body style="background-color:#078388;font-family:Verdana">   

<a href="./cheq_book.php"><button class="btn btn-warning" style="color:#000;margin-top:10px;margin-left:10px;letter-spacing:1px"><b>Go Back</b></button></a>
<a href="./"><button class="btn btn-warning" style="color:#000;margin-top:10px;margin-left:10px;letter-spacing:1px"><b>Dashboard</b></button></a>  

        <div class="container-fluid">
            <div class="col-md-12">
                <center>
				<h4 style="font-family:Baumans;color:#FFF;font-size:24px;">ChequeBook - Summary</h4>
                    </center>
                    <div style="overflow-x:auto" class="panel-body table-responsive">                         
<?php
echo "<table class='table table-bordered' style='font-size:12px;background-color:#FFF;'>";
echo "
<tr>
		<th>Vou No</th>
		<th>Vou Type</th>
		<th>Adv/Bal</th>
		<th>LR No</th>
		<th>Cheq Date</th>
		<th>Vou Date</th>
		<th>Amount</th>
		<th>Cheq No</th>
</tr>";

while($row=mysqli_fetch_array($result))
  {

$dt1=date('d-M-y', strtotime($row['date']));  
echo "<tr>";
$vou_type=$row['vou_type'];
$vou_no=$row['vou_no'];

if($vou_type=="Freight_Memo")
{
echo "<td>
<form action='smemo2.php' target='_blank' method='POST'>
<input type='hidden' name='idmemo' value='$vou_no' />
<input type='hidden' name='key' value='FM' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else if($vou_type=="Expense_Voucher"){
echo "<td>
<form action='showvou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else if($vou_type=="Truck_Voucher"){
echo "<td>
<form action='showvou.php' target='_blank' method='POST'>
<input type='hidden' name='vou_no' value='$vou_no' />
<input type='hidden' name='voutype' value='$vou_type' />
<a href='#' onclick='this.parentNode.submit();' style='letter-spacing:1px;font-weight:bold'>$vou_no</a>
</form>
</td>";
}
else
{
echo "<td>$vou_no</td>";
}
echo "
<td>$vou_type</td>
<td>$row[pay_type]</td>
<td>$row[lrno]</td>
<td>$row[date]</td>
<td>$row[vou_date]</td>
<td>$row[amount]</td>
<td>$row[cheq_no]</td>
</tr>";
}
echo "</table>";
?>             
                </div>
            </div>
        </div>
       
</body>
</html>