<?php
require_once 'connect.php';

$ho = $_SESSION['ho'];
$q_99 = mysqli_query($conn,"SELECT title,branch FROM user WHERE username='$ho'");
$row_99 = mysqli_fetch_array($q_99);

$limit= $row_99['title'];
$branches= $row_99['branch'];

$from_date=$_POST['from'];
$to_date=$_POST['to'];
$branch=$_POST['branch'];
$company=$_POST['company'];
?>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RRPL</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid gray;
}

.table-bordered > tbody > tr > td {
     border: 1px solid gray;
}
 </style> 			
</head>
<body style="background-color:lightblue;font-family:Verdana">   
<a href="./diesel_book.php"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:1px">Go Back</button></a>
<a href="./"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:1px">Dashboard</button></a>  
<div class="container-fluid">
            <div class="col-md-12 col-sm-12">
                <center>
					<span style="font-size:20px;font-family:Verdana">Diesel-Book Summary</span>
				</center>
					<div class="table-responsive" style="font-family:Verdana;font-size:13px">                         
<a href="download_dieselbook.php?branch=<?php echo $branch; ?>&from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&company=<?php echo $company; ?>">
<button class="btn btn-danger pull-right">Download Dieselbook</button></a>
<br />
<br />
<br />
<?php
if($company=='RRPL') 
{
if($branch=="ALL")	
{
$result = mysqli_query($conn,"SELECT fno,branch,totalamt,disamt,tno,lrno,acname,type,dsl_by,dcard,dcom,pay_date FROM diesel_fm WHERE pay_date between '$from_date' AND '$to_date' AND branch in($limit) AND com='RRPL' ORDER BY id ASC");
}
else
{
$result = mysqli_query($conn,"SELECT fno,branch,totalamt,disamt,tno,lrno,acname,type,dsl_by,dcard,dcom,pay_date FROM diesel_fm WHERE pay_date between '$from_date' AND '$to_date' AND com='RRPL' AND branch='$branch' ORDER BY id ASC");	
}
}
else if($company=='RAMAN_ROADWAYS')
{
if($branch=="ALL")	
{
$result = mysqli_query($conn,"SELECT fno,branch,totalamt,disamt,tno,lrno,acname,type,dsl_by,dcard,dcom,pay_date FROM diesel_fm WHERE pay_date between '$from_date' AND '$to_date' AND branch in($limit) AND com='RAMAN_ROADWAYS' ORDER BY id ASC");
}
else
{
$result = mysqli_query($conn,"SELECT fno,branch,totalamt,disamt,tno,lrno,acname,type,dsl_by,dcard,dcom,pay_date FROM diesel_fm WHERE pay_date between '$from_date' AND '$to_date' AND com='RAMAN_ROADWAYS' AND branch='$branch' ORDER BY id ASC");	
}	
}

if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
	alert('No Diesel entry found..'); 
	location.href='diesel_book.php'; 
	</script>";
	exit();
}

echo "<table class='table table-bordered' style='font-size:14px'>";
echo "
<tr>
		<th>Branch</th>
		<th>Frno</th>
		<th>Rchg Date</th>
		<th>Freight</th>
		<th>Diesel Amt</th>
		<th>Party Name</th>
		<th>TruckNo</th>
		<th>Lr No</th>
		<th>Adv/Bal</th>
		<th>Card/Pump</th>
		<th>Card/Pump No</th>
		<th>Pump Name</th>
	 </tr>		
	";

while($row = mysqli_fetch_array($result))
  {
$dt1 = date('d-M-y', strtotime($row['pay_date']));  
	
echo "<td>$row[branch]</td>";
echo "<td>$row[fno]</td>";
echo "<td>$dt1</td>";
echo "<td>$row[totalamt]</td>";
echo "<td>$row[disamt]</td>";
echo "<td>$row[acname]</td>";
echo "<td>$row[tno]</td>";
echo "<td>$row[lrno]</td>";
echo "<td>$row[type]</td>";
echo "<td>$row[dsl_by]</td>";
echo "<td>$row[dcard]</td>";
echo "<td>$row[dcom]</td>
</tr>
";
  }
echo "</table>";
?>             
                </div>
            </div>
        </div>
</body>
</html>