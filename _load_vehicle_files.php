<?php
require_once '../_connect.php';

$id = escapeString($conn,$_POST['id']);
$ident = escapeString($conn,$_POST['ident']);

$sql = Qry($conn,"SELECT tno,name,up1,rc_rear,up4,up6 FROM mk_truck WHERE id='$id'");

if(!$sql){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

$row = fetchArray($sql);

$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
$url =substr($url,0,-28)."b5aY6EZzK52NA8F";

$rc_copy1 = "$url/truck_rc/".substr($row['up1'], strpos($row['up1'], "truck_rc/") + 9);    
$rc_copy2 = "$url/truck_rc/".substr($row['rc_rear'], strpos($row['rc_rear'], "truck_rc/") + 9);    
$pan_copy = "$url/truck_pan/".substr($row['up4'], strpos($row['up4'], "truck_pan/") + 10);    
$dec_copy = "$url/truck_dec/".substr($row['up6'], strpos($row['up6'], "truck_dec/") + 10);    
	
$rc_front_path = pathinfo($rc_copy1, PATHINFO_EXTENSION);
$rc_rear_path = pathinfo($rc_copy2, PATHINFO_EXTENSION);
$pan_path = pathinfo($pan_copy, PATHINFO_EXTENSION);
$dec_path = pathinfo($dec_copy, PATHINFO_EXTENSION);

if($ident=='FRONT')
{
	$extension = $rc_front_path;
	$img = $rc_copy1;
}
else if($ident=='REAR')
{
	$extension = $rc_rear_path;
	$img = $rc_copy2;
}
else if($ident=='PAN')
{
	$extension = $pan_path;
	$img = $pan_copy;
}
else if($ident=='DECL')
{
	$extension = $dec_path;
	$img = $dec_copy;
}
else
{
	exit();
}

if($extension=='')
{
	echo "<script>
		alert('Invalid file !!');
		$('#loadicon').hide();
	</script>";
	exit();
}

			if($extension=='pdf')
			{
				echo "<script>
					$('#img_path').attr('href','$img');
					document.getElementById('img_path').click();
					$('#loadicon').hide();
				<script>";
				exit();
			}
			else
			{
				echo "<script>
					$('.imagepreview').attr('src','$img');
					$('#name_box').html('$row[name]');
					$('#no_box').html('$row[tno]');
					$('#FullLink').attr('href','$img');
					$('#btnmodal1').click();  
					$('#loadicon').hide();
				</script>";
				exit();
			}
?>