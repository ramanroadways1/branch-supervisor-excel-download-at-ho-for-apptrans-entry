<?php
require_once("./connect.php");

$id = escapeString($conn,strtoupper($_POST['id']));
$frno = escapeString($conn,strtoupper($_POST['frno']));
$creditor_type = escapeString($conn,strtoupper($_POST['creditor_type']));

if($creditor_type!='OWNER' AND $creditor_type!='BROKER')
{
	echo "
	<script>
		alert('Warning : creditor not found !');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

// echo "
	// <script>
		// alert('Error !');
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
	
$qry = Qry($conn,"SELECT f.frno,f.totalf,f.totaladv,f.baladv,f.totalbal,f.ptob,f.paidto,b.name as broker_name,b.pan as broker_pan,
o.name as owner_name,o.pan as owner_pan 
FROM freight_form AS f 
LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
WHERE f.id='$id'");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows = numRows($qry);

if($numrows==0)
{
	echo "
	<script>
		alert('Freight memo found !!');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

$row = fetchArray($qry);

if($creditor_type=='OWNER')
{
	$creditor_name = $row['owner_name'];
	$creditor_pan = $row['owner_pan'];
}
else
{
	$creditor_name = $row['broker_name'];
	$creditor_pan = $row['broker_pan'];
}

if(strlen($creditor_pan)!=10)
{
	echo "
	<script>
		alert('Warning : Creditor PAN card: ($creditor_pan) is not valid !');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

if($row['paidto']!='')
{
	echo "
	<script>
		alert('Warning : Balance paid !!');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

if($row['frno']!=$frno)
{
	echo "
	<script>
		alert('Warning : Freight memo number not verified !!');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', true);
	</script>";
	exit();
}

$check_total_lrs = Qry($conn,"SELECT id FROM freight_form_lr WHERE frno='$row[frno]'");

if(!$check_total_lrs){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$total_lrs = numRows($check_total_lrs);

$check_pod = Qry($conn,"SELECT lrno FROM rcv_pod WHERE frno='$frno'");

if(!$check_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$numrows_pod = numRows($check_pod);

if($numrows_pod==0)
{
	$pod_status="ALL_PENDING";
}
else if($numrows_pod!=$total_lrs)
{
	$lrnos = array();
	while($row_lrno=fetchArray($check_pod))
	{
		$lrnos[] = "'".$row_lrno['lrno']."'";
	}

	$lrnos=implode(',', $lrnos);
	$pod_status="PARTIAL_PENDING";
}
else
{
	$pod_status = "ALL_RCVD";
}

StartCommit($conn);
$flag = true;

if($pod_status=="ALL_PENDING")
{
	$copy_pod = Qry($conn,"INSERT INTO rcv_pod(frno,veh_type,lrno,lr_id,consignor_id,branch,fm_date,fm_amount,pod_branch,
	pod_copy,pod_date,del_date,ho_rcvd,timestamp,exdate) SELECT f.frno,'MARKET',f.lrno,l.id,l.con1_id,f.branch,f.create_date,
	f2.actualf,l.branch,'pod_copy/pod_auto_rcvd.jpg','$date','$date','1','$timestamp','$date' 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.id='$id' 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	WHERE f.frno='$frno'");
	
	if(!$copy_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='$date' WHERE frno='$frno'");
	
	if(!$update_pod_date_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}
else if($pod_status=='PARTIAL_PENDING')
{
	$copy_pod = Qry($conn,"INSERT INTO rcv_pod(frno,veh_type,lrno,lr_id,consignor_id,branch,fm_date,fm_amount,pod_branch,
	pod_copy,pod_date,del_date,ho_rcvd,timestamp,exdate) SELECT f.frno,'MARKET',f.lrno,l.id,l.con1_id,f.branch,f.create_date,
	f2.actualf,l.branch,'pod_copy/pod_auto_rcvd.jpg','$date','$date','1','$timestamp','$date' 
	FROM freight_form_lr AS f 
	LEFT OUTER JOIN freight_form AS f2 ON f2.id='$id' 
	LEFT OUTER JOIN lr_sample AS l ON l.lrno=f.lrno 
	WHERE f.frno='$frno' AND f.lrno NOT IN($lrnos)");
	
	if(!$copy_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
	
	$update_pod_date_fm = Qry($conn,"UPDATE freight_form_lr SET market_pod_date='$date' WHERE frno='$frno' AND lrno NOT IN($lrnos)");
	
	if(!$update_pod_date_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}

if($row['ptob']=='')
{
	$update_fm = Qry($conn,"UPDATE freight_form SET newother = newother+totalf,totalf=0,adv_branch_user=branch_user,ptob='$creditor_type',rtgs_adv='1',
	adv_date='$date',pto_adv_name='$creditor_name',adv_pan='$creditor_pan',narre='Advance Forfeit. Creditor: $creditor_type',baladv=0,totalbal='0.00',
	paidto='$creditor_type',bal_date='$date',pto_bal_name='$creditor_name',bal_pan='$creditor_pan',rtgs_bal='1',
	narra='Balance Forfeit. Creditor: $creditor_type',pod='1',branch_bal=branch,forfeit_balance='1' WHERE id='$id'");
	
	if(!$update_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}
else
{
	$update_fm = Qry($conn,"UPDATE freight_form SET otherfr=baladv,totalbal='0.00',paidto='$creditor_type',bal_date='$date',
	bal_branch_user=adv_branch_user,pto_bal_name='$creditor_name',bal_pan='$creditor_pan',rtgs_bal='1',narra='Balance Forfeit. Creditor: $creditor_type',pod='1',
	branch_bal=branch,forfeit_balance='1' WHERE id='$id'");
	
	if(!$update_fm){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		$flag = false;
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Done !');
		window.location.href='./forfeit_balance.php';
	</script>"; 
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error !');
		$('#loadicon').fadeOut('slow');
		$('#Btn1$id').attr('disabled', false);
	</script>";
	exit();
}	
?>