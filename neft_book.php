<?php
require_once 'connect.php';
?>	
<!doctype html>
<html lang="en">

<?php
include("./_header.php"); 
?>

<link href="css/styles.css" rel="stylesheet">

<style>
label{
	color:#FFF;
}
</style>

<body style="background:#078388;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<?php include 'sidebar.php';?>

<div class="container-fluid">

<div class="form-group col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3">			

<div class="row">
	<div class="form-group col-md-4 col-md-offset-4">
	<br />
	<br />
	<center>
		<h4 style="letter-spacing:1px;color:#FFF;font-weight:bold;font-size:16px">NEFT Sheet : Download</h4>
	</center>	
	<br />
	<form action="load_books.php" method="POST">
	<label>Select Branch <font color="red">*</font></label>
	<select name="branch" class="form-control" required="required">
		<option value="">Select Branch</option>
		<option value="ALL">ALL Branch</option>
		<?php 
		$qry= Qry($conn,"SELECT username FROM user WHERE role='2' AND branch_inactive!='1' AND branch_supervisor_ho='$my_id' ORDER BY username ASC");
		while($row=fetchArray($qry))
		{
			echo "<option value='$row[username]'>$row[username]</option>";
		}
		?>
	</select>
	<br />
	<label>From date <font color="red">*</font></label>
	<input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" name="from_date" class="form-control" required="required">
	<br />
	<label>To date <font color="red">*</font></label>
	<input type="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" name="to_date" class="form-control" required="required">
	<br />
	<input type="hidden" name="identifier" value="NEFT">
	<input type="hidden" name="company" value="">
	<input type="submit" class="btn btn-sm btn-danger" name="submit" value="View NEFT/RTGS" /> 
	</form>
	
</div>
</div>
</div>
</div>

</body>
</html>