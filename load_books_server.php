<?php
require_once 'connect.php';

$branch = escapeString($conn,$_POST['branch']);
$from_date = escapeString($conn,$_POST['from_date']);
$to_date = escapeString($conn,$_POST['to_date']);
$identifier = escapeString($conn,$_POST['identifier']);
$company = escapeString($conn,$_POST['company']);
$timestamp = date("Y-m-d H:i:s");

if($identifier=='CASHBOOK')
{
	if($branch=='ALL')
	{
		$qry = Qry($conn,"SELECT date,user,vou_date,comp,vou_no,vou_type,desct,lrno,IF(comp='RRPL',debit,debit2) as debit,
		IF(comp='RRPL',credit,credit2) as credit,IF(comp='RRPL',balance,balance2) as balance FROM cashbook WHERE date BETWEEN 
		'$from_date' AND '$to_date' AND comp='$company' AND user IN($limit) ORDER BY id ASC");
	}
	else
	{
		$qry = Qry($conn,"SELECT date,user,vou_date,comp,vou_no,vou_type,desct,lrno,IF(comp='RRPL',debit,debit2) as debit,
		IF(comp='RRPL',credit,credit2) as credit,IF(comp='RRPL',balance,balance2) as balance FROM cashbook WHERE date BETWEEN 
		'$from_date' AND '$to_date' AND comp='$company' AND user='$branch' ORDER BY id ASC");
	}
	
}
else if($identifier=='PASSBOOK')
{
	if($branch=='ALL')
	{
		$qry = Qry($conn,"SELECT date,user,vou_date,comp,vou_no,vou_type,desct,lrno,IF(comp='RRPL',debit,debit2) as debit,
		IF(comp='RRPL',credit,credit2) as credit,'' as balance FROM passbook WHERE date BETWEEN 
		'$from_date' AND '$to_date' AND comp='$company' AND user IN($limit) ORDER BY id ASC");
	}
	else
	{
		$qry = Qry($conn,"SELECT date,user,vou_date,comp,vou_no,vou_type,desct,lrno,IF(comp='RRPL',debit,debit2) as debit,
		IF(comp='RRPL',credit,credit2) as credit,'0' as balance FROM passbook WHERE date BETWEEN 
		'$from_date' AND '$to_date' AND comp='$company' AND user='$branch' ORDER BY id ASC");
	}
	
}
else if($identifier=='NEFT')
{
	if($branch=='ALL')
	{
		$qry = Qry($conn,"SELECT fno,branch,com,amount,acname,acno,pan,fm_date,pay_date,lrno,tno,type,crn,bank,utr_date 
		FROM rtgs_fm WHERE pay_date BETWEEN '$from_date' AND '$to_date' AND branch IN($limit) ORDER BY id ASC");
	}
	else
	{
		$qry = Qry($conn,"SELECT fno,branch,com,amount,acname,acno,pan,fm_date,pay_date,lrno,tno,type,crn,bank,utr_date 
		FROM rtgs_fm WHERE pay_date BETWEEN '$from_date' AND '$to_date' AND branch='$branch' ORDER BY id ASC");
	}
}
else if($identifier=='DIESEL')
{
	echo "under construction";
	exit();
}
else
{
	echo "Error";
	exit();
}

if(numRows($qry)==0)
{
	echo "
	<font color='red'><center>No result found..</center></font>
	<script>
		$('#loadicon').fadeOut();
	</script>";
}

if($identifier=='NEFT')
{
	echo "<table id='example' class='display nowrap table table-bordered' style='font-size:11px;'>
		<thead>
			<tr>
				<th>#</th>
				<th>Branch</th>
				<th>Vou_no</th>
				<th>Vou_date</th>
				<th>Payment_date</th>
				<th>Company</th>
				<th>Vou_type</th>
				<th>Amount</th>
				<th>Ac_holder</th>
				<th>LR_number</th>
				<th>Vehicle_number</th>
				<th>UTR_number</th>
				<th>UTR_date</th>
			</tr>
		</thead>
		</tbody>";
		
		$sn=1;	
		
		while($row = fetchArray($qry))
		{
			if($row['bank']!='') { $utr_date = date("d-m-y",strtotime($row['utr_date'])); } else { $utr_date=""; }
			echo "<tr>
				<td>$sn</td>
				<td>$row[branch]</td>
				<td>$row[fno]</td>
				<td>".date("d-m-y",strtotime($row['fm_date']))."</td>
				<td>".date("d-m-y",strtotime($row['pay_date']))."</td>
				<td>$row[com]</td>
				<td>$row[type]</td>
				<td>$row[amount]</td>
				<td>$row[acname]</td>
				<td>$row[lrno]</td>
				<td>$row[tno]</td>
				<td>$row[bank]</td>
				<td>$utr_date</td>
			</tr>";
		$sn++;	
		}
		echo "
		</tbody>
		</table>";


echo "<script>
	$('#loadicon').fadeOut();
	$('#opening_balance').hide();
</script>";
}
else
{
	echo "<table id='example' class='display nowrap table table-bordered' style='font-size:11px;'>
		<thead>
			<tr>
				<th>#</th>
				<th>Branch</th>
				<th>Vou_no</th>
				<th>Vou_date</th>
				<th>Date</th>
				<th>Company</th>
				<th>Vou_type</th>
				<th style='width:200px !important'>Particulars</th>
				<th style='width:30px'>LR_no</th>
				<th>Debit</th>
				<th>Credit</th>
				<th>Balance</th>
			</tr>
		</thead>
		</tbody>";
		
		$sn=1;	
		$closing_balance = 0;
		while($row = fetchArray($qry))
		{
			if($sn == 1)
			{
				$opening_balance = $row['balance']-$row['credit']+$row['debit'];
			}
			
			$first = false;
		
			echo "<tr>
				<td>$sn</td>
				<td>$row[user]</td>
				<td>$row[vou_no]</td>
				<td>".date("d-m-y",strtotime($row['vou_date']))."</td>
				<td>".date("d-m-y",strtotime($row['date']))."</td>
				<td>$row[comp]</td>
				<td>$row[vou_type]</td>
				<td>$row[desct]</td>
				<td>$row[lrno]</td>
				<td>$row[debit]</td>
				<td>$row[credit]</td>
				<td>$row[balance]</td>
			</tr>";
			
			$closing_balance = $row['balance'];
		$sn++;	
		}
		echo "
		</tbody>
		</table>";


echo "<script>
	$('#loadicon').fadeOut();
	$('#opening_balance').html('<br>Opening : $opening_balance - Closing : $closing_balance.');
	$('#opening_balance').show();
</script>";
}
?>