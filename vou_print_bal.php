<?php
require_once('./connect.php');

// $branch=escapeString($conn,strtoupper($_SESSION['user']));
$vou_no=escapeString($conn,strtoupper($_POST['frno']));
?>		
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">

<style type="text/css" media="print">
@media print {
body {
   zoom:45%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.printpage * { visibility: visible}
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>

<body style="overflow-x:hidden;font-family: 'Open Sans', sans-serif !important">

<?php
$ones = array(
 "",
 " ONE",
 " TWO",
 " THREE",
 " FOUR",
 " FIVE",
 " SIX",
 " SEVEN",
 " EIGHT",
 " NINE",
 " TEN",
 " ELEVEN",
 " TWELVE",
 " THIRTEEN",
 " FOURTEEN",
 " FIFTEEN",
 " SIXTEEN",
 " SEVENTEEN",
 " EIGHTEEN",
 " NINETEEN"
);
 
$tens = array(
 "",
 "",
 " TWENTY",
 " THIRTY",
 " FORTY",
 " FIFTY",
 " SIXTY",
 " SEVENTY",
 " EIGHTY",
 " NINETY"
);
 
$triplets = array(
 "",
 " THOUSAND",
 " MILLION",
 " BILLION",
 " TRILLION",
 " quadrillion",
 " quintillion",
 " sextillion",
 " septillion",
 " octillion",
 " nonillion"
);
 
 // recursive fn, converts three digits per pass
function convertTri($num, $tri) {
  global $ones, $tens, $triplets;
 
  // chunk the number, ...rxyy
  $r = (int) ($num / 1000);
  $x = ($num / 100) % 10;
  $y = $num % 100;
 
  // init the output string
  $str = "";
 
  // do hundreds
  if ($x > 0)
   $str = $ones[$x] . " HUNDRED";
 
  // do ones and tens
  if ($y < 20)
   $str .= $ones[$y];
  else
   $str .= $tens[(int) ($y / 10)] . $ones[$y % 10];
 
  // add triplet modifier only if there
  // is some output to be modified...
  if ($str != "")
   $str .= $triplets[$tri];
 
  // continue recursing?
  if ($r > 0)
   return convertTri($r, $tri+1).$str;
  else
   return $str;
 }
 
// returns the number as an anglicized string
function convertNum($num) {
 $num = (int) $num;    // make sure it's an integer
 
 if ($num < 0)
  return "NEGATIVE".convertTri(-$num, 0);
 
 if ($num == 0)
  return "ZERO";
 
 return convertTri($num, 0);
}
 
 // Returns an integer in -10^9 .. 10^9
 // with log distribution
 function makeLogRand() {
  $sign = mt_rand(0,1)*2 - 1;
  $val = randThousand() * 1000000
   + randThousand() * 1000
   + randThousand();
  $scale = mt_rand(-9,0);
 
  return $sign * (int) ($val * pow(10.0, $scale));
 }
 
$qry=Qry($conn,"SELECT f.frno,f.company,f.branch,f.truck_no,f.actualf,f.baladv,f.unloadd,f.otherfr,f.claim,f.late_pod,f.totalbal,
f.bal_date,f.pto_bal_name,f.bal_pan,f.paycash,f.paydsl,f.newrtgsamt,SUM(l.weight) as total_weight,
GROUP_CONCAT(l.lrno SEPARATOR ',') as lrnos,GROUP_CONCAT(l.tstation SEPARATOR ',') as tstation,GROUP_CONCAT(l.lrno SEPARATOR ',') as lrnos,
l.consignor,l.fstation,l.date
FROM freight_form AS f 
LEFT OUTER JOIN freight_form_lr as l on l.frno=f.frno 
WHERE f.frno='$vou_no' GROUP by f.frno");

if(!$qry)
{
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($qry)==0)
{
	echo "Zero Result.";
	exit();
}

$row=fetchArray($qry);

if($row['company']=='RRPL')
{
	$logo='<img src="../b5aY6EZzK52NA8F/logo/rrpl.jpg" style="width:500px" />';
}
else
{
	$logo='<img src="../b5aY6EZzK52NA8F/logo/rr.jpg" style="width:500px" />';
}

$lrnos = $row['lrnos'];
 
?>
<button id="button1" onclick="print();" style="margin-top:10px;margin-left:10px;" class="btn-sm btn btn-primary"><b>Print Voucher</button></b>
<button onclick="window.close();" style="margin-top:10px" class="btn-sm btn btn-danger"><b>Close Window</button></b>
<br />
<br />
<div class="printpage">
<?php 
if($row['paycash']>0)
{
?>
<div class="container-fluid">
<center>
<table style="width:90%;font-size:12px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $row['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['date'])); ?></td>
		<th>Bal Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">CASH</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['paycash'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['paycash']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row['total_weight'])." pmt"; ?></td>
      </tr>

		<tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['paycash']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>
<br />
<br />
<br />
<?php
}
?>

<!-- DIESEL --> 

<?php
if($row['paydsl']>0)
{
?>
<div class="container-fluid">
<center>
<table style="width:90%;font-size:12px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $row['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['date'])); ?></td>
		<th>Adv Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">DIESEL</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['paydsl'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['paydsl']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row['total_weight'])." pmt"; ?></td>
      </tr>
	  
	  	<tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['paydsl']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>
<br />
<br />
<br />
<?php
}
?>

<!-- RTGS -->

<?php
if($row['newrtgsamt']>0)
{
?>
<div class="container-fluid">
<center>
<table style="width:90%;font-size:12px;" class="table table-bordered">
     <tr>
       <td colspan="4"><?php echo $logo; ?></td>
       <td colspan="4"><center><b>FM No : </b><?php echo $vou_no."<br><br><b>Branch :</b> ".$row['branch']."</center>"; ?></b></td>
      </tr>
	 <tr>
	   <th>A/c With. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $row['consignor']." - FREIGHT PAY (BAL)"; ?></td>
     </tr>
	 
	  <tr>
        <th>Truck No. </th>
        <td id="linew"><?php echo $row['truck_no']; ?></td>
		<th>LR Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['date'])); ?></td>
		<th>Adv Date </th>
        <td id="linew"><?php echo date("d-m-Y", strtotime($row['bal_date'])); ?></td>
		<th>Vou Type </th>
        <td id="linew">RTGS</td>
      </tr>
	 
     <tr>
	 <th>Amount(in words)</th>
        <td colspan="5" id="linew"><?php echo convertNum($row['newrtgsamt'])." ONLY"; ?></td>
        <th>Amount  </th>
        <td id="linew"><?php echo $row['newrtgsamt']; ?></td>
		
      </tr>
     
	 <tr>
        <th>Freight </th>
        <td colspan="2" id="linew"><?php echo "Rs: ".$row['actualf']." /-"; ?></td>
		<th>Weight </th>
        <td id="linew"><?php echo $row['total_weight']." TN"; ?></td>
		<th>Rate </th>
        <td colspan="2" id="linew"><?php echo round($row['actualf']/$row['total_weight'])." pmt"; ?></td>
      </tr>
	  
	  <tr>
			<th>Balance </th>
			<td colspan="2" id="linew"><?php echo $row['baladv']; ?></td>
			<th>Unload/Deten. </th>
			<td colspan="2" id="linew"><?php echo $row['unloadd']; ?></td>
			<th>Other </th>
			<td colspan="2" id="linew"><?php echo $row['otherfr']; ?></td>
      </tr>
	  
	  <tr>
			<th>Claim </th>
			<td colspan="2" id="linew"><?php echo $row['claim']; ?></td>
			<th>LatePOD </th>
			<td colspan="2" id="linew"><?php echo $row['late_pod']; ?></td>
			<th>Total Bal </th>
			<td colspan="2" id="linew"><?php echo $row['totalbal']; ?></td>
      </tr>
	  
	  <tr>
	   <th>LR No. </th>
        <td colspan="7" style="font-size:14px;" id="linew"><?php echo $lrnos; ?></td>
     </tr>
	  
	   <tr>
        <th>From </th>
        <td colspan="3" id="linew"><?php echo $row['fstation']; ?></td>
		<th>To </th>
        <td colspan="3" id="linew"><?php echo $row['tstation']; ?></td>
      </tr>
	  
	   <tr>
        <th>Party Name </th>
        <td colspan="3" id="linew"><?php echo $row['pto_bal_name']; ?></td>
		<th>PAN No </th>
        <td colspan="3" id="linew"><?php echo $row['bal_pan']; ?></td>
      </tr>
	 
    <?php
	if($row['newrtgsamt']>=5000)
	{	
	?>
	<tr>
       <th colspan="3" style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign <br>
		<div style="margin-left:120px;border:1px solid #ddd;height:70px;width:70px;"><center><span style="color:red;font-size:10px">रेवेन्यू टिकिट अनिवार्य है।</span></center></div>
	   </th>
	</tr>
	<?php
	}
	else
	{
		?>
		<tr>
       <th colspan="3"style="height:100px;">Cashier Sign</th>
	   <th colspan="3" style="height:100px;">Accountant Sign</th>
	   <th colspan="3" style="height:100px;" colspan="2">Payee Sign</th>
	</tr>
		<?php
	}
	?>
   
</table>
</center>
</div>
<br />
<br />
<br />
<?php
}
?>
</div>
<script>
function myFunction() {
    window.print();
}
</script>
</body>
</html>					
<?php
closeConnection($conn);
?>	