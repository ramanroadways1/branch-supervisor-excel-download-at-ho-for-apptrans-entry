<?php 
require_once './connect.php';

$fm_no = escapeString($conn,strtoupper($_POST['fm_no']));
$party = escapeString($conn,$_POST['party']);
$timestamp = date("Y-m-d H:i:s");

$fm_id = explode("_",$party)[2];
$party_id = explode("_",$party)[1];
$party_type = explode("_",$party)[0];

if($party=='')
{
	echo "<script>
		alert('Party not found !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	exit();
}

if($fm_no=='')
{
	echo "<script>
		alert('FM number not found !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	exit();
}

$get_fm = Qry($conn,"SELECT f.id,f.frno,f.date,f.truck_no,f.branch,f.branch_user,f.from1,f.to1,f.ptob,f.totalf,f.totaladv,f.baladv,f.paidto,f.bid,f.oid,
b.name as broker_name,b.pan as broker_pan,o.name as owner_name,o.pan as owner_pan 
FROM freight_form AS f 
LEFT OUTER JOIN mk_broker AS b ON b.id = f.bid 
LEFT OUTER JOIN mk_truck AS o ON o.id = f.oid 
WHERE f.id = '$fm_id'");

if(!$get_fm){
	echo "<script>
		alert('Error !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_fm)==0)
{
	echo "<script>
		alert('FM not found !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	exit();
}

$row = fetchArray($get_fm);

if($row['frno'] != $fm_no)
{
	echo "<script>
		alert('FM not verified !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	exit();
}	

 $check_total_lrs = Qry($conn,"SELECT id FROM freight_form_lr WHERE frno='$fm_no'");

	if(!$check_total_lrs){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$total_lrs = numRows($check_total_lrs);

	$check_pod = Qry($conn,"SELECT id FROM rcv_pod WHERE frno='$fm_no'");

	if(!$check_pod){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$numrows_pod = numRows($check_pod);

	if($numrows_pod==0)
	{
		$pod_status = "<font color='red'></font> ALL pending";
	}
	else if($numrows_pod!=$total_lrs)
	{
		$pod_status = "<font color='red'>".($total_lrs - $numrows_pod)."</font> pending";
	}
	else
	{
		$pod_status = "<font color='green'>ALL received</font>";
	}

	echo "<table class='table table-bordered' style='font-size:11px'>
	<tr>
		<th>FM_date</th>
		<th>Branch</th>
		<th>Vehicle_no</th>
		<th>From</th>
		<th>To</th>
		<th>Total_Frt</th>
		<th>Total_Adv</th>
		<th>Balance</th>
		<th>Broker</th>
		<th>Owner</th>
		<th>Pod_status</th>
		<th>Forfeit_Amount</th>
		<th>#</th>
	</tr>";
	
if($party_type=='BROKER')
{
	$broker_name = "<font color='red'>$row[broker_name]</font>";
	$owner_name = "$row[owner_name]";
	$creditor_pan = "$row[owner_pan]";
}	
else
{
	$broker_name = "$row[broker_name]";
	$creditor_pan = "$row[broker_pan]";
	$owner_name = "<font color='red'>$row[owner_name]</font>";
}

if(strlen($creditor_pan)!=10)
{
	echo "<script>
		alert('Creditor\'s pan card: ($creditor_pan) is not valid !');
		$('#loadicon').fadeOut('slow');
		$('#creditor_party').val('');
	</script>";
	exit();
}
	
if($row['ptob']=='')
{
	$forfeit_amount = $row['totalf'];
}
else if($row['paidto']=='')
{
	$forfeit_amount = $row['baladv'];
}
else
{
	$forfeit_amount="NA";
}
	
	echo "<tr>
		<td>$row[date]</td>
		<td>$row[branch]</td>
		<td>$row[truck_no]</td>
		<td>$row[from1]</td>
		<td>$row[to1]</td>
		<td>$row[totalf]</td>
		<td>$row[totaladv]</td>
		<td>$row[baladv]</td>
		<td>$broker_name</td>
		<td>$owner_name</td>
		<td>$pod_status</td>
		<td style='color:red'>$forfeit_amount</td>
		";
		if($row['paidto']=='')
		{
		echo "	
		<td><button id='Btn1$row[id]' onclick=ForfeitFMAmount('$row[id]','$row[frno]','$party_type') style='font-size:12px;' type='button' 
		class='btn btn-xs btn-danger'>Proceed</button></td>";
		}
		else
		{
			echo "<td><font color='red'>Balance Paid</font></td>";
		}
		
	echo "</tr>
	</table>";
	
echo "<script>
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();	
?>