<?php
require_once 'connect.php';
$sql = "SELECT username FROM user WHERE username!='ADMIN' AND username!='DUMMY' AND role='2' ORDER BY username ASC";
$result = mysqli_query($conn,$sql);
$max = date("Y-m-d");
?>	
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RRPL</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/styles.css" rel="stylesheet">
    <script src="js/lumino.glyphs.js"></script>
    <style>
        body {
            overflow-x: hidden;
        }
		.form-control{
			border:1px solid #000;
			background:#eee;
		}
    </style>
</head>
<body style="background:lightblue;font-family:Verdana">
    <?php include 'sidebar.php';?>
<div class="col-sm-5 col-sm-offset-4 col-lg-5 col-lg-offset-4">
<br />

      <center><h4 style="color:#000;font-family:Verdana">Show Balance Difference</center></h4>
                       <br />
					   
<form action="bal_diff2.php" method="POST">

	<div class="form-group">
		<label class="col-md-3 control-label">Company <font color="red">*</font></label>
				<div class="col-md-9">
					 <select class="form-control" name="company" style="border:1px solid #000;background:#eee" required="required">
						<option value="">Select Company</option>
						<option value="RRPL">RRPL</option>
						<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</OPTION>
					</select>
				</div>
	</div>
	<br />
	<br />
	<br />
	<div class="form-group">
		<label class="col-md-3 control-label">To Date <font color="red">*</font></label>
			<div class="col-md-9">
				<input name="to" type="date" max="<?php echo $max; ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
    </div>
	<br />
	<br />
	<div class="form-group">
		<div class="col-md-12">
			<button type="submit" style="letter-spacing:1px;" class="btn btn-danger pull-right" name="submit">View Bal Difference</button>
                            </div>
                        </div>
                        </fieldset>
                        </form>
                    </div>
 

</body>
</html>