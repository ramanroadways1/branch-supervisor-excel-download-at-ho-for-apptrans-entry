<?php
if(isset($_SERVER['HTTPS'])){
        $protocol11 = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol11 = 'http';
    }
	
$baseUrl1 = $protocol11."://".$_SERVER['HTTP_HOST']."/USER/";
$baseUrl_main = $protocol11."://".$_SERVER['HTTP_HOST']."/b5aY6EZzK52NA8F/";
?>

<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
	<link rel="icon" type="image/png" href="<?php echo $protocol11."://".$_SERVER['HTTP_HOST']; ?>/favi1.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!--<script src="help/tphead.js" type="text/javascript"></script>-->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  
	<script src="<?php echo $baseUrl_main; ?>js/lumino.glyphs.js"></script>  
	<link rel="stylesheet" href="<?php echo $baseUrl_main; ?>font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="<?php echo $baseUrl_main; ?>google_font.css" rel="stylesheet">
</head>

<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}

body {
    overflow-x: auto;
}

.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}

label{
	font-size:13px;
}
.ui-autocomplete { z-index:2147483647; }
</style>

<div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:150px" src="../b5aY6EZzK52NA8F/load.gif" /></center>
</div>